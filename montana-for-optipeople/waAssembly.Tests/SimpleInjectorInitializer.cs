
namespace CPS.Tests
{
	using System.Reflection;
	using waAssembly.Services;
	using waAssembly.Services.interfaces;
	using SimpleInjector;
	using SimpleInjector.Extensions;

	public static class SimpleInjectorInitializer
	{

		public static Container container;

		/// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
		public static void Initialize()
		{
			container = new Container();
			InitializeContainer(container);
			container.Verify();
		}

		private static void InitializeContainer(Container container)
		{
			container.Register<ISessionContext, TestSessionHandler>();
			container.Register<ValidationTestControllerService, ValidationTestControllerService>();
		}
	}
}