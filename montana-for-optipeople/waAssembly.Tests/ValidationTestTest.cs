﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPS.Tests
{
	[TestClass]
	public class ValidationTestTest
	{
		[TestMethod]
		public void TestSession()
		{
			SimpleInjectorInitializer.Initialize();

			TestSessionHandler tsh = new TestSessionHandler();
			tsh.SetSession(API.Data.enums.SessionKeys.CurrentUser, new { Name = "snaps" });

			var controllerToTest = SimpleInjectorInitializer.container.GetInstance<waAssembly.Controllers.HomeController>();
			
			var result = controllerToTest.Index();

			Assert.AreEqual(result.GetType(), typeof(System.Web.Mvc.ContentResult));
			Assert.AreEqual(((System.Web.Mvc.ContentResult)result).Content, "False");

		}

	}
}
