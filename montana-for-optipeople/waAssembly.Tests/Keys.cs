﻿using System;
using System.Security.Cryptography;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CPS.Tests {
	[TestClass]
	public class Keys {

		public string CreateKey(int numBytes) {
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			byte[] buff = new byte[numBytes];
			rng.GetBytes(buff);
			return BytesToHexString(buff);
		}

		private String BytesToHexString(byte[] bytes) {
			StringBuilder hexString = new StringBuilder(64);
			for (int counter = 0; counter < bytes.Length; counter++) {
				hexString.Append(String.Format("{0:X2}", bytes[counter]));
			}
			return hexString.ToString();
		}


		[TestMethod]
		public void validationKey() {
			string q = @"<machineKey validationKey='{0}'
			decryptionKey='{1}'
			validation='SHA1'
			decryption='AES'
		/> <!-- for sharing login cookie across applications -->"
				.Replace("'", "\"")
				.Replace("{0}", CreateKey(24))
				.Replace("{1}", CreateKey(32));
			System.Diagnostics.Debug.WriteLine(q);
		}

		[TestMethod]
		public void encryptionsKeys() { 
			string q = @"<add key='EncryptionIV' value='{0}' />
				 <add key='EncryptionKey' value='{1}' />"
				.Replace("'", "\"")
				.Replace("{0}", CreateKey(8))
				.Replace("{1}", CreateKey(16));
			System.Diagnostics.Debug.WriteLine(q);
		}

	}
}
