﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using API.Data;
using waAssembly.Services.interfaces;

namespace CPS.Tests
{
	public class TestSessionHandler : ISessionContext
	{
		ObjectCache cache = MemoryCache.Default;

		public API.web.UserIdentityData CurrentUser
		{
			get
			{
				try
				{
					return new API.web.UserIdentityData() { Name = "dotnector", ID = 1 };
				} catch (Exception)
				{
					return null;
				}
			}
			set { }
		}

        public long? WorkstationID {
            get {
                return 1;
            }
            set { }
        }

        public object GetSession(enums.SessionKeys SessionKey)
		{
			if (cache.Contains(SessionKey.ToString()))
			{
				return cache[SessionKey.ToString()];
			}
			return null;
		}

		public T GetSession<T>(enums.SessionKeys SessionKey)
		{
			if (cache.Contains(SessionKey.ToString()))
			{
				try
				{
					var value = cache[SessionKey.ToString()];
					return (T)value;
				} catch (Exception)
				{
					return default(T);
				}
			}
			return default(T);
		}

		public string GetSessionString(enums.SessionKeys SessionKey)
		{
			var o = GetSession(SessionKey);
			if (o != null)
				return o.ToString();
			else
				return string.Empty;
		}

		public void SetSession(enums.SessionKeys SessionKey, object o)
		{
			if (cache.Contains(SessionKey.ToString()))
			{
				cache[SessionKey.ToString()] = o;
			}
			else
			{
				cache.Add(SessionKey.ToString(), o, new CacheItemPolicy() { SlidingExpiration = TimeSpan.FromMinutes(10) });
			}
		}

	}
}