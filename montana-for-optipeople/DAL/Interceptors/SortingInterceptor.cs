﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;

namespace API.Interceptors
{

	public class SortingInterceptor : IDbCommandTreeInterceptor
	{
		/// <summary>
		/// Columns to order by. First match on entity is used for sorting.
		/// </summary>
		public static readonly IList<String> ColumnNamesPriority = new ReadOnlyCollection<string>(new List<String> { "Priority", "SortOrder", "Name", "CreatedOnUtc desc" });

		public void TreeCreated(DbCommandTreeInterceptionContext interceptionContext)
		{
			if (interceptionContext.OriginalResult.DataSpace != DataSpace.CSpace)
			{
				return;
			}

			var queryCommand = interceptionContext.Result as DbQueryCommandTree;
			if (queryCommand != null)
			{
				interceptionContext.Result = HandleQueryCommand(queryCommand);
			}

		}

		private static DbCommandTree HandleQueryCommand(DbQueryCommandTree queryCommand)
		{
			var newQuery = queryCommand.Query.Accept(new SortingQueryVisitor());
			return new DbQueryCommandTree(
				 queryCommand.MetadataWorkspace,
				 queryCommand.DataSpace,
				 newQuery);
		}



		public class SortingQueryVisitor : DefaultExpressionVisitor
		{
			private string stripOrder(string v)
			{
				return (v.Contains(" ") ? v.Substring(0, v.IndexOf(" ")).Trim() : v);
			}
			public override DbExpression Visit(DbScanExpression expression)
			{
				var table = (EntityType)expression.Target.ElementType;
				string sortingColumn = string.Empty;
				foreach (var columnName in ColumnNamesPriority)
				{
					var strippedColumnName = stripOrder(columnName);  // strip any "desc" or "asc"
					if (table.Properties.Any(p => p.Name == strippedColumnName))
					{
						sortingColumn = columnName;
						break;
					}
				}

				if (string.IsNullOrEmpty(sortingColumn))
				{
					return base.Visit(expression);
				}


				var binding = expression.Bind();
				var sortExpression = binding
						.VariableType
						.Variable(binding.VariableName)
						.Property(stripOrder(sortingColumn))
						;

				return DbExpressionBuilder.Sort(binding,
					new[] {
								(sortingColumn.EndsWith(" desc")
									? DbExpressionBuilder.ToSortClauseDescending(sortExpression)
									: DbExpressionBuilder.ToSortClause(sortExpression)
								)
							}
				);

			}
		}

	}
}

