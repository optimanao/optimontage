﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.OrderParking orderparking_get_single(db.mtaOrderParking data)
        {
            var item = statics.Mapper.Map<API.Data.OrderParking>(data);
            item.ModifiedByUser = user_get_single(data.mtaUser);
            return item;
        }

        public API.Data.OrderParking OrderParkingGetSingle(string Barcode)
        {
            db.mtaOrderParking item = context.mtaOrderParkings.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                return orderparking_get_single(item);
            }
            return null;
        }

        public List<API.Data.OrderParking> OrderParkingGetCollection()
        {
            List<API.Data.OrderParking> collection = new List<API.Data.OrderParking>();

            var codes = StatusCodeGetCollection(true);
            var items = context.mtaOrderParkings;

            foreach (var item in items)
            {
                var entity = orderparking_get_single(item);
                if (entity.StatusCodeID.HasValue && codes.Any(m => m.ID == entity.StatusCodeID.Value)) {
                    entity.StatusCode = codes.First(m => m.ID == entity.StatusCodeID.Value);
                }
                collection.Add(entity);
            }
            return collection;
        }

        public IEnumerable<Tuple<int, string>> OrderParkingGetCollectionForAndOn(DateTime From, DateTime To)
        {
            List<API.Data.OrderParking> collection = new List<API.Data.OrderParking>();

            var groups = context.mtaOrderParkings
                .Where(m => m.StatusCodeID.HasValue)
                .Where(m => m.ModifiedOnUtc >= From && m.ModifiedOnUtc < To)
                .GroupBy(m => m.StatusCodeID.Value);

            var codes = StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 1); // error
            var codeIds = codes.Select(m => m.ID).ToList();

            groups = groups.Where(m => m.Any(n => codeIds.Contains(n.StatusCodeID ?? 0)));
            
            foreach (var group in groups) {
                yield return new Tuple<int, string>(group.Count(), codes.First(m => m.ID == group.Key).Name);
            }
        }

        public API.Data.OrderParking OrderParkingSave(API.Data.OrderParking savedata)
        {
            var existing = context.mtaOrderParkings.FirstOrDefault(n => n.Barcode == savedata.Barcode);
            if (existing != null)
            {
                savedata.ID = existing.ID; // to prevent EF hick-up
                statics.Mapper.Map(savedata, existing);
                context.SaveChanges();
                return orderparking_get_single(existing);
            }

            // new
            var data = new db.mtaOrderParking();
            statics.Mapper.Map(savedata, data);
            context.mtaOrderParkings.Add(data);
            context.SaveChanges();
            return orderparking_get_single(data);
        }

        //public void OrderParkingDeletePermanently(long ID)
        //{
        //    var item = context.mtaOrderParkings.FirstOrDefault(n => n.ID == ID);
        //    if (item != null)
        //    {
        //        context.mtaOrderParkings.Remove(item);
        //        context.SaveChanges();
        //    }
        //}

        public void OrderParkingDeletePermanently(string Barcode)
        {
            var item = context.mtaOrderParkings.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                context.mtaOrderParkings.Remove(item);
                context.SaveChanges();
            }
        }

        public bool OrderParkingExists(string Barcode)
        {
            return context.mtaOrderParkings.Any(m => m.Barcode == Barcode);
        }

        public List<string> OrderParkingBarcodes() {
            return context.mtaOrderParkings.Select(m => m.Barcode).ToList();
        }

    }
}
