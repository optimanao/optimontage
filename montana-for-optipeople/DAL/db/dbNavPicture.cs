﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.NavPicture navPicture_get_single(db.NavPicture data)
        {
            var item = statics.Mapper.Map<API.Data.NavPicture>(data);
            return item;
        }

        public API.Data.NavPicture NavPictureGetSingle(string Barcode, bool LoadDimensions = false)
        {
            db.NavPicture item = navContext.NavPictures.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                return navPicture_get_single(item);
            }
            return null;
        }

    }
}
