﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Product product_get_single(db.mtaProduct data)
        {
            var item = statics.Mapper.Map<API.Data.Product>(data);
            return item;
        }

        public API.Data.Product ProductGetSingle(long ID)
        {
            db.mtaProduct item = context.mtaProducts.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return product_get_single(item);
            }
            return null;
        }

        public List<API.Data.Product> ProductGetCollection()
        {
            List<API.Data.Product> collection = new List<API.Data.Product>();

            var items = context.mtaProducts;
            foreach (var item in items)
            {
                collection.Add(product_get_single(item));
            }
            return collection;
        }

        public API.Data.Product ProductSave(API.Data.Product savedata)
        {
            API.Data.Product product = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaProduct();
                statics.Mapper.Map(savedata, data);
                context.mtaProducts.Add(data);
                context.SaveChanges();
                product = product_get_single(data);
            }
            else
            { // existing
                var data = context.mtaProducts.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return product;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.Product ProductAdd(API.Data.Product savedata)
        {
            API.Data.Product product = savedata;
            var data = new db.mtaProduct();
            statics.Mapper.Map(savedata, data);
            context.mtaProducts.Add(data);
            context.SaveChanges();
            product = product_get_single(data);
            return product;
        }

        public List<API.Data.Product> ProductSearch(string Phrase) {

            List<API.Data.Product> collection = new List<API.Data.Product>();

            var items = context.mtaProducts
                .AsQueryable()
                //.SoftDelete(this)
                .Where(m => m.Name.Contains(Phrase));
            foreach (var item in items.Take(50))
            {
                collection.Add(product_get_single(item));
            }
            return collection;
        }

        public void ProductDeletePermanently(long ID)
        {
            var item = context.mtaProducts.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaProducts.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
