﻿using API.Data.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.TimeLog timelog_get_single(db.mtaTimeLog data, bool LoadRelated = false)
        {
            var item = statics.Mapper.Map<API.Data.TimeLog>(data);
            if (LoadRelated) {
                if (data.StatusCodeID.HasValue) {
                    item.StatusCode = StatusCodeGetSingle(data.StatusCodeID.Value);
                }
                if (data.mtaWorkstation != null) {
                    item.Workstation = workstation_get_single(data.mtaWorkstation);
                }
                if (data.mtaUser != null) {
                    item.User = user_get_single(data.mtaUser);
                }
            }
            return item;
        }

        public API.Data.TimeLog TimeLogGetSingle(long ID)
        {
            db.mtaTimeLog item = context.mtaTimeLogs.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return timelog_get_single(item);
            }
            return null;
        }

        public API.Data.TimeLog TimeLogGetSingleMostRecent(long UserID)
        {
            db.mtaTimeLog item = context.mtaTimeLogs.OrderByDescending(m => m.StartTimeUtc).FirstOrDefault(m => m.UserID == UserID); 
            if (item != null)
            {
                return timelog_get_single(item);
            }
            return null;
        }

        public API.Data.TimeLog TimeLogGetRunning(long UserID)
        {
            db.mtaTimeLog item = context.mtaTimeLogs.FirstOrDefault(n => n.UserID == UserID && n.InProgress);
            if (item != null)
            {
                var log = timelog_get_single(item);
                if (log != null && item.StatusCodeID.HasValue)
                {
                    log.StatusCode = StatusCodeGetSingle(item.StatusCodeID.Value, true);
                }
                return log;
            }
            return null;
        }

        public List<API.Data.TimeLog> TimeLogGetCollectionRunning(long UserID)
        {
            var collection = new List<API.Data.TimeLog>();
            var items = context.mtaTimeLogs.Where(n => n.UserID == UserID && n.InProgress);
            foreach (var item in items)
            {
                var log = timelog_get_single(item);
                if (log != null && item.StatusCodeID.HasValue)
                {
                    log.StatusCode = StatusCodeGetSingle(item.StatusCodeID.Value, true);
                }
                collection.Add(log);
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollection()
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs;
            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item));
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollection(string Barcode)
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs.Where(m => m.Barcode == Barcode);
            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item));
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollectionForStats(string Barcode)
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs.Where(m => m.Barcode == Barcode);
            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item, true));
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollection(long UserID, DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs.Where(m => m.UserID == UserID 
                && DbFunctions.TruncateTime(m.StartTimeUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.StartTimeUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item));
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollection(DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs.Where(m => DbFunctions.TruncateTime(m.StartTimeUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.StartTimeUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item));
            }
            return collection;
        }

        public List<API.Data.TimeLog> TimeLogGetCollectionByWorkstation(long WorkstationID, DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.TimeLog> collection = new List<API.Data.TimeLog>();

            var items = context.mtaTimeLogs.Where(m => m.WorkstationID == WorkstationID
                && DbFunctions.TruncateTime(m.StartTimeUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.StartTimeUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(timelog_get_single(item));
            }
            return collection;
        }

        public API.Data.TimeLog TimeLogSave(API.Data.TimeLog savedata)
        {
            API.Data.TimeLog timelog = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaTimeLog();
                statics.Mapper.Map(savedata, data);
                context.mtaTimeLogs.Add(data);
                context.SaveChanges();
                timelog = timelog_get_single(data);
            }
            else
            { // existing
                var data = context.mtaTimeLogs.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return timelog;
        }

        public List<API.Data.TimeLog> TimeLogSearch(TimeLogSearchModel model)
        {
            var collection = new List<API.Data.TimeLog>();

            var dtStart = (model.DateFrom ?? DateTime.Now.AddMonths(-1)).Date;
            var dtEnd = (model.DateTo ?? DateTime.Now).Date.AddDays(1);

            var items = context.mtaTimeLogs.Where(m => m.StartTimeUtc >= dtStart && m.StartTimeUtc < dtEnd);
            if (model.UserID.HasValue)
            {
                items = items.Where(m => m.UserID == model.UserID.Value);
            }
            if (model.WorkstationID.HasValue)
            {
                items = items.Where(m => m.WorkstationID == model.WorkstationID.Value);
            }

            if (items.Any())
            {
                foreach (var item in items)
                {
                    collection.Add(timelog_get_single(item));
                }
                var userids = items.Select(m => m.UserID).Distinct();
                var workstationids = items.Select(m => m.WorkstationID).Distinct();
                var statuscodeids = items.Where(m => m.StatusCodeID.HasValue).Select(m => m.StatusCodeID.Value).Distinct();

                foreach (var id in userids)
                {
                    var user = UserGetSingle(id);
                    if (user != null)
                    {
                        foreach (var itemUpdate in collection.Where(m => m.UserID == id))
                        {
                            itemUpdate.User = user;
                        }
                    }
                }
                foreach (var id in workstationids)
                {
                    var workstation = WorkstationGetSingle(id);
                    if (workstation != null)
                    {
                        foreach (var itemUpdate in collection.Where(m => m.WorkstationID == id))
                        {
                            itemUpdate.Workstation = workstation;
                        }
                    }
                }
                foreach (var id in statuscodeids)
                {
                    var status = StatusCodeGetSingle(id, true);
                    if (status != null)
                    {
                        foreach (var itemUpdate in collection.Where(m => m.StatusCodeID == id))
                        {
                            itemUpdate.StatusCode = status;
                        }
                    }
                }
            }

            return collection;
        }

        /// <summary>
        /// Remove the "Completed" bit from any time logs related to Barcode
        /// </summary>
        /// <param name="Barcode"></param>
        public void TimeLogClearOrderCompleted(string Barcode) {
            foreach (var tl in context.mtaTimeLogs.Where(m => m.Barcode == Barcode && m.OrderCompleted)) {
                tl.OrderCompleted = false;
            }
            context.SaveChanges();
        }

        public void TimeLogDeletePermanently(long ID)
        {
            var item = context.mtaTimeLogs.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaTimeLogs.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
