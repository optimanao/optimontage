﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using API;

namespace API.Data {
	public partial class DAL {

		private Data.DictionarySection dictionarysection_get_single(db.mtaDictionarySection data) {
            var de = statics.Mapper.Map<Data.DictionarySection>(data);
            de.EntryCount = data.mtaDictionaryEntries.Count;
			return de;
		}

		public List<DictionarySection> DictionarySectionGetCollection() {
			List<DictionarySection> entries = new List<DictionarySection>();
			var datas = context.mtaDictionarySections.OrderBy(n => n.Name);
			if (datas.Any())
				foreach (var data in datas)
					entries.Add(dictionarysection_get_single(data));
			return entries;
			//return new List<DictionarySection>();
		}

		public DictionarySection DictionarySectionGetSingle(int ID) {
			var data = context.mtaDictionarySections.FirstOrDefault(n => n.ID == ID);
			return dictionarysection_get_single(data);
		}

		public void DictionarySectionDelete(int ID) {
			var item = DictionarySectionGetSingle(ID);
			if (item != null) {
				var prop = item.GetType().GetProperty("Enabled");
				if (prop != null)
					prop.SetValue(false, item, null);
				context.SaveChanges();
			}
		}

		public void DictionarySectionDeletePermanently(int ID) {
			var item = context.mtaDictionarySections.Find(ID);
			if (item != null) {
				context.mtaDictionarySections.Remove(item);
				context.SaveChanges();
			}
		}

		public Data.DictionarySection DictionarySectionSave(Data.DictionarySection savedata) {
			Data.DictionarySection language = savedata;
			if (savedata.ID == 0) { // new
				var data = new db.mtaDictionarySection();
                statics.Mapper.Map(savedata, data);
                context.mtaDictionarySections.Add(data);
				context.SaveChanges();
				language = dictionarysection_get_single(data);
			} else { // existing
				var data = context.mtaDictionarySections.Find(savedata.ID);
				if (data != null) {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
				}
			}
			return language;
		}

		/// <summary>
		/// Get a list of anonymous objects to return directly to javascript/json.
		/// ID = DictionarySection ID, Count = Key count
		/// </summary>
		/// <returns></returns>
		public List<object> DictionarySectionKeyCountCollection() {
			List<object> list = new List<object>();

			list.AddRange(context.mtaDictionarySections.Select(n => new { ID = n.ID, Count = n.mtaDictionaryEntries.Count }));

			// root
			list.Add(new { ID = 0, Count = context.mtaDictionaryEntries.Count(n => n.DictionarySectionID == null || n.DictionarySectionID == 0) });

			return list;
		}

	}
}
