﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {
        // todo: belongs in service
        public List<API.Data.Models.OrderInProgressModel> OrderGetCollectionInProgress()
        {
            List<Models.OrderInProgressModel> collection = new List<Models.OrderInProgressModel>();

            var items = context
                .mtaTimeLogs
                .Where(m => !string.IsNullOrEmpty(m.Barcode))
                .Where(m => !context.mtaOrders.Any(n => n.Barcode == m.Barcode && (n.IsCompleted || n.IsDiscarded)))
                .GroupBy(m => m.Barcode);

            Dictionary<long, User> dicUsers = new Dictionary<long, User>();
            Dictionary<long, Workstation> dicWorkstation = new Dictionary<long, Workstation>();
            foreach (var item in items)
            {
                var model = new Models.OrderInProgressModel();
                model.InProgress = item.Any(m => m.InProgress && m.StatusCodeID == null);
                model.Barcode = item.First().Barcode;

                var forUser = item.OrderByDescending(m => m.StartTimeUtc).First();
                model.ModifiedByUserID = forUser.UserID;
                if (!dicUsers.ContainsKey(forUser.UserID)) {
                    dicUsers.Add(forUser.UserID, user_get_single(forUser.mtaUser));
                }
                model.ModifiedByUser = dicUsers[forUser.UserID];  //model.ModifiedByUser = user_get_single(forUser.mtaUser);
                model.ModifiedOn = forUser.StartTimeUtc;

                model.WorkstationID = forUser.WorkstationID;
                if (!dicWorkstation.ContainsKey(forUser.WorkstationID))
                {
                    dicWorkstation.Add(forUser.WorkstationID, workstation_get_single(forUser.mtaWorkstation));
                }
                model.Workstation = dicWorkstation[forUser.WorkstationID];  //model.ModifiedByUser = user_get_single(forUser.mtaUser);

                collection.Add(model);
            }
            return collection;
        }

      

    }
}
