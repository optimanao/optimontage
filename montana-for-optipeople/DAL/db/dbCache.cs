﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Cache cache_get_single(db.mtaCache data)
        {
            var item = statics.Mapper.Map<API.Data.Cache>(data);
            return item;
        }

        public API.Data.Cache CacheGetSingle(string CacheKey)
        {
            db.mtaCache item = context.mtaCaches.FirstOrDefault(n => n.CacheKey == CacheKey);
            if (item != null)
            {
                return cache_get_single(item);
            }
            return null;
        }

        public List<API.Data.Cache> CacheGetCollection()
        {
            List<API.Data.Cache> collection = new List<API.Data.Cache>();

            var items = context.mtaCaches;
            foreach (var item in items)
            {
                collection.Add(cache_get_single(item));
            }
            return collection;
        }

        public API.Data.Cache CacheSave(API.Data.Cache savedata)
        {
            API.Data.Cache checklistitem = savedata;
            var data = context.mtaCaches.FirstOrDefault(n => n.CacheKey == savedata.CacheKey);
            if (data == null)
            { // new
                data = new db.mtaCache();
                statics.Mapper.Map(savedata, data);
                context.mtaCaches.Add(data);
                context.SaveChanges();
                checklistitem = cache_get_single(data);
            }
            else
            { // existing
                var did = data.ID;
                statics.Mapper.Map(savedata, data);
                data.ID = did;
                context.SaveChanges();
            }
            return checklistitem;
        }

        public void CacheDeletePermanently(long ID)
        {
            var item = context.mtaCaches.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaCaches.Remove(item);
                context.SaveChanges();
                string name = typeof(Cache).Name;
                context.SaveChanges();
            }
        }

        public void CacheDeletePermanently(string CacheKey)
        {
            var item = context.mtaCaches.FirstOrDefault(n => n.CacheKey == CacheKey);
            if (item != null)
            {
                context.mtaCaches.Remove(item);
                context.SaveChanges();
                string name = typeof(Cache).Name;
                context.SaveChanges();
            }
        }

    }
}
