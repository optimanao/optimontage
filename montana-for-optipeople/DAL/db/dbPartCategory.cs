﻿using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.PartCategory partCategory_get_single(db.mtaPartCategory data)
        {
            var item = statics.Mapper.Map<API.Data.PartCategory>(data);
            return item;
        }

        public API.Data.PartCategory PartCategoryGetSingle(long ID)
        {
            db.mtaPartCategory item = context.mtaPartCategories.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return partCategory_get_single(item);
            }
            return null;
        }

        public List<API.Data.PartCategory> PartCategoryGetCollection()
        {
            List<API.Data.PartCategory> collection = new List<API.Data.PartCategory>();

            var items = context.mtaPartCategories;
            foreach (var item in items)
            {
                collection.Add(partCategory_get_single(item));
            }
            return collection;
        }

        public API.Data.PartCategory PartCategorySave(API.Data.PartCategory savedata)
        {
            API.Data.PartCategory part = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaPartCategory();
                statics.Mapper.Map(savedata, data);
                context.mtaPartCategories.Add(data);
                context.SaveChanges();
                part = partCategory_get_single(data);
            }
            else
            { // existing
                var data = context.mtaPartCategories.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return part;
        }

        //public List<API.Data.PartCategory> PartCategorySearch(string Phrase)
        //{
        //    List<API.Data.PartCategory> collection = new List<API.Data.PartCategory>();

        //    var items = context.mtaPartCategories
        //        .AsQueryable()
        //        .Where(m => m.Name.Contains(Phrase) || m.Barcode == Phrase);
        //    foreach (var item in items.Take(50))
        //    {
        //        collection.Add(partCategory_get_single(item));
        //    }
        //    return collection;
        //}

        public void PartCategoryDeletePermanently(long ID)
        {
            var item = context.mtaPartCategories.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaPartCategories.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
