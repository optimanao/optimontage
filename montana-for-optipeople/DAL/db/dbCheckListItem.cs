﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.CheckListItem checklistitem_get_single(db.mtaCheckListItem data, bool LoadLocalizedValues = false)
        {
            var item = statics.Mapper.Map<API.Data.CheckListItem>(data);
            if (LoadLocalizedValues) {
                item.LocalizedValues = LocalizedValueGetCollection(item.GetType().Name, item.ID);
            }
            return item;
        }

        public API.Data.CheckListItem CheckListItemGetSingle(long ID, bool LoadLocalizedValues = false)
        {
            db.mtaCheckListItem item = context.mtaCheckListItems.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return checklistitem_get_single(item, LoadLocalizedValues);
            }
            return null;
        }

        public List<API.Data.CheckListItem> CheckListItemGetCollection(bool LoadLocalizedValues = false)
        {
            List<API.Data.CheckListItem> collection = new List<API.Data.CheckListItem>();

            var items = context.mtaCheckListItems;
            foreach (var item in items)
            {
                collection.Add(checklistitem_get_single(item, LoadLocalizedValues));
            }
            return collection;
        }

        public List<API.Data.CheckListItem> CheckListItemGetCollection(long ProductID, bool LoadLocalizedValues = false)
        {
            List<API.Data.CheckListItem> collection = new List<API.Data.CheckListItem>();

            var items = context.mtaCheckListItems.Where(m => m.mtaLinkProductCheckListItems.Any(n => n.ProductID == ProductID)).OrderBy(m => m.mtaLinkProductCheckListItems.FirstOrDefault().SortOrder);

            foreach (var item in items)
            {
                collection.Add(checklistitem_get_single(item, LoadLocalizedValues));
            }
            return collection;
        }

        public List<API.Data.CheckListItem> CheckListItemGetCollection(string ProductName, bool LoadLocalizedValues = false)
        {
            List<API.Data.CheckListItem> collection = new List<API.Data.CheckListItem>();

            var items = context.mtaCheckListItems
                    .Where(m => m.mtaLinkProductCheckListItems.Any(n => n.mtaProduct.Name == ProductName))
                    .OrderBy(m => m.mtaLinkProductCheckListItems.FirstOrDefault().SortOrder);

            foreach (var item in items)
            {
                collection.Add(checklistitem_get_single(item, LoadLocalizedValues));
            }
            return collection;
        }

        public API.Data.CheckListItem CheckListItemSave(API.Data.CheckListItem savedata)
        {
            API.Data.CheckListItem checklistitem = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaCheckListItem();
                statics.Mapper.Map(savedata, data);
                context.mtaCheckListItems.Add(data);
                context.SaveChanges();
                checklistitem = checklistitem_get_single(data);
            }
            else
            { // existing
                var data = context.mtaCheckListItems.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return checklistitem;
        }

        public List<API.Data.CheckListItem> CheckListItemSearch(string Phrase)
        {
            List<API.Data.CheckListItem> collection = new List<API.Data.CheckListItem>();
            string sourceTypeName = typeof(CheckListItem).ToString();

            var items = context.mtaCheckListItems
                .Select(m => new {
                    entity = m,
                    localizedValues = context.mtaLocalizedValues.Where(o => o.SourceEntityID == m.ID && o.SourceEntityName == sourceTypeName)
                })
                .Where(m => 
                m.entity.Name.Contains(Phrase) || m.localizedValues.Any(n => n.Text.Contains(Phrase)));

            foreach (var item in items.OrderBy(m => m.entity.Name).Take(50))
            {
                collection.Add(checklistitem_get_single(item.entity));
            }
            return collection;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.CheckListItem CheckListItemAdd(API.Data.CheckListItem savedata)
        {
            API.Data.CheckListItem checklistitem = savedata;
            var data = new db.mtaCheckListItem();
            statics.Mapper.Map(savedata, data);
            context.mtaCheckListItems.Add(data);
            context.SaveChanges();
            checklistitem = checklistitem_get_single(data);
            return checklistitem;
        }

        public void CheckListItemDeletePermanently(long ID)
        {
            var item = context.mtaCheckListItems.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaCheckListItems.Remove(item);
                context.SaveChanges();
                string name = typeof(CheckListItem).Name;
                foreach (var child in context.mtaLocalizedValues.Where(m => m.SourceEntityName == name && m.SourceEntityID == ID)) {
                    context.mtaLocalizedValues.Remove(child);
                }
                context.SaveChanges();
            }
        }

    }
}
