﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using API.dbExtensions;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Workstation workstation_get_single(db.mtaWorkstation data)
        {
            var item = statics.Mapper.Map<API.Data.Workstation>(data);
            return item;
        }

        public API.Data.Workstation WorkstationGetSingle(long ID)
        {
            db.mtaWorkstation item = context.mtaWorkstations.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return workstation_get_single(item);
            }
            return null;
        }

        public List<API.Data.Workstation> WorkstationGetCollection()
        {
            List<API.Data.Workstation> collection = new List<API.Data.Workstation>();

            var items = context.mtaWorkstations.AsQueryable().SoftDelete(this).OrderBy(m => m.Name);
            foreach (var item in items)
            {
                collection.Add(workstation_get_single(item));
            }
            return collection;
        }

        public API.Data.Workstation WorkstationSave(API.Data.Workstation savedata)
        {
            API.Data.Workstation workstation = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaWorkstation();
                statics.Mapper.Map(savedata, data);
                context.mtaWorkstations.Add(data);
                context.SaveChanges();
                workstation = workstation_get_single(data);
            }
            else
            { // existing
                var data = context.mtaWorkstations.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return workstation;
        }

        public List<API.Data.Workstation> WorkstationSearch(string Phrase)
        {
            List<API.Data.Workstation> collection = new List<API.Data.Workstation>();

            var items = context.mtaWorkstations
                .AsQueryable()
                .SoftDelete(this)
                .Where(m => m.Name.Contains(Phrase));
            foreach (var item in items.Take(50))
            {
                collection.Add(workstation_get_single(item));
            }
            return collection;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.Workstation WorkstationAdd(API.Data.Workstation savedata)
        {
            API.Data.Workstation workstation = savedata;
            var data = new db.mtaWorkstation();
            statics.Mapper.Map(savedata, data);
            context.mtaWorkstations.Add(data);
            context.SaveChanges();
            workstation = workstation_get_single(data);
            return workstation;
        }

        public void WorkstationDeletePermanently(int ID)
        {
            var item = context.mtaWorkstations.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaWorkstations.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
