﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.PartOrder partorder_get_single(db.mtaPartOrder data)
        {
            var item = statics.Mapper.Map<API.Data.PartOrder>(data);
            
            var ws = entityCache(data.WorkstationID, data.mtaWorkstation, "workstation_get_single");
            if (ws != null && ws is Workstation) {
                item.Workstation = ws as Workstation;
            }

            var part = entityCache(data.PartID, data.mtaPart, "part_get_single");
            if (part != null && part is Part) {
                item.Part = part as Part;
            }

            var user = entityCache(data.CreatedByUserID, data.mtaUser, "user_get_single");
            if (user != null && user is User)
            {
                item.CreatedByUser = user as User;
            }

            return item;
        }

        public API.Data.PartOrder PartOrderGetSingle(long ID)
        {
            db.mtaPartOrder item = context.mtaPartOrders.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return partorder_get_single(item);
            }
            return null;
        }

        public List<API.Data.PartOrder> PartOrderGetCollection()
        {
            List<API.Data.PartOrder> collection = new List<API.Data.PartOrder>();

            var items = context.mtaPartOrders;
            foreach (var item in items)
            {
                collection.Add(partorder_get_single(item));
            }
            return collection;
        }

        public List<API.Data.PartOrder> PartOrderGetCollection(long WorkstationID)
        {
            List<API.Data.PartOrder> collection = new List<API.Data.PartOrder>();

            var items = context.mtaPartOrders.Where(m => m.WorkstationID == WorkstationID);
            foreach (var item in items)
            {
                collection.Add(partorder_get_single(item));
            }
            return collection;
        }

        public List<API.Data.PartOrder> PartOrderGetCollection(long WorkstationID, long UserID)
        {
            List<API.Data.PartOrder> collection = new List<API.Data.PartOrder>();

            var items = context.mtaPartOrders.Where(m => m.WorkstationID == WorkstationID && m.CreatedByUserID == UserID);
            foreach (var item in items)
            {
                collection.Add(partorder_get_single(item));
            }
            return collection;
        }

        public List<API.Data.PartOrder> PartOrderGetCollectionForRunner()
        {
            List<API.Data.PartOrder> collection = new List<API.Data.PartOrder>();

            var items = context.mtaPartOrders.Where(m => !m.IsDelivered).OrderBy(m => m.mtaWorkstation.Name);
            foreach (var item in items)
            {
                collection.Add(partorder_get_single(item));
            }
            return collection;
        }

        public API.Data.PartOrder PartOrderSave(API.Data.PartOrder savedata)
        {
            API.Data.PartOrder partorder = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaPartOrder();
                statics.Mapper.Map(savedata, data);
                context.mtaPartOrders.Add(data);
                context.SaveChanges();
                partorder = partorder_get_single(data);
            }
            else
            { // existing
                var data = context.mtaPartOrders.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return partorder;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.PartOrder PartOrderAdd(API.Data.PartOrder savedata)
        {
            API.Data.PartOrder partorder = savedata;
            var data = new db.mtaPartOrder();
            statics.Mapper.Map(savedata, data);
            context.mtaPartOrders.Add(data);
            context.SaveChanges();
            partorder = partorder_get_single(data);
            return partorder;
        }

        public void PartOrderDeletePermanently(long ID)
        {
            var item = context.mtaPartOrders.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaPartOrders.Remove(item);
                context.SaveChanges();
            }
        }

        public PartOrder PartOrderUnprocessed(long UserID, long WorkstationID, long PartID) {
            var item = context.mtaPartOrders.FirstOrDefault(m =>
            m.CreatedByUserID == UserID
            && m.WorkstationID == WorkstationID
            && m.PartID == PartID
            && !m.IsDelivered
            && !m.IsPicked);

            if (item != null) {
                return partorder_get_single(item);
            }
            return null;
        }

    }
}
