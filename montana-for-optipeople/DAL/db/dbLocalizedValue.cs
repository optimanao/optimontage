﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.LocalizedValue localizedvalue_get_single(db.mtaLocalizedValue data)
        {
            var item = statics.Mapper.Map<API.Data.LocalizedValue>(data);
            item.Language = language_get_single(data.mtaLanguage);
            return item;
        }

        public API.Data.LocalizedValue LocalizedValueGetSingle(long ID)
        {
            db.mtaLocalizedValue item = context.mtaLocalizedValues.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return localizedvalue_get_single(item);
            }
            return null;
        }

        public List<API.Data.LocalizedValue> LocalizedValueGetCollection(string SourceEntityType, long SourceEntityID)
        {
            List<API.Data.LocalizedValue> collection = new List<API.Data.LocalizedValue>();

            var items = context.mtaLocalizedValues
                .Include(n => n.mtaLanguage)
                .Where(m => m.SourceEntityName == SourceEntityType && m.SourceEntityID == SourceEntityID);
            foreach (var item in items)
            {
                collection.Add(localizedvalue_get_single(item));
            }
            return collection;
        }

        public API.Data.LocalizedValue LocalizedValueSave(API.Data.LocalizedValue savedata)
        {
            API.Data.LocalizedValue localizedvalue = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaLocalizedValue();
                statics.Mapper.Map(savedata, data);
                context.mtaLocalizedValues.Add(data);
                context.SaveChanges();
                localizedvalue = localizedvalue_get_single(data);
            }
            else
            { // existing
                var data = context.mtaLocalizedValues.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return localizedvalue;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.LocalizedValue LocalizedValueAdd(API.Data.LocalizedValue savedata)
        {
            API.Data.LocalizedValue localizedvalue = savedata;
            var data = new db.mtaLocalizedValue();
            statics.Mapper.Map(savedata, data);
            context.mtaLocalizedValues.Add(data);
            context.SaveChanges();
            localizedvalue = localizedvalue_get_single(data);
            return localizedvalue;
        }

        public void LocalizedValueDeletePermanently(long ID)
        {
            var item = context.mtaLocalizedValues.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaLocalizedValues.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
