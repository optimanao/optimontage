﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using API.dbExtensions;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.StatusCode statuscode_get_single(db.mtaStatusCode data, bool LoadLocalizedValues = false)
        {
            var item = statics.Mapper.Map<API.Data.StatusCode>(data);
            if (data.mtaStatusCodeType != null) {
                item.StatusCodeType = statuscodetype_get_single(data.mtaStatusCodeType);
            }
            if (LoadLocalizedValues) {
                item.LocalizedValues = LocalizedValueGetCollection(item.GetType().Name, item.ID);
            }
            return item;
        }

        public API.Data.StatusCode StatusCodeGetSingle(long ID, bool LoadLocalizedValues = false)
        {
            db.mtaStatusCode item = context.mtaStatusCodes.AsQueryable().SoftDelete(this).FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return statuscode_get_single(item, LoadLocalizedValues);
            }
            return null;
        }

        public List<API.Data.StatusCode> StatusCodeGetCollection(bool LoadLocalizedValues = false)
        {
            List<API.Data.StatusCode> collection = new List<API.Data.StatusCode>();

            var items = context.mtaStatusCodes.AsQueryable().SoftDelete(this);
            foreach (var item in items)
            {
                collection.Add(statuscode_get_single(item, LoadLocalizedValues));
            }
            return collection;
        }

        public List<API.Data.StatusCode> StatusCodeSearch(string Phrase)
        {
            var collection = new List<API.Data.StatusCode>();

            var items = context.mtaStatusCodes
                .AsQueryable()
                .SoftDelete(this)
                .Where(m => m.Name.Contains(Phrase));
            foreach (var item in items.Take(50))
            {
                collection.Add(statuscode_get_single(item));
            }
            return collection;
        }

        public API.Data.StatusCode StatusCodeSave(API.Data.StatusCode savedata)
        {
            API.Data.StatusCode statuscode = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaStatusCode();
                statics.Mapper.Map(savedata, data);
                context.mtaStatusCodes.Add(data);
                context.SaveChanges();
                statuscode = statuscode_get_single(data);
            }
            else
            { // existing
                var data = context.mtaStatusCodes.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return statuscode;
        }

        public void StatusCodeDeletePermanently(long ID)
        {
            var item = context.mtaStatusCodes.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaStatusCodes.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
