﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Part part_get_single(db.mtaPart data)
        {
            var item = statics.Mapper.Map<API.Data.Part>(data);
            if (data.mtaPartCategory != null)
            {
                item.PartCategory = partCategory_get_single(data.mtaPartCategory);
            }
            return item;
        }

        public API.Data.Part PartGetSingle(long ID)
        {
            db.mtaPart item = context.mtaParts.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return part_get_single(item);
            }
            return null;
        }

        public List<API.Data.Part> PartGetCollection()
        {
            List<API.Data.Part> collection = new List<API.Data.Part>();

            var items = context.mtaParts;
            foreach (var item in items)
            {
                collection.Add(part_get_single(item));
            }
            return collection;
        }

        public API.Data.Part PartSave(API.Data.Part savedata)
        {
            API.Data.Part part = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaPart();
                statics.Mapper.Map(savedata, data);
                context.mtaParts.Add(data);
                context.SaveChanges();
                part = part_get_single(data);
            }
            else
            { // existing
                var data = context.mtaParts.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return part;
        }

        public List<API.Data.Part> PartSearch(string Phrase)
        {
            List<API.Data.Part> collection = new List<API.Data.Part>();

            var items = context.mtaParts
                .AsQueryable()
                .Where(m => m.Name.Contains(Phrase) || m.Barcode == Phrase);
            foreach (var item in items.Take(50))
            {
                collection.Add(part_get_single(item));
            }
            return collection;
        }

        public void PartDeletePermanently(long ID)
        {
            var item = context.mtaParts.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaParts.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
