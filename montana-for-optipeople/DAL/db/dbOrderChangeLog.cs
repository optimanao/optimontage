﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {
        private API.Data.OrderChangeLog orderchangelog_get_single(db.mtaOrderChangeLog data)
        {
            var item = statics.Mapper.Map<API.Data.OrderChangeLog>(data);
            if (data.mtaWorkstation != null) {
                item.Workstation = workstation_get_single(data.mtaWorkstation);
            }
            if (data.mtaUser != null) {
                item.CreatedByUser = user_get_single(data.mtaUser);
            }
            if (data.ParkedStatusCodeID.HasValue) {
                item.ParkedStatusCode = StatusCodeGetSingle(data.ParkedStatusCodeID.Value, true);
            }
            return item;
        }

        public API.Data.OrderChangeLog OrderChangeLogGetSingle(long ID)
        {
            db.mtaOrderChangeLog item = context.mtaOrderChangeLogs.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return orderchangelog_get_single(item);
            }
            return null;
        }

        //public List<API.Data.OrderChangeLog> OrderChangeLogGetCollection()
        //{
        //    List<API.Data.OrderChangeLog> collection = new List<API.Data.OrderChangeLog>();

        //    var items = context.mtaOrderChangeLogs;
        //    foreach (var item in items)
        //    {
        //        collection.Add(orderchangelog_get_single(item));
        //    }
        //    return collection;
        //}

        public List<API.Data.OrderChangeLog> OrderChangeLogGetCollection(string BarcodePrefix)
        {
            List<API.Data.OrderChangeLog> collection = new List<API.Data.OrderChangeLog>();

            var items = context.mtaOrderChangeLogs.Where(m => m.Barcode.StartsWith(BarcodePrefix));
            foreach (var item in items)
            {
                collection.Add(orderchangelog_get_single(item));
            }
            return collection;
        }


        public API.Data.OrderChangeLog OrderChangeLogSave(API.Data.OrderChangeLog savedata)
        {
            API.Data.OrderChangeLog timelog = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaOrderChangeLog();
                statics.Mapper.Map(savedata, data);
                context.mtaOrderChangeLogs.Add(data);
                context.SaveChanges();
                timelog = orderchangelog_get_single(data);
            }
            else
            { // existing
                var data = context.mtaOrderChangeLogs.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return timelog;
        }

        public IEnumerable<Tuple<int, string>> OrderChangeLogGetCollectionForAndOn(DateTime From, DateTime To)
        {
            List<API.Data.OrderChangeLog> collection = new List<API.Data.OrderChangeLog>();

            var groups = context.mtaOrderChangeLogs
                .Where(m => m.IsParked)
                .Where(m => m.CreatedOnUtc >= From && m.CreatedOnUtc < To)
                .Where(m => m.ParkedStatusCodeID.HasValue)
                .GroupBy(m => m.ParkedStatusCodeID.Value);

            var codes = StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 1); // error
            var codeIds = codes.Select(m => m.ID).ToList();

            groups = groups.Where(m => m.Any(n => codeIds.Contains(n.ParkedStatusCodeID ?? 0)));

            foreach (var group in groups)
            {
                yield return new Tuple<int, string>(group.Count(), codes.First(m => m.ID == group.Key).Name);
            }
        }

        public void OrderChangeLogDeletePermanently(long ID)
        {
            var item = context.mtaOrderChangeLogs.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaOrderChangeLogs.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
