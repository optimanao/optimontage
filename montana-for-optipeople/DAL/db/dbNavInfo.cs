﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using System.Drawing;
using System.IO;
using System.Data.SqlClient;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.NavInfo navInfo_get_single(db.Montana_Møbler_A_S_MON_info_for_OptiBox data, bool loadDimensions = false)
        {
            var item = statics.Mapper.Map<API.Data.NavInfo>(data);
            if (loadDimensions) {
                item.Dimensions = NavDimensionGetCollection(item.Barcode);
                item.Comments = NavCommentGetSingle(item.OrderNo.StripFromLast(".", true));
            }
            return item;
        }

        public API.Data.NavInfo NavInfoGetSingle(string Barcode, bool LoadDimensions = false)
        {
            db.Montana_Møbler_A_S_MON_info_for_OptiBox item = navContext.Montana_Møbler_A_S_MON_info_for_OptiBox.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                return navInfo_get_single(item, LoadDimensions);
            }
            return null;
        }

        //public List<API.Data.NavInfo> NavInfoGetCollection()
        //{
        //    List<API.Data.NavInfo> collection = new List<API.Data.NavInfo>();

        //    var items = navContext.NavInfos;
        //    foreach (var item in items)
        //    {
        //        collection.Add(navInfo_get_single(item));
        //    }
        //    return collection;
        //}

        //public API.Data.NavInfo NavInfoSave(API.Data.NavInfo savedata)
        //{
        //    API.Data.NavInfo navInfo = savedata;
        //    if (savedata.ID == 0)
        //    { // new
        //        var data = new db.Montana_Møbler_A_S_MON_info_for_OptiBox();
        //        statics.Mapper.Map(savedata, data);
        //        navContext.NavInfos.Add(data);
        //        navContext.SaveChanges();
        //        navInfo = navInfo_get_single(data);
        //    }
        //    else
        //    { // existing
        //        var data = navContext.NavInfos.FirstOrDefault(n => n.ID == savedata.ID);
        //        if (data != null)
        //        {
        //            statics.Mapper.Map(savedata, data);
        //            navContext.SaveChanges();
        //        }
        //    }
        //    return navInfo;
        //}

        public List<API.Data.NavInfo> NavInfoSearch(string Phrase)
        {
            List<API.Data.NavInfo> collection = new List<API.Data.NavInfo>();

            var parkedBarcodes = OrderParkingBarcodes();
            var completedBarcode = OrderGetCollection().Where(m => m.IsCompleted).Select(m => m.Barcode).ToList();

            var items = navContext.Montana_Møbler_A_S_MON_info_for_OptiBox
                .Where(m => m.Barcode.Contains(Phrase)) // order number included in beginning of barcode
                //.Where(m => !completedOrDisgardedBarcode.Contains(m.Barcode))
                .OrderByDescending(m => m.Barcode)
                .Take(100)
                ;

            foreach (var item in items)
            {
                if(completedBarcode.Contains(item.Barcode)) {
                    continue;
                }

                var entity = navInfo_get_single(item);
                entity.IsParked = parkedBarcodes.Contains(item.Barcode);
                collection.Add(entity);

                if (collection.Count >= 15)
                {
                    return collection;
                }
            }
            return collection;
        }

        public List<string> NavInfoGetProductNamesUnique()
        {
            var names = navContext.Montana_Møbler_A_S_MON_info_for_OptiBox.Select(m => m.Master).Distinct();
            return names.ToList();
        }

        /// <summary>
        /// This is read from NAV database to include even non-started orders
        /// </summary>
        /// <param name="Prefix"></param>
        /// <returns></returns>
        public List<API.Data.NavInfo> NavInfoGetCollectionByOrderPrefix(string Prefix)
        {
            List<API.Data.NavInfo> collection = new List<API.Data.NavInfo>();

            var items = navContext.Montana_Møbler_A_S_MON_info_for_OptiBox.Where(m => m.Order_no_.StartsWith(Prefix)).OrderBy(m => m.Barcode);
            foreach (var item in items)
            {
                collection.Add(navInfo_get_single(item));
            }
            return collection;
        }

        //public byte[] PictureFromNav(string Barcode, int PictureNo) {
        //    //Bitmap bit = null;
        //    using (var connection = new SqlConnection(this.ConnectionStringNav)) {
        //        connection.Open();
        //        string sql = $"SELECT [Picture {PictureNo}] FROM [Montana Møbler A_S$MON info for OptiBox] WHERE Barcode = @Barcode";
        //        using (SqlCommand command = new SqlCommand(sql, connection)) { 

        //            command.Parameters.AddWithValue("@Barcode", Barcode);
        //            byte[] img = (byte[])command.ExecuteScalar();
        //            connection.Close();
        //            return img;
        //            //MemoryStream str = new MemoryStream();
        //            //str.Write(img, 0, img.Length);
        //            //bit = new Bitmap(str);
        //        }

        //    }
        //    return null;
        //}

    }
}
