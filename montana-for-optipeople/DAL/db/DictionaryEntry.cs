﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using API;

namespace API.Data
{
    public partial class DAL
    {

        private Data.DictionaryEntry dictionaryentry_get_single(db.mtaDictionaryEntry data)
        {
            var de = statics.Mapper.Map<Data.DictionaryEntry>(data);
            de = statics.FromJson<Data.DictionaryEntry>(data.Values);
            de.ID = data.ID;
            if (data.mtaDictionarySection != null)
            {
                de.DictionarySectionName = data.mtaDictionarySection.Name;
            }
            return de;
        }

        public IEnumerable<DictionaryEntry> DictionaryEntryGetCollection()
        {
            List<DictionaryEntry> entries = new List<DictionaryEntry>();
            var datas = context.mtaDictionaryEntries;
            if (datas.Any())
                foreach (var data in datas)
                {
                    entries.Add(dictionaryentry_get_single(data));
                }
            return entries;
            //return new List<DictionaryEntry>();
        }

        public IEnumerable<DictionaryEntry> DictionaryEntryGetCollection(int SectionID = 0)
        {
            List<DictionaryEntry> entries = new List<DictionaryEntry>();
            var datas = context.mtaDictionaryEntries.Where(n => n.ID > 0);
            if (SectionID > 0)
            {
                datas = datas.Where(n => n.DictionarySectionID == SectionID);
            }
            if (datas.Any())
                foreach (var data in datas)
                    entries.Add(dictionaryentry_get_single(data));
            return entries;
            //return new List<DictionaryEntry>();
        }

        public DictionaryEntry DictionaryEntryGetSingle(int ID)
        {
            var data = context.mtaDictionaryEntries.FirstOrDefault(n => n.ID == ID);
            return dictionaryentry_get_single(data);
        }

        public DictionaryEntry DictionaryEntryGetSingle(string Key, int SectionID)
        {
            var data = context.mtaDictionaryEntries.FirstOrDefault(n => n.Key == Key && n.DictionarySectionID == SectionID);
            if (data == null)
                return null;

            return dictionaryentry_get_single(data);
        }

        public void DictionaryEntryDelete(int ID)
        {
            var item = DictionaryEntryGetSingle(ID);
            if (item != null)
            {
                var prop = item.GetType().GetProperty("Enabled");
                if (prop != null)
                    prop.SetValue(false, item, null);
                context.SaveChanges();
            }
        }

        public void DictionaryEntryDeletePermanently(int ID)
        {
            var item = context.mtaDictionaryEntries.Find(ID);
            if (item != null)
            {
                context.mtaDictionaryEntries.Remove(item);
                context.SaveChanges();
            }
        }

        public DictionaryEntry DictionaryEntrySave(DictionaryEntry savedata)
        {
            Data.DictionaryEntry de = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaDictionaryEntry();
                statics.Mapper.Map(savedata, data);
                savedata.Values = ""; // to avoid redundancy
                data.Values = statics.ToJson(savedata);
                if (data.DictionarySectionID == 0)
                {
                    data.DictionarySectionID = null;
                }
                context.mtaDictionaryEntries.Add(data);
                context.SaveChanges();
                de = dictionaryentry_get_single(data);
            }
            else
            { // existing
                var data = context.mtaDictionaryEntries.Find(savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    savedata.Values = ""; // to avoid redundancy
                    data.Values = statics.ToJson(savedata);
                    if (data.DictionarySectionID == 0)
                    {
                        data.DictionarySectionID = null;
                    }
                    context.SaveChanges();
                }
            }
            return de;
        }

        // todo: check that DicValue has the regex method to embed other items
        public IEnumerable<DictionaryEntry> DictionaryEntrySearch(string Phrase, int SectionID, bool SearchAllSections = true)
        {
            List<DictionaryEntry> entries = new List<DictionaryEntry>();

            var items = context.mtaDictionaryEntries.Where(n => n.ID > 0);
            if (!SearchAllSections && SectionID > 0)
            {
                items = items.Where(n => n.DictionarySectionID == SectionID);
            }
            else if (SearchAllSections)
            {
                items = items.Where(n => n.ID > 0);
            }
            else if (SectionID == 0)
            {
                items = items.Where(n => n.DictionarySectionID == null);
            }

            items = items.Where(n => n.Values.Contains(Phrase) || n.Key.Contains(Phrase));

            foreach (var item in items.OrderBy(n => n.Key))
            {
                entries.Add(dictionaryentry_get_single(item));
            }

            return entries;
        }

        /// <summary>
        /// Verifies that another entry with the same name in the same section doesn't already exist
        /// </summary>
        /// <param name="Entry"></param>
        public bool DictionaryEntryOkToSave(DictionaryEntry Entry)
        {
            int i1 = context.mtaDictionaryEntries.Where(n => n.Key == Entry.Key).Count();
            int i2 = context.mtaDictionaryEntries.Where(n => n.Key == Entry.Key && n.DictionarySectionID == null).Count();
            int i3 = context.mtaDictionaryEntries.Where(n => n.Key == Entry.Key && n.DictionarySectionID == null && n.ID != Entry.ID).Count();
            bool any = true;

            if (Entry.DictionarySectionID == null)
            {
                any = context.mtaDictionaryEntries.Any(n =>
                  n.ID != Entry.ID &&
                  n.Key == Entry.Key &&
                  n.DictionarySectionID == null
              );
            }
            else
            {
                any = context.mtaDictionaryEntries.Any(n =>
                      n.ID != Entry.ID &&
                      n.Key == Entry.Key &&
                      n.DictionarySectionID == Entry.DictionarySectionID
                  );
            }
            return !any;
        }

    }
}
