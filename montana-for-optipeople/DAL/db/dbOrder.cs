﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Order order_get_single(db.mtaOrder data)
        {
            var item = statics.Mapper.Map<API.Data.Order>(data);
            return item;
        }

        public API.Data.Order OrderGetSingle(string Barcode)
        {
            db.mtaOrder item = context.mtaOrders.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                return order_get_single(item);
            }
            return null;
        }

        /// <summary>
        /// Completed or discarded orders
        /// </summary>
        /// <returns></returns>
        public List<API.Data.Order> OrderGetCollection()
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var items = context.mtaOrders;
            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        public List<API.Data.Order> OrderGetCollection(long UserID, DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var items = context.mtaOrders.Where(m => m.CreatedByUserID == UserID
                && DbFunctions.TruncateTime(m.CreatedOnUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.CreatedOnUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        public List<API.Data.Order> OrderGetCollection(DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var items = context.mtaOrders.Where(m => DbFunctions.TruncateTime(m.CreatedOnUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.CreatedOnUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        public List<API.Data.Order> OrderGetCollectionByWorkstation(long WorkstationID, DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var items = context.mtaOrders.Where(m => m.WorkstationID == WorkstationID
                && DbFunctions.TruncateTime(m.CreatedOnUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.CreatedOnUtc) <= DateTo.Date
            );

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        /// <summary>
        /// Gets a list of orders that were completed on CompletionDate but underwent repair at some point.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="CompletionDate"></param>
        /// <returns></returns>
        public List<API.Data.Order> OrderGetCollectionRepaired(long UserID, DateTime CompletionDate)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var completions = context.mtaOrders.Where(m => 
                m.CreatedByUserID == UserID
                && m.IsCompleted
                && DbFunctions.TruncateTime(m.CreatedOnUtc) >= CompletionDate.Date
            ).Select(m => m.Barcode);

            var repairs = context
                .mtaTimeLogs
                .Where(m => m.IsRepair && completions.Contains(m.Barcode))
                .Select(m => m.Barcode)
                .Distinct();

            var items = context.mtaOrders.Where(m => repairs.Contains(m.Barcode));

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        public List<API.Data.Order> OrderSearchCompleted(string Phrase, int Take = 100)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var items = context
                .mtaOrders
                .Where(m => m.Barcode.Contains(Phrase))
                .OrderByDescending(m => m.CreatedOnUtc)
                .Take(100)
                .Select(n => new { order = n, user = context.mtaUsers.FirstOrDefault(m => m.ID == n.CreatedByUserID) });
            foreach (var item in items)
            {
                var o = order_get_single(item.order);
                if (item.user != null) {
                    o.CreatedByUserName = item.user.NameFull;
                }
                collection.Add(o);
            }
            return collection;
        }

        /// <summary>
        /// Gets a list of orders that were completed on CompletionDate but underwent repair at some point.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="CompletionDate"></param>
        /// <returns></returns>
        public List<API.Data.Order> OrderGetCollectionRepairedAndCompletedByUser(long UserID, DateTime CompletionDate)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var completions = context.mtaOrders.Where(m =>
                m.CreatedByUserID == UserID
                && m.IsCompleted
                && DbFunctions.TruncateTime(m.CreatedOnUtc) == CompletionDate.Date
            ).Select(m => m.Barcode).ToList();

            var repairs = context
                .mtaTimeLogs
                .Where(m => m.IsRepair && completions.Contains(m.Barcode))
                .GroupBy(m => m.Barcode)
                .Where(m => m.OrderByDescending(n => n.StartTimeUtc).FirstOrDefault().IsRepair)
                .Select(m => m.FirstOrDefault().Barcode)
                .Distinct()
                .ToList();

            var items = context.mtaOrders.Where(m => repairs.Contains(m.Barcode));

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        /// <summary>
        /// Gets a list of orders that were completed on CompletionDate but underwent repair at some point.
        /// </summary>
        /// <param name="UserID"></param>
        /// <param name="CompletionDate"></param>
        /// <returns></returns>
        public List<API.Data.Order> OrderGetCollectionRepairedAndCompletedAtWorkstation(long WorkstationID, DateTime CompletionDate)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var completions = context.mtaOrders.Where(m =>
                m.WorkstationID == WorkstationID
                && m.IsCompleted
                && DbFunctions.TruncateTime(m.CreatedOnUtc) == CompletionDate.Date
            ).Select(m => m.Barcode).ToList();

            var repairs = context
                .mtaTimeLogs
                .Where(m => m.IsRepair && completions.Contains(m.Barcode))
                .GroupBy(m => m.Barcode)
                .Where(m => m.OrderByDescending(n => n.StartTimeUtc).FirstOrDefault().IsRepair)
                .Select(m => m.FirstOrDefault().Barcode)
                .Distinct()
                .ToList();

            var items = context.mtaOrders.Where(m => repairs.Contains(m.Barcode));

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        /// <summary>
        /// Returns a list of Orders as a subset of BarcodesToCheck that has been repaired at any point
        /// </summary>
        /// <param name="BarcodesToCheck"></param>
        /// <returns></returns>
        public List<Order> OrderHasBeenRepaired(List<string> BarcodesToCheck)
        {
            List<API.Data.Order> collection = new List<API.Data.Order>();

            var repairs = context.mtaTimeLogs.Where(m => BarcodesToCheck.Contains(m.Barcode) && m.IsRepair).Select(m => m.Barcode).ToList();
            var items = context.mtaOrders.Where(m => repairs.Contains(m.Barcode));

            foreach (var item in items)
            {
                collection.Add(order_get_single(item));
            }
            return collection;
        }

        public API.Data.Order OrderSave(API.Data.Order savedata)
        {
            API.Data.Order order = savedata;
            var data = context.mtaOrders.FirstOrDefault(n => n.Barcode == savedata.Barcode);
            if (data != null)
            {
                statics.Mapper.Map(savedata, data);
                context.SaveChanges();
            }
            else
            { // new
                data = new db.mtaOrder();
                statics.Mapper.Map(savedata, data);
                context.mtaOrders.Add(data);
                context.SaveChanges();
                order = order_get_single(data);
            }
            return order;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.Order OrderAdd(API.Data.Order savedata)
        {
            API.Data.Order order = savedata;
            var data = new db.mtaOrder();
            statics.Mapper.Map(savedata, data);
            context.mtaOrders.Add(data);
            context.SaveChanges();
            order = order_get_single(data);
            return order;
        }

        public void OrderDeletePermanently(string Barcode)
        {
            var item = context.mtaOrders.FirstOrDefault(n => n.Barcode == Barcode);
            if (item != null)
            {
                context.mtaOrders.Remove(item);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Remove data marked order as completed
        /// </summary>
        /// <param name="Barcode"></param>
        public void OrdreClearCompleted(string Barcode) {
            var orders = context.mtaOrders.Where(m => m.Barcode == Barcode && m.IsCompleted);
            if (orders != null && orders.Any()) {
                foreach (var order in orders) {
                    context.mtaOrders.Remove(order);
                }
                context.SaveChanges();
            }
        }

    }
}
