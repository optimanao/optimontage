﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.NavComment navComment_get_single(db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Comment data)
        {
            var item = statics.Mapper.Map<API.Data.NavComment>(data);
            return item;
        }

        public NavComment NavCommentGetSingle(string OrderNo)
        {
            var item = navContext.Montana_Møbler_A_S_MON_Info_for_OptiBox_Comment
                .FirstOrDefault(n => n.Order_No_ == OrderNo)
                ;
            if (item != null) {
                return navComment_get_single(item);
            }
            return null;
        }

        //public List<API.Data.NavComment> NavCommentGetCollection(string Barcode)
        //{
        //    var collection = new List<API.Data.NavComment>();

        //    var items = navContext.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension.Where(n => n.Barcode == Barcode).OrderBy(m => m.Line_No_);
        //    foreach (var item in items) {
        //        collection.Add(navComment_get_single(item));
        //    }
        //    return collection;
        //}

    }
}
