﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using API;

namespace API.Data {
	public partial class DAL {

		private Data.Language language_get_single(db.mtaLanguage data) {
            var item = statics.Mapper.Map<API.Data.Language>(data);
            return item;
        }

		public Data.Language LanguageGetSingle(int ID) {
			db.mtaLanguage item = context.mtaLanguages.Find(ID);
			if (item != null) {
				return language_get_single(item);
			}
			return null;
		}

		public List<Data.Language> LanguageGetCollection() {
			List<Data.Language> collection = new List<Data.Language>();

			var items = context.mtaLanguages;
			foreach (var item in items) {
				collection.Add(language_get_single(item));
			} // using
			return collection;
		}

		public List<Data.Language> LanguageSearch(string SearchExpression, object[] Parameters, string OrderByExpression = "", int SkipCount = 0, int TakeCount = 0) {
			List<Data.Language> resultSet = new List<Data.Language>();
			var items = context.mtaLanguages.Where(SearchExpression, Parameters.ToArray());
			if (!String.IsNullOrEmpty(OrderByExpression))
				items = items.OrderBy(OrderByExpression);
			if (SkipCount > 0)
				items = items.Skip(SkipCount);
			if (TakeCount > 0)
				items = items.Take(TakeCount);
			if (items.Count() > 0)
				resultSet.AddRange(items.AsEnumerable().Select(n => language_get_single(n)));
			return resultSet;
		} // method

		public Data.Language LanguageSave(Data.Language savedata) {
			Data.Language language = savedata;
			if (savedata.ID == 0) { // new
				var data = new db.mtaLanguage();
                statics.Mapper.Map(savedata, data);
                context.mtaLanguages.Add(data);
				context.SaveChanges();
				language = language_get_single(data);
			} else { // existing
				var data = context.mtaLanguages.Find(savedata.ID);
				if (data != null) {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
				}
			}
			return language;
		}

		/// This method doesn't eval ID for updating/inserting. It will always insert
		public Data.Language LanguageAdd(Data.Language savedata) {
			Data.Language language = savedata;
			var data = new db.mtaLanguage();
            statics.Mapper.Map(savedata, data);
            context.mtaLanguages.Add(data);
			context.SaveChanges();
			language = language_get_single(data);
			return language;
		}

		public void LanguageDelete(int ID) {
			var item = LanguageGetSingle(ID);
			if (item != null) {
				var prop = item.GetType().GetProperty("Enabled");
				if (prop != null)
					prop.SetValue(false, item, null);
				context.SaveChanges();
			}
		}

		public void LanguageDeletePermanently(int ID) {
			var item = context.mtaLanguages.Find(ID);
			if (item != null) {
				context.mtaLanguages.Remove(item);
				context.SaveChanges();
			}
		}

	}
}
