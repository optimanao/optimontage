﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using API.dbExtensions;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.User user_get_single(db.mtaUser data)
        {
            var item = statics.Mapper.Map<API.Data.User>(data);
            return item;
        }

        public API.Data.User UserGetSingle(long ID)
        {
            db.mtaUser item = context.mtaUsers.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return user_get_single(item);
            }
            return null;
        }

        public List<API.Data.User> UserGetAll()
        {
            List<API.Data.User> collection = new List<API.Data.User>();

            var items = context.mtaUsers.OrderBy(m => m.NameFull);
            foreach (var item in items)
            {
                collection.Add(user_get_single(item));
            }
            return collection;
        }


        public List<API.Data.User> UserGetCollection()
        {
            List<API.Data.User> collection = new List<API.Data.User>();

            var items = context.mtaUsers.AsQueryable().SoftDelete(this).OrderBy(m => m.NameFull);
            foreach (var item in items)
            {
                collection.Add(user_get_single(item));
            }
            return collection;
        }

        public API.Data.User UserSave(API.Data.User savedata)
        {
            API.Data.User user = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaUser();
                statics.Mapper.Map(savedata, data);
                context.mtaUsers.Add(data);
                context.SaveChanges();
                user = user_get_single(data);
            }
            else
            { // existing
                var data = context.mtaUsers.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return user;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.User UserAdd(API.Data.User savedata)
        {
            API.Data.User user = savedata;
            var data = new db.mtaUser();
            statics.Mapper.Map(savedata, data);
            context.mtaUsers.Add(data);
            context.SaveChanges();
            user = user_get_single(data);
            return user;
        }

        public void UserDeletePermanently(long ID)
        {
            var item = context.mtaUsers.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaUsers.Remove(item);
                context.SaveChanges();
            }
        }

        public List<API.Data.User> UserSearch(string Phrase)
        {
            List<API.Data.User> collection = new List<API.Data.User>();

            var items = context.mtaUsers
                .AsQueryable()
                .SoftDelete(this)
                .Where(m => m.NameFull.Contains(Phrase) || m.JobTitle.Contains(Phrase))
                .OrderBy(m => m.NameFull);
            foreach (var item in items.Take(50))
            {
                collection.Add(user_get_single(item));
            }
            return collection;
        }

        public void UserClearRoles(long ID)
        {
            //var items = context.chiUserInRoles.Where(m => m.UserID == ID);
            //foreach (var item in items)
            //{
            //    context.chiUserInRoles.Remove(item);
            //}
            //context.SaveChanges();
        }

        public void UserAddRole(long UserID, int RoleID)
        {
            //var role = context.chiUserRoles.FirstOrDefault(m => m.ID == RoleID);
            //if (role == null)
            //{
            //    return;
            //}

            //var user = context.chiUsers.FirstOrDefault(m => m.ID == UserID);
            //if (user == null)
            //{
            //    return;
            //}

            //var item = context.chiUserInRoles.FirstOrDefault(m => m.UserID == UserID && m.UserRoleID == RoleID);
            //if (item != null)
            //{
            //    return;
            //}

            //context.chiUserInRoles.Add(new db.chiUserInRole()
            //{
            //    OutletID = null,
            //    UserID = UserID,
            //    UserRoleID = RoleID
            //});
            //context.SaveChanges();
        }

        /// <summary>
        /// Gets a list of unique Users that have logged time at the WorkstationID within period
        /// </summary>
        /// <param name="WorkstationID"></param>
        /// <param name="DateFrom"></param>
        /// <param name="DateTo"></param>
        /// <returns></returns>
        public List<User> UserGetCollectionFromWorkstation(long WorkstationID, DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.User> collection = new List<API.Data.User>();

            var items = context.mtaTimeLogs.Where(m => m.WorkstationID == WorkstationID
                && DbFunctions.TruncateTime(m.StartTimeUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.StartTimeUtc) <= DateTo.Date
            )
            .Select(m => m.mtaUser)
            .Distinct();

            foreach (var item in items)
            {
                collection.Add(user_get_single(item));
            }
            return collection;
        }

        /// <summary>
        /// Gets a list of unique Users that have logged time within period
        /// </summary>
        /// <param name="WorkstationID"></param>
        /// <param name="DateFrom"></param>
        /// <param name="DateTo"></param>
        /// <returns></returns>
        public List<User> UserGetCollectionByTimeLogPeriod(DateTime DateFrom, DateTime DateTo)
        {
            List<API.Data.User> collection = new List<API.Data.User>();

            var items = context.mtaTimeLogs.Where(m => DbFunctions.TruncateTime(m.StartTimeUtc) >= DateFrom.Date
                && DbFunctions.TruncateTime(m.StartTimeUtc) <= DateTo.Date
            )
            .Select(m => m.mtaUser)
            .Distinct();

            foreach (var item in items)
            {
                collection.Add(user_get_single(item));
            }
            return collection;
        }

    }
}
