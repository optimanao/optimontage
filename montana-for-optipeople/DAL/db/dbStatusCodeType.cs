﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.StatusCodeType statuscodetype_get_single(db.mtaStatusCodeType data)
        {
            var item = statics.Mapper.Map<API.Data.StatusCodeType>(data);
            return item;
        }

        public API.Data.StatusCodeType StatusCodeTypeGetSingle(long ID)
        {
            db.mtaStatusCodeType item = context.mtaStatusCodeTypes.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return statuscodetype_get_single(item);
            }
            return null;
        }

        public List<API.Data.StatusCodeType> StatusCodeTypeGetCollection()
        {
            List<API.Data.StatusCodeType> collection = new List<API.Data.StatusCodeType>();

            var items = context.mtaStatusCodeTypes.OrderBy(n => n.Name);
            foreach (var item in items)
            {
                collection.Add(statuscodetype_get_single(item));
            }
            return collection;
        }

        public API.Data.StatusCodeType StatusCodeTypeSave(API.Data.StatusCodeType savedata)
        {
            API.Data.StatusCodeType statuscodetype = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaStatusCodeType();
                statics.Mapper.Map(savedata, data);
                context.mtaStatusCodeTypes.Add(data);
                context.SaveChanges();
                statuscodetype = statuscodetype_get_single(data);
            }
            else
            { // existing
                var data = context.mtaStatusCodeTypes.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return statuscodetype;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.StatusCodeType StatusCodeTypeAdd(API.Data.StatusCodeType savedata)
        {
            API.Data.StatusCodeType statuscodetype = savedata;
            var data = new db.mtaStatusCodeType();
            statics.Mapper.Map(savedata, data);
            context.mtaStatusCodeTypes.Add(data);
            context.SaveChanges();
            statuscodetype = statuscodetype_get_single(data);
            return statuscodetype;
        }

        public void StatusCodeTypeDeletePermanently(long ID)
        {
            var item = context.mtaStatusCodeTypes.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaStatusCodeTypes.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
