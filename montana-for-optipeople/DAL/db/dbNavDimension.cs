﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.NavDimension navDimension_get_single(db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension data)
        {
            var item = statics.Mapper.Map<API.Data.NavDimension>(data);
            return item;
        }

        public List<API.Data.NavDimension> NavDimensionGetCollection(string Barcode)
        {
            var collection = new List<API.Data.NavDimension>();

            var items = navContext.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension.Where(n => n.Barcode == Barcode).OrderBy(m => m.Line_No_);
            foreach (var item in items) {
                collection.Add(navDimension_get_single(item));
            }
            return collection;
        }

        //public List<API.Data.NavDimension> NavDimensionGetCollection()
        //{
        //    List<API.Data.NavDimension> collection = new List<API.Data.NavDimension>();

        //    var items = navContext.NavDimensions;
        //    foreach (var item in items)
        //    {
        //        collection.Add(navDimension_get_single(item));
        //    }
        //    return collection;
        //}

        //public API.Data.NavDimension NavDimensionSave(API.Data.NavDimension savedata)
        //{
        //    API.Data.NavDimension navDimension = savedata;
        //    if (savedata.ID == 0)
        //    { // new
        //        var data = new db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension();
        //        statics.Mapper.Map(savedata, data);
        //        navContext.NavDimensions.Add(data);
        //        navContext.SaveChanges();
        //        navDimension = navDimension_get_single(data);
        //    }
        //    else
        //    { // existing
        //        var data = navContext.NavDimensions.FirstOrDefault(n => n.ID == savedata.ID);
        //        if (data != null)
        //        {
        //            statics.Mapper.Map(savedata, data);
        //            navContext.SaveChanges();
        //        }
        //    }
        //    return navDimension;
        //}

    }
}
