﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;
using API.dbExtensions;

namespace API.Data
{
    public partial class DAL
    {

        private API.Data.Setting setting_get_single(db.mtaSetting data)
        {
            var item = statics.Mapper.Map<API.Data.Setting>(data);
            return item;
        }

        public API.Data.Setting SettingGetSingle(long ID)
        {
            db.mtaSetting item = context.mtaSettings.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return setting_get_single(item);
            }
            return null;
        }

        public API.Data.Setting SettingGetSingle(string Name)
        {
            db.mtaSetting item = context.mtaSettings.FirstOrDefault(n => n.Name == Name);
            if (item != null)
            {
                return setting_get_single(item);
            }
            return null;
        }

        public List<API.Data.Setting> SettingGetCollection()
        {
            List<API.Data.Setting> collection = new List<API.Data.Setting>();

            var items = context.mtaSettings.AsQueryable().SoftDelete(this).OrderBy(m => m.Name);
            foreach (var item in items)
            {
                collection.Add(setting_get_single(item));
            }
            return collection;
        }

        public API.Data.Setting SettingSave(API.Data.Setting savedata)
        {
            API.Data.Setting Setting = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaSetting();
                statics.Mapper.Map(savedata, data);
                context.mtaSettings.Add(data);
                context.SaveChanges();
                Setting = setting_get_single(data);
            }
            else
            { // existing
                var data = context.mtaSettings.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return Setting;
        }
        
        public void SettingDeletePermanently(int ID)
        {
            var item = context.mtaSettings.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaSettings.Remove(item);
                context.SaveChanges();
            }
        }

    }
}
