﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Dynamic;
using System.Text;
using dotUtils;
using API;


namespace API.Data
{
    public partial class DAL
    {

        private API.Data.LinkProductCheckListItem linkproductchecklistitem_get_single(db.mtaLinkProductCheckListItem data)
        {
            var item = statics.Mapper.Map<API.Data.LinkProductCheckListItem>(data);
            return item;
        }

        public API.Data.LinkProductCheckListItem LinkProductCheckListItemGetSingle(long ID)
        {
            db.mtaLinkProductCheckListItem item = context.mtaLinkProductCheckListItems.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                return linkproductchecklistitem_get_single(item);
            }
            return null;
        }

        public List<API.Data.LinkProductCheckListItem> LinkProductCheckListItemGetCollection()
        {
            List<API.Data.LinkProductCheckListItem> collection = new List<API.Data.LinkProductCheckListItem>();

            var items = context.mtaLinkProductCheckListItems;
            foreach (var item in items)
            {
                collection.Add(linkproductchecklistitem_get_single(item));
            }
            return collection;
        }

        public API.Data.LinkProductCheckListItem LinkProductCheckListItemSave(API.Data.LinkProductCheckListItem savedata)
        {
            API.Data.LinkProductCheckListItem linkproductchecklistitem = savedata;
            if (savedata.ID == 0)
            { // new
                var data = new db.mtaLinkProductCheckListItem();
                statics.Mapper.Map(savedata, data);
                context.mtaLinkProductCheckListItems.Add(data);
                context.SaveChanges();
                linkproductchecklistitem = linkproductchecklistitem_get_single(data);
            }
            else
            { // existing
                var data = context.mtaLinkProductCheckListItems.FirstOrDefault(n => n.ID == savedata.ID);
                if (data != null)
                {
                    statics.Mapper.Map(savedata, data);
                    context.SaveChanges();
                }
            }
            return linkproductchecklistitem;
        }

        /// This method doesn't eval ID for updating/inserting. It will always insert
        public API.Data.LinkProductCheckListItem LinkProductCheckListItemAdd(API.Data.LinkProductCheckListItem savedata)
        {
            API.Data.LinkProductCheckListItem linkproductchecklistitem = savedata;
            var data = new db.mtaLinkProductCheckListItem();
            statics.Mapper.Map(savedata, data);
            context.mtaLinkProductCheckListItems.Add(data);
            context.SaveChanges();
            linkproductchecklistitem = linkproductchecklistitem_get_single(data);
            return linkproductchecklistitem;
        }

        public void LinkProductCheckListItemDeletePermanently(long ID)
        {
            var item = context.mtaLinkProductCheckListItems.FirstOrDefault(n => n.ID == ID);
            if (item != null)
            {
                context.mtaLinkProductCheckListItems.Remove(item);
                context.SaveChanges();
            }
        }

        public void LinkProductCheckListItemDeleteByProduct(long ProductID)
        {
            var items = context.mtaLinkProductCheckListItems.Where(m => m.ProductID == ProductID);
            foreach(var item in items)
            { 
                context.mtaLinkProductCheckListItems.Remove(item);
            }
            context.SaveChanges();
        }

    }
}

