//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace db
{
    using System;
    using System.Collections.Generic;
    
    public partial class mtaUser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public mtaUser()
        {
            this.mtaOrderParkings = new HashSet<mtaOrderParking>();
            this.mtaPartOrders = new HashSet<mtaPartOrder>();
            this.mtaTimeLogs = new HashSet<mtaTimeLog>();
            this.mtaOrderChangeLogs = new HashSet<mtaOrderChangeLog>();
        }
    
        public long ID { get; set; }
        public string NameFirst { get; set; }
        public string NameLast { get; set; }
        public Nullable<System.DateTime> DeletedOnUtc { get; set; }
        public string NameFull { get; set; }
        public string JobTitle { get; set; }
        public string Password { get; set; }
        public Nullable<bool> IsAdmin { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mtaOrderParking> mtaOrderParkings { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mtaPartOrder> mtaPartOrders { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mtaTimeLog> mtaTimeLogs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<mtaOrderChangeLog> mtaOrderChangeLogs { get; set; }
    }
}
