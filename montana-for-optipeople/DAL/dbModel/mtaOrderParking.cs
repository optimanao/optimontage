//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace db
{
    using System;
    using System.Collections.Generic;
    
    public partial class mtaOrderParking
    {
        public long ID { get; set; }
        public string Barcode { get; set; }
        public System.DateTime ModifiedOnUtc { get; set; }
        public long ModifiedByUserID { get; set; }
        public Nullable<long> StatusCodeID { get; set; }
        public Nullable<System.DateTime> ModifiedOnUtcDate { get; set; }
    
        public virtual mtaUser mtaUser { get; set; }
    }
}
