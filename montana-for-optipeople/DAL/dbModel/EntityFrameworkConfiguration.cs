﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API
{
	public class EntityFrameworkConfiguration : DbConfiguration
	{
		public EntityFrameworkConfiguration()
		{
			AddInterceptor(new Interceptors.SortingInterceptor());
			//AddInterceptor(new Interceptors.CreatedOnAndModifiedOnInterceptor());
		}
	}
}
