﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Dynamic;
using API.Data;

namespace API.dbExtensions
{
	public static class dbModelExtensions
	{
	
		public static IQueryable<T> SoftDelete<T>(this IQueryable<T> data, DAL dbInstance)
		{
			if (!dbInstance.SoftDeleteEnabled) {
				return data;
			}
			if (!dbInstance.SoftDeleteTypes.Contains(typeof(T)))
				return data;

			if(typeof(T).GetProperty("DeletedOnUtc") != null) {
				return data.Where("DeletedOnUtc == null");
			}

			return data;
		}

		public static T SoftDelete<T>(this T data, DAL dbInstance)
		{
			if (!dbInstance.SoftDeleteEnabled)
			{
				return data;
			}

			if (!dbInstance.SoftDeleteTypes.Contains(typeof(T)))
				return data;

			var prop = typeof(T).GetProperty("DeletedOnUtc");

			if (data != null && prop != null)
			{
				DateTime? val = prop.GetValue(data, null) as DateTime?;
				if (val.HasValue) {
					return default(T);
				}
			}

			return data;
		}

	}


}
