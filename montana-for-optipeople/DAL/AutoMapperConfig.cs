﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API.Data;
using API.Data.Models;

namespace API
{
	public class AutoMapperConfig
	{

		public static void ConfigureMaps()
		{
			var auConfig = new AutoMapper.MapperConfiguration(cfg =>
			{
				cfg.CreateMap<API.Data.DictionaryEntry, db.mtaDictionaryEntry>().ReverseMap();
                cfg.CreateMap<API.Data.DictionarySection, db.mtaDictionarySection>().ReverseMap();

                cfg.CreateMap<db.mtaTimeLog, API.Data.TimeLog>().ReverseMap();

                cfg.CreateMap<db.mtaUser, API.Data.User>().ReverseMap();
                cfg.CreateMap<UserEditModel, API.Data.User>().ReverseMap();

                cfg.CreateMap<db.mtaWorkstation, API.Data.Workstation>().ReverseMap();
                cfg.CreateMap<WorkstationEditModel, API.Data.Workstation>().ReverseMap();

                cfg.CreateMap<db.mtaCheckListItem, API.Data.CheckListItem>().ReverseMap();
                cfg.CreateMap<CheckListItemEditModel, API.Data.CheckListItem>().ReverseMap();

                cfg.CreateMap<db.mtaLinkProductCheckListItem, API.Data.LinkProductCheckListItem>().ReverseMap();
                cfg.CreateMap<db.mtaLocalizedValue, API.Data.LocalizedValue>().ReverseMap();

                cfg.CreateMap<db.mtaProduct, API.Data.Product>().ReverseMap();
                cfg.CreateMap<ProductEditModel, API.Data.Product>().ReverseMap();

                cfg.CreateMap<StatusCodeEditModel, API.Data.StatusCode>().ReverseMap();
                cfg.CreateMap<db.mtaStatusCode, API.Data.StatusCode>().ReverseMap();
                cfg.CreateMap<db.mtaStatusCodeType, API.Data.StatusCodeType>().ReverseMap();

                cfg.CreateMap<db.mtaOrder, API.Data.Order>().ReverseMap();
                cfg.CreateMap<db.mtaOrderParking, API.Data.OrderParking>().ReverseMap();

                cfg.CreateMap<db.mtaPart, API.Data.Part>().ReverseMap();
                cfg.CreateMap<db.mtaPartOrder, API.Data.PartOrder>().ReverseMap();

                cfg.CreateMap<db.mtaOrderChangeLog, OrderChangeLog>().ReverseMap();

                cfg.CreateMap<db.mtaPartCategory, PartCategory>().ReverseMap();

                // <nav>

                // nav info
                cfg.CreateMap<db.Montana_Møbler_A_S_MON_info_for_OptiBox, NavInfo>()
                .ForMember(dest => dest.OrderNo, opt => opt.MapFrom(src => src.Order_no_))
                .ForMember(dest => dest.ProductionWeekString, opt => opt.MapFrom(src => src.Prod__Series))
                .ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.MON_Department))
                .ForMember(dest => dest.RoutePlan, opt => opt.MapFrom(src => src.MON_Plan_for_Route))
                .ForMember(dest => dest.NormTime, opt => opt.MapFrom(src => src.Norm_time))
                ;

                cfg.CreateMap<NavInfo, db.Montana_Møbler_A_S_MON_info_for_OptiBox>()
                .ForMember(dest => dest.Order_no_, opt => opt.MapFrom(src => src.OrderNo))
                .ForMember(dest => dest.Prod__Series, opt => opt.MapFrom(src => src.ProductionWeekString))
                .ForMember(dest => dest.MON_Department, opt => opt.MapFrom(src => src.Department))
                .ForMember(dest => dest.MON_Plan_for_Route, opt => opt.MapFrom(src => src.RoutePlan))
                ;

                // nav dimension
                cfg.CreateMap<NavDimension, db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension>()
                .ForMember(dest => dest.Line_No_, opt => opt.MapFrom(src => src.LineNo));

                cfg.CreateMap<db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Dimension, NavDimension>()
                .ForMember(dest => dest.LineNo, opt => opt.MapFrom(src => src.Line_No_));

                // nav comment
                cfg.CreateMap<db.Montana_Møbler_A_S_MON_Info_for_OptiBox_Comment, NavComment>()
                .ForMember(dest => dest.SpecAtt, opt => opt.MapFrom(src => src.Spec__att_))
                .ForMember(dest => dest.ShippingComment, opt => opt.MapFrom(src => src.Shipping_Comment))
                .ForMember(dest => dest.Comment, opt => opt.MapFrom(src => src.Comment))
                .ReverseMap()
                ;

                // </nav>

                cfg.ValidateInlineMaps = false;
            });

			//auConfig.AssertConfigurationIsValid();

			API.statics.Mapper = auConfig.CreateMapper();

		}
	}
}