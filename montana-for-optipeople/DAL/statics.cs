﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Mail;
using System.Reflection;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using API.Data;
using System.Text.RegularExpressions;
using AutoMapper;
using NLog;
using dotUtils;
using System.Globalization;

namespace API {
	public static partial class statics {

        public static readonly Logger log = LogManager.GetCurrentClassLogger();
        public static IMapper Mapper;

        /// <summary>
        /// Byte array for a 1x1 pixel transparent PNG image
        /// </summary>
        public static byte[] BlankPngBytes = new byte[] { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82, 0, 0, 0, 1, 0, 0, 0, 1, 8, 6, 0, 0, 0, 31, 21, 196, 137, 0, 0, 0, 25, 116, 69, 88, 116, 83, 111, 102, 116, 119, 97, 114, 101, 0, 65, 100, 111, 98, 101, 32, 73, 109, 97, 103, 101, 82, 101, 97, 100, 121, 113, 201, 101, 60, 0, 0, 0, 16, 73, 68, 65, 84, 120, 218, 98, 248, 255, 255, 63, 3, 64, 128, 1, 0, 8, 252, 2, 254, 219, 162, 77, 22, 0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130 };


        public static decimal NormTimeMinutes { get { return 38; } }

        /*
		public static byte[] EncryptionKeyBytes {
			get {
				string str = EncryptionKey;
				byte[] bytes = new byte[str.Length * sizeof(char)];
				System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
				return bytes;

			}
		}*/
        public static bool AllDigits(string value)
		{
			return value.All(c => c >= '0' && c <= '9');
		}

        public static int WeekNumber(DateTime Date)
        {
            // Seriously cheat.  If its Monday, Tuesday or Wednesday, then it'll 
            // be the same week# as whatever Thursday, Friday or Saturday are,
            // and we always get those right
            DayOfWeek day = System.Globalization.CultureInfo.InvariantCulture.Calendar.GetDayOfWeek(Date);
            if (day >= DayOfWeek.Monday && day <= DayOfWeek.Wednesday)
            {
                Date = Date.AddDays(3);
            }

            // Return the week of our adjusted day
            return CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(Date, System.Globalization.CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);
        }

        public static DateTime FirstDateOfWeekISO8601(int year, int weekOfYear)
        {
            DateTime jan1 = new DateTime(year, 1, 1);
            int daysOffset = DayOfWeek.Thursday - jan1.DayOfWeek;

            // Use first Thursday in January to get first week of the year as
            // it will never be in Week 52/53
            DateTime firstThursday = jan1.AddDays(daysOffset);
            var cal = CultureInfo.CurrentCulture.Calendar;
            int firstWeek = cal.GetWeekOfYear(firstThursday, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            var weekNum = weekOfYear;
            // As we're adding days to a date in Week 1,
            // we need to subtract 1 in order to get the right date for week #1
            if (firstWeek == 1)
            {
                weekNum -= 1;
            }

            // Using the first Thursday as starting week ensures that we are starting in the right year
            // then we add number of weeks multiplied with days
            var result = firstThursday.AddDays(weekNum * 7);

            // Subtract 3 days from Thursday to get Monday, which is the first weekday in ISO8601
            return result.AddDays(-3);
        }

        /// <summary>
        /// Return number of seconds between argument and UTC time right now.
        /// </summary>
        /// <param name="StartTimeUtc"></param>
        /// <returns></returns>
        public static int Duration(DateTime StartTimeUtc) {
            return (int)DateTime.UtcNow.Subtract(StartTimeUtc).TotalSeconds;
        }

        public static class Cache {
            public static List<API.Data.Language> LanguagesActive()
            {
                List<API.Data.Language> langs = (List<API.Data.Language>)CacheHandler.GetItem(enums.CacheKeys.LanguagesActive);
                if (langs == null)
                {
                    using (var ipa = new DAL())
                    {
                        langs = ipa.LanguageGetCollection().Where(n => n.Active).OrderBy(n => n.Name).ToList();
                        CacheHandler.AddItem(enums.CacheKeys.LanguagesAll, langs, 120);
                    }
                }
                return langs;
            }

            public static List<API.Data.Language> LanguagesAll()
            {
                List<API.Data.Language> langs = (List<API.Data.Language>)CacheHandler.GetItem(enums.CacheKeys.LanguagesAll);
                if (langs == null)
                {
                    using (var ipa = new DAL())
                    {
                        langs = ipa.LanguageGetCollection().OrderBy(n => n.Name).ToList();
                        CacheHandler.AddItem(enums.CacheKeys.LanguagesAll, langs, 120);
                    }
                }
                return langs;
            }

        }


        public static class App {

			public static string Version
			{
				get
				{
					if (HttpContext.Current == null)
					{
						return Guid.NewGuid().ToString().Substring(0, 6);
					}
					var appInstance = HttpContext.Current.ApplicationInstance;
					var assemblyVersion = appInstance.GetType().BaseType.Assembly.GetName().Version;
					return assemblyVersion.ToString().Replace(".", "");
				}
			}

			public static object GetSession(enums.SessionKeys SessionKey) {
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[SessionKey.ToString()] != null) {
					return System.Web.HttpContext.Current.Session[SessionKey.ToString()];
				} else {
					return null;
				}
			}
			public static T GetSession<T>(enums.SessionKeys SessionKey)
			{
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[SessionKey.ToString()] != null)
				{
					var value = System.Web.HttpContext.Current.Session[SessionKey.ToString()];
					try
					{
						return (T)value;
					}
					catch (Exception)
					{
						return default(T);
					}
				}
				else
				{
					return default(T);
				}
			}
			public static string GetSessionString(enums.SessionKeys SessionKey) {
				var o = GetSession(SessionKey);
				if (o != null)
					return o.ToString();
				else
					return string.Empty;
			}
			public static void SetSession(enums.SessionKeys SessionKey, object o) {
				if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null) {
					var session = System.Web.HttpContext.Current.Session;
					if (session[SessionKey.ToString()] == null)
						session.Add(SessionKey.ToString(), o);
					else
						session[SessionKey.ToString()] = o;
				}
			}

			public static int MaxResultResultCount = 35;

			public static string GetSetting(enums.SettingsKeys SettingsKey) {
				return System.Configuration.ConfigurationManager.AppSettings[SettingsKey.ToString()];
			}
			public static void SetSetting(enums.SettingsKeys SettingsKey, string Value) {

				System.Configuration.Configuration config = System.Web.Configuration.WebConfigurationManager.OpenWebConfiguration("~");
				System.Configuration.KeyValueConfigurationElement setting = config.AppSettings.Settings[SettingsKey.ToString()];
				setting.Value = Value;
				config.Save();
			}

			public static string GetCulture() {
				return System.Threading.Thread.CurrentThread.CurrentCulture.Name;
			}
			public static string GetUICulture() {
				return System.Threading.Thread.CurrentThread.CurrentUICulture.Name;
			}
			// don't you dare! Use SetCulture in SiteController instead!
			///// <summary>
			///// Sets both CurrentCulture and CurrentUICulture
			///// </summary>
			///// <param name="Locale"></param>
			//public static void SetCultures(string Locale) {
			//	System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Locale);
			//	System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Locale);
			//}
			//public static void SetCulture(string Locale) {
			//	System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo(Locale);
			//}
			//public static void SetUICulture(string Locale) {
			//	System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(Locale);
			//}

			/// <summary>
			/// Gets the physical root path of the applications
			/// </summary>
			/// <returns></returns>
			//public static string RootPath {
			//	get {
			//		string rootPath = String.Empty;
			//		if (HttpContext.Current != null && HttpContext.Current.Server != null) {
			//			rootPath = HttpContext.Current.Server.MapPath("~/");
			//		}
			//		return rootPath;
			//	}
			//}
		}

		public static string SendEmailDefaultTemplate(string To, string Subject, string Teaser, string Heading, string Body) {
			string mailtemplate = System.IO.File.ReadAllText(System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/mailtemplate/default.htm"));

			MailMessage mail = new MailMessage();
			mail.Subject = Subject;
			mail.IsBodyHtml = true;

			//mail.From = new MailAddress(App.GetSetting(SettingsKeys.MailSenderAddress));
			if (To.Contains(";")) {
				foreach (string recipient in To.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries))
					mail.To.Add(recipient);
			} else
				mail.To.Add(To);



			if (mail.IsBodyHtml) {
				Body = Body.Replace("\n", "<br />");
			}

			string bodyhtml = mailtemplate
				.Replace("{topTeaser}", Teaser)
				.Replace("{heading}", Heading)
				.Replace("{content}", Body)
				.Replace("{companyName}", statics.App.GetSetting(enums.SettingsKeys.CompanyName))
				.Replace("{currentYear}", DateTime.Now.Year.ToString());


			mail.Body = bodyhtml;



			//mail.Attachments.Add(new Attachment(FileToAttach));

			try {
				using(var smtp = new SmtpClient()) {
					smtp.Timeout = 60 * 1000;
					smtp.Send(mail);
					mail.Dispose();
				}
				return String.Empty;
			} catch (Exception ex) {
				return ex.ToString();
			}
		}

		public static string RemoveAccent(this string txt) {
			byte[] bytes = System.Text.Encoding.GetEncoding("Cyrillic").GetBytes(txt);
			return System.Text.Encoding.ASCII.GetString(bytes);
		}

		public static string Slugify(this string phrase) {
			string str = phrase.RemoveAccent().ToLower();
			str = System.Text.RegularExpressions.Regex.Replace(str, @"[^a-z0-9\s-]", ""); // Remove all non valid chars          
			str = System.Text.RegularExpressions.Regex.Replace(str, @"\s+", " ").Trim(); // convert multiple spaces into one space  
			str = System.Text.RegularExpressions.Regex.Replace(str, @"\s", "-"); // //Replace spaces by dashes


			return str;
		}

		public static DataTable ToDataTable<T>(IList<T> list) {
			var table = new DataTable();
			var props = TypeDescriptor.GetProperties(typeof(T))
				//Dirty hack to make sure we only have system data types
				//i.e. filter out the relationships/collections
			.Cast<PropertyDescriptor>()
			.Where(propertyInfo => propertyInfo.PropertyType.Namespace.Equals("System"))
			.ToArray();
			foreach (var propertyInfo in props) {
				table.Columns.Add(propertyInfo.Name, Nullable.GetUnderlyingType(propertyInfo.PropertyType) ?? propertyInfo.PropertyType);
			}

			var values = new object[props.Length];
			foreach (var item in list) {
				for (var i = 0; i < values.Length; i++) {
					values[i] = props[i].GetValue(item);
				}

				table.Rows.Add(values);
			}
			return table;
		}

		public static string ToJson(object obj) {
			var serial = new JavaScriptSerializer();
			return serial.Serialize(obj);
		}

		public static object FromJson(string json) {
			var serial = new JavaScriptSerializer();
			return serial.Deserialize(json, typeof(Object));
		}

		public static T FromJson<T>(string json) /*where T : class*/ {
			var serial = new JavaScriptSerializer();
			return serial.Deserialize<T>(json);
		}

		public static DateTime FromUnix(double milliseconds) {
			return new DateTime(1970, 1, 1, 0, 0, 0).AddMilliseconds(milliseconds);
		}

		public static double ToUnix(DateTime dt) {
			return dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0)).TotalMilliseconds;
		}

		public static Dictionary DictionaryReload()
		{
			Dictionary dic = new Dictionary();
			using (var db = new API.Data.DAL())
			{
				dic.ParseEntries(db.DictionarySectionGetCollection(), db.DictionaryEntryGetCollection().ToList());
			}
			CacheHandler.AddItem("Dictionary", dic, 60);
			return dic;
		}


		public static string DicValue(string TranslationKey, string Locale = "")
		{
			Dictionary dic = (Dictionary)CacheHandler.GetItem("Dictionary");
			string value = "not translated: " + TranslationKey;

			if (dic == null)
			{
				dic = DictionaryReload();
				//dic = (Dictionary)CacheHandler.GetItem("Dictionary");
			}

			var entry = (dic != null ? dic.GetEntry(TranslationKey) : null);
			string culture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
			culture = (Locale == "" ? culture : Locale);

			if (entry != null)
			{

				value = entry.GetValue(culture);
				if (!entry.IsRichText)
				{
					value = value.Replace(Environment.NewLine, "<br />");
				}
			}

			if (String.IsNullOrEmpty(value))
				value = "not translated: " + TranslationKey;

			//foreach (Match m in Regex.Matches(value, "{@\\w.+}")) // this only work for one merge field pr. dic value
			foreach (Match m in Regex.Matches(value, @"{(?<=\{)[^}]*(?=\})}"))
			{
				var replacementKey = m.Value.Replace("{@", "")/*.Replace("{", "")*/.Replace("}", "");
				string repValue = dic.GetValue(replacementKey, culture);
				if (!repValue.StartsWith("not translated:") && !string.IsNullOrEmpty(repValue))
				{
					value = value.Replace(m.Value, repValue);
				}
			}

			return value;
		}

		// http://stackoverflow.com/questions/1984165/strong-typing-a-property-name-in-net/1984194#1984194
		// http://stackoverflow.com/questions/13074202/passing-strongly-typed-property-name-as-argument

		/// <summary>
		/// Returns the memberinfo of a strongly typed property. Usage GetMemberInfo(([ObjectType] s) => s.[PropertyName])
		/// </summary>
		/// <typeparam name="TObject"></typeparam>
		/// <typeparam name="TProperty"></typeparam>
		/// <param name="expression"></param>
		/// <returns></returns>
		public static MemberInfo GetMemberInfo<TObject, TProperty>(Expression<Func<TObject, TProperty>> expression) {
			var member = expression.Body as MemberExpression;
			if (member != null) {
				return member.Member;
			}
			throw new ArgumentException("Not a member!");
		}

		/// <summary>
		/// Returns the name of a strongly typed property. Usage GetMemberInfo(([ObjectType] s) => s.[PropertyName])
		/// </summary>
		/// <typeparam name="TObject"></typeparam>
		/// <typeparam name="TProperty"></typeparam>
		/// <param name="expression"></param>
		/// <returns></returns>
		public static string GetMemberName<TObject, TProperty>(Expression<Func<TObject, TProperty>> expression) {
			var member = expression.Body as MemberExpression;
			if (member != null) {
				return member.Member.Name;
			}
			throw new ArgumentException("Not a member!");
		}

		public static string ModelErrorsToString(System.Web.Mvc.ModelStateDictionary ModelState, System.Web.Mvc.ViewDataDictionary ViewData) {
			string errors = "";
			if (!ModelState.IsValid) {
				foreach (System.Web.Mvc.ModelState modelState in ViewData.ModelState.Values) {
					foreach (System.Web.Mvc.ModelError error in modelState.Errors) {
						errors += error.ErrorMessage + "\n" + (error.Exception != null ? error.Exception.Message : "") + "\n\n";
					}
				}
			}
			return errors;
		}

		/// <summary>
		/// Helper method. returns a list of <int, string> where int is enum value and string is enum name
		/// </summary>
		/// <param name="EnumType"></param>
		/// <returns></returns>
		public static List<KeyValuePair<int, string>> EnumPairs(Type EnumType) {
			var list = new List<KeyValuePair<int, string>>();
			foreach (var value in Enum.GetValues(EnumType)) {
				string name = Enum.GetName(EnumType, value);
				list.Add(new KeyValuePair<int, string>((int)value, name));
			}
			return list;
		}

		//TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.UtcNow, "some other timezone id");
	}
}
