﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using API;
using API.Data;

namespace API.web {
	public class UserIdentity : IIdentity, IPrincipal {
		private readonly FormsAuthenticationTicket _ticket;

		public UserIdentity(FormsAuthenticationTicket ticket) {
			_ticket = ticket;
			UserData = statics.FromJson<UserIdentityData>(ticket.UserData);
		}

		public string AuthenticationType {
			get {
				//throw new NotImplementedException();
				return "User";
			}
		}

		public bool IsAuthenticated {
			get {
				//throw new NotImplementedException();
				return true;
			}
		}

		public string Name {
			get { return _ticket.Name; }
		}

		public string UserId {
			get { return _ticket.UserData; }
		}

		public UserIdentityData UserData { get; set; }
		//public long ID { get; set; }
		//public string ID { get; set; }

		public bool IsInRole(string role) {
			return Roles.IsUserInRole(role);
		}

		public IIdentity Identity {
			get { return this; }
		}

	}

}