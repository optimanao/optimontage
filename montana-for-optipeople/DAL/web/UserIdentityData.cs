﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace API.web {
	public class UserIdentityData {

		public long ID { get; set; }
		//public string Email { get; set; }
        public string Name { get; set; }
		public string TimeZoneId { get; set; }

		/// <summary>
		/// In minutes...
		/// </summary>
		public int CurrentUtcOffset {
			get {
				if (String.IsNullOrEmpty(TimeZoneId))
					return 0;
				return (int)TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId)
					.GetUtcOffset(DateTime.UtcNow)
					.TotalMinutes;
			}
		}

		public DateTime ToLocalTime(DateTime UTC) {
			if (String.IsNullOrEmpty(TimeZoneId))
				return UTC;
			return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(UTC, TimeZoneId);
		}

		public long? ImpersonatingUserID { get; set; }

		[ScriptIgnore]
		public List<string> Roles { get; set; } = new List<string>();

        public bool HasRole(string Role)
        {
            return Roles != null && Roles.Contains(Role);
        }

        public bool HasRoleAll(params string[] UserRoles)
        {
            return Roles != null && UserRoles != null && (Roles.All(m => UserRoles.Contains(m)));
        }

        public bool HasRoleAny(params string[] UserRoles)
        {
            return Roles != null && UserRoles != null && (Roles.Intersect(UserRoles).Any());
        }

        public bool IsAdmin { get; set; }
    }
}
