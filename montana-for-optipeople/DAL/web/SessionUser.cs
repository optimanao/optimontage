﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Data;

namespace API.web {
	public class SessionUser {
		/// <summary>
		/// User ID
		/// </summary>
		public int ID { get; set; }
		public string Username { get; set; }
		public int CompanyID { get; set; }
		public int UserTypeID { get; set; }
		public enums.UserType UserTypeFromID { get { return (enums.UserType)UserTypeID; } }
	}
}
