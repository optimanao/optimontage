﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using db;
using System.Reflection;


// for catching entity validation errors :-) :-)
// http://stackoverflow.com/questions/15820505/dbentityvalidationexception-how-can-i-easily-tell-what-caused-the-error
namespace db {

	public partial class databaseEntities {
		public override int SaveChanges() {
			try {
				return base.SaveChanges();
			} catch (DbEntityValidationException ex) {
				// Retrieve the error messages as a list of strings.
				var errorMessages = ex.EntityValidationErrors
						  .SelectMany(x => x.ValidationErrors)
						  .Select(x => x.ErrorMessage);

				// Join the list to a single string.
				var fullErrorMessage = string.Join(";\n ", errorMessages);

				// Combine the original exception message with the new one.
				var exceptionMessage = string.Concat(ex.Message, " \nThe validation errors are:\n", fullErrorMessage);

				//API.statics.log.Fatal(exceptionMessage);

				// Throw a new DbEntityValidationException with the improved exception message.
				throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
			}
		}
	}
    public partial class NAV60Entities
    {
        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                          .SelectMany(x => x.ValidationErrors)
                          .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join(";\n ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " \nThe validation errors are:\n", fullErrorMessage);

                //API.statics.log.Fatal(exceptionMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    }
}

namespace API.Data {
    

	public partial class DAL : IDisposable {
		db.databaseEntities context = null;
        db.NAV60Entities navContext = null;

		public Type[] SoftDeleteTypes = new Type[] {
			typeof(db.mtaUser),
            typeof(db.mtaWorkstation),
            typeof(db.mtaStatusCode)
        };

		public bool SoftDeleteEnabled { get; set; } = true;

		private bool softDeleteActive(Type T)
		{
			return SoftDeleteEnabled && SoftDeleteTypes.Contains(T);
		}

        // <local entity caching>
        Dictionary<Type, List<DbCacheEntity>> dicCache = new Dictionary<Type, List<DbCacheEntity>>();

        private object entityCache(object EntityKey, object Entity, string retrieverMethodName) {
            if (Entity == null) {
                return null;
            }
            var t = Entity.GetType();
            if (!dicCache.ContainsKey(t)) {
                dicCache.Add(t, new List<DbCacheEntity>());
            }
            var list = dicCache[t];
            var listFirst = list.FirstOrDefault(m => m.EntityKey == ("" + EntityKey));
            if (listFirst != null) {
                return listFirst.Entity;
            } else {
                var method = this.GetType().GetMethod(retrieverMethodName, BindingFlags.NonPublic | BindingFlags.Instance);
                if (method == null) {
                    return null;
                }

                var newEntity = method.Invoke(this, new object[] { Entity });
                if (newEntity != null)
                {
                    var newCache = new DbCacheEntity() { EntityKey = "" + EntityKey, Entity = newEntity };
                    list.Add(newCache);
                }
                return newEntity;
            }
        }

        // </local entity caching>

        public DAL() {
			context = new db.databaseEntities();
            navContext = new db.NAV60Entities();
            context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;"); // 2019-03-21: getting deadlock on timelog table
        }

		public db.databaseEntities getContext() {
			return context;
		}
		
		/// <summary>
		/// resets the db context
		/// </summary>
		private void resetContext() {
			context.Dispose();
			context = new db.databaseEntities();
            context.Database.ExecuteSqlCommand("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;"); // 2019-03-21: getting deadlock on timelog table

            navContext.Dispose();
            navContext = new db.NAV60Entities();
		}

		/// <summary>
		/// Enable to write sql statements and such to debug window
		/// </summary>
		/// <param name="Enabled"></param>
		public void Log(bool Enabled) {
			if (Enabled) {
				context.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
			} else {
				context.Database.Log = null;
			}
		}
		
		public string ConnectionString { get { return context.Database.Connection.ConnectionString; } }

        public string ConnectionStringNav { get { return navContext.Database.Connection.ConnectionString; } }

        public void SQLExecuteNonQuery(string SQL) {
			using (SqlConnection conn = new SqlConnection(context.Database.Connection.ConnectionString)) {
				conn.Open();
				using (SqlCommand com = new SqlCommand(SQL, conn)) {
					com.ExecuteNonQuery();
				}
				conn.Close();
			}
		}

		public void Dispose() {
			context.Dispose();
		}

        private class DbCacheEntity
        {
            public string EntityKey { get; set; }
            public object Entity { get; set; }
        }
       
    }
}
