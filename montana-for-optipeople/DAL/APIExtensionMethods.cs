﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace API {
	public static class APIExtensionMethods {
		/// <summary>
		/// Removes the passed value from the end of the string
		/// </summary>
		/// <param name="Value"></param>
		/// <param name="Remove"></param>
		/// <returns></returns>
		public static string StripFromEnd(this string Value, string Remove) {
			if (Value.EndsWith(Remove))
				return Value.Substring(0, Value.Length - Remove.Length);
			else
				return Value;
		}

		/// <summary>
		/// Removes characters from the the last index of the passed value
		/// </summary>
		/// <param name="Value"></param>
		/// <param name="Remove"></param>
		/// <returns></returns>
		public static string StripFromLast(this string Value, string Remove, bool AlsoStripLastOccurence = false) {
            if (Value == null || string.IsNullOrEmpty(Value)) {
                return Value;
            }
            string r = Value;
            if (Value.Contains(Remove))
            {
                r = Value.Substring(0, Value.LastIndexOf(Remove) + Remove.Length);
                if (AlsoStripLastOccurence) {
                    r = StripFromEnd(r, Remove);
                }
                return r;
            }
            else { 
                return Value;
            }
        }

		/// <summary>
		/// Transform character at index to uppercase
		/// </summary>
		/// <param name="Value"></param>
		/// <param name="LetterIndex"></param>
		/// <returns></returns>
		public static string ToUpperCase(this string Value, int LetterIndex) {
			if (Value.Length < LetterIndex + 1)
				return Value;

			string ret = Value.Substring(0, LetterIndex) 
				+ Value.Substring(LetterIndex, 1).ToUpper() 
				+ Value.Substring(LetterIndex + 1);
			
			return ret;
		}
		
		/// <summary>
		/// Transform character at index to lowercase
		/// </summary>
		/// <param name="Value"></param>
		/// <param name="LetterIndex"></param>
		/// <returns></returns>
		public static string ToLowerCase(this string Value, int LetterIndex) {
			if (Value.Length < LetterIndex + 1)
				return Value;

			string ret = Value.Substring(0, LetterIndex) 
				+ Value.Substring(LetterIndex, 1).ToLower() 
				+ Value.Substring(LetterIndex + 1);

			return ret;
		}
		
		public static object CloneObject(this object objSource, int level = 0) {
			//Get the type of source object and create a new instance of that type
			Type typeSource = objSource.GetType();
			object objTarget = Activator.CreateInstance(typeSource);

			//Get all the properties of source object type
			PropertyInfo[] propertyInfo = typeSource.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);

			//Assign all source property to taget object 's properties
			foreach (PropertyInfo property in propertyInfo) {
				//Check whether property can be written to
				if (property.CanWrite) {
					//check whether property type is value type, enum or string type
					if (property.PropertyType.IsValueType || property.PropertyType.IsEnum || property.PropertyType.Equals(typeof(System.String))) {
						property.SetValue(objTarget, property.GetValue(objSource, null), null);
					}
						//else property type is object/complex types, so need to recursively call this method until the end of the tree is reached
					else {
						object objPropertyValue = property.GetValue(objSource, null);
						if (objPropertyValue == null) {
							property.SetValue(objTarget, null, null);
						} else {
							if (level <= 0)
								property.SetValue(objTarget, objPropertyValue.CloneObject(level++), null);
						}
					}
				}
			}
			return objTarget;
		}

		//public static bool HasType(this API.Data.User User, enums.UserType UserType) {
		//	if (User == null || User.ID == 0)
		//		return false;
		//	return User.UserTypeID == (int)UserType;
		//}

		//public static bool HasType(this API.Data.User User, enums.UserType[] UserTypes) {
		//	if (User == null || User.ID == 0)
		//		return false;
		//	var ut = (enums.UserType)User.UserTypeID;
		//	return UserTypes.Contains(ut);
		//}


		public static string ToDefString(this DateTime dt, bool IncludeTimeStamp = false) {
			var date = dt;
			if (dt == null)
				date = DateTime.Now;

			return dt.ToString("yyyy-MM-dd" + (IncludeTimeStamp ? " HH:mm" : ""));
		}

        public static string ToDefStringLocalTime(this DateTime dtUtc, bool IncludeTimeStamp = false)
        {
            var date = dtUtc;
            if (dtUtc == null) { 
                date = DateTime.UtcNow;
            }

            date = new DateTime(date.Ticks, DateTimeKind.Utc);

            // convert time to local
            date = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(date, "Central European Standard Time");

            return date.ToString("yyyy-MM-dd" + (IncludeTimeStamp ? " HH:mm" : ""));
        }

        public static object TryGetPropertyValue(this object Obj, string PropertyName) {
			if (Obj == null)
				return null;

			var prop = Obj.GetType().GetProperty(PropertyName);
			if (prop == null)
				return null;
			else
				return prop.GetValue(Obj, null);
		}

		public static void TrySetPropertyValue(this object Obj, string PropertyName, object Value) {
			if (Obj == null)
				return;

			var prop = Obj.GetType().GetProperty(PropertyName);
			if (prop != null)
				prop.SetValue(Obj, Value, null);
		}

		public static int ToUnixTime(this DateTime Obj) {
			return (int)Obj.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
		}

		public static DateTime FromUnixTime(this int Obj) {
			return (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).AddSeconds(Obj);
		}

	}
}