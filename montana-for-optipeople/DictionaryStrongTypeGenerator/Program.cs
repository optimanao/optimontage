﻿// version "2.0" - from workflows
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using API.Data;

namespace DictionaryStrongTypeGenerator
{
    class Program
    {
        static List<DictionaryEntry> entries = new List<DictionaryEntry>();
        static List<DictionarySection> sections = new List<DictionarySection>();
        static string basepath = @"E:\Development\clients\optipeople\montana\webapp\";
        static string appname = "DALData";

        static string[] reservedWords = new string[] { "abstract", "as", "base", "bool", "break", "byte", "case", "catch", "char", "checked", "class", "const", "continue", "decimal", "default", "delegate", "do", "double", "else", "enum", "event", "explicit", "extern", "false", "finally", "fixed", "float", "for", "forech", "goto", "if", "implicit", "in", "int", "interface", "internal", "is", "lock", "long", "namespace", "new", "null", "object", "operator", "out", "override", "params", "private", "protected", "public", "readonly", "ref", "return", "sbyte", "sealed", "short", "sizeof", "stackalloc", "static", "string", "struct", "switch", "this", "throw", "true", "try", "typeof", "uint", "ulong", "unchecked", "unsafe", "ushort", "using", "virtual", "volatile", "void", "while" };

        static Dictionary<int, List<string>> dicIdVsKeys { get; set; } = new Dictionary<int, List<string>>();

        static void w(object q)
        {
            Console.WriteLine(q);
        }

        static void Main(string[] args)
        {
            API.AutoMapperConfig.ConfigureMaps();
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            DirectoryInfo di = new DirectoryInfo(path);

            di = di.Parent.Parent.Parent.Parent;

            if (!System.IO.Directory.Exists(Path.Combine(di.FullName, appname)))
            {
                return;
            }

            Console.WriteLine("starting...");

            using (var ipa = new API.Data.DAL())
            {

                Console.WriteLine("loading sections...");
                sections = ipa.DictionarySectionGetCollection();

                Console.WriteLine("loading all entries...");
                entries = ipa.DictionaryEntryGetCollection().ToList();

                string filename = Path.Combine(di.FullName, appname + "\\TKey.cs");

                var fs = System.IO.File.CreateText(filename);
                fs.WriteLine("using System;");
                fs.WriteLine("using API;");
                fs.WriteLine("namespace API.Data {");

                fs.WriteLine("public static class TKeys {");

                int iCounter = 0;
                int iCounterTotal = entries.Count(n => n.DictionarySectionID == 0);

                foreach (var entry in entries.Where(n => (n.DictionarySectionID ?? 0) == 0))
                {
                    Console.Write("\r" + (++iCounter).ToString().PadLeft(iCounterTotal.ToString().Length, '0') + "/" + iCounterTotal);

                    if (!dicIdVsKeys.ContainsKey(entry.ID))
                    {
                        dicIdVsKeys.Add(entry.ID, new List<string>());
                    }
                    dicIdVsKeys[entry.ID].Add(entry.Key);

                    if (!string.IsNullOrEmpty(entry.Notes))
                    {
                        fs.WriteLine("/// <summary>");
                        fs.WriteLine("/// " + entry.Notes);
                        fs.WriteLine("/// </summary>");
                    }

                    string fixedKeyName = entry.Key + (reservedWords.Contains(entry.Key) ? "_" : "");

                    //fs.WriteLine("public static readonly DicKey " + entry.Key + " = new DicKey() { Name = \"" + keyName(entry) + "\" };");
                    fs.WriteLine("public const string " + fixedKeyName + " = \"" + keyName(entry) + "\";");
                }

                foreach (var section in sections.Where(n => n.ParentID == 0).OrderBy(n => n.Name))
                {
                    recurseSection(sections, entries, ref fs, section);
                }

                iCounter = 0;
                iCounterTotal = sections.Count(n => n.ParentID == 0);
                Console.WriteLine("");

                // 2nd attempt - make every key available in the level maintaining uniqueness
                foreach (var section in sections.Where(n => n.ParentID == 0).OrderBy(n => n.Name))
                {
                    Console.Write("\r" + (++iCounter).ToString().PadLeft(iCounterTotal.ToString().Length, '0') + "/" + iCounterTotal);
                    recurseSection2(sections, entries, ref fs, section);
                }

                fs.WriteLine("}");
                fs.WriteLine("}");

                fs.Dispose();
            }

            outputLog();
        } // Main

        static string keyName(DictionaryEntry entry)
        {
            if (!entry.DictionarySectionID.HasValue || entry.DictionarySectionID.Value == 0)
                return entry.Key;

            string key = entry.Key;
            int sectionid = entry.DictionarySectionID.Value;
            if (!sections.Any(n => n.ID == sectionid))
                return key;

            while (sections.Any(n => n.ID == sectionid))
            {
                var sec = sections.FirstOrDefault(n => n.ID == sectionid);
                key = sec.Name + "." + key;
                sectionid = sec.ParentID;
            }
            return key;
        }

        static void recurseSection(List<DictionarySection> sections, IEnumerable<DictionaryEntry> entries, ref StreamWriter fs, DictionarySection curSection)
        {


            fs.WriteLine("public static class " + curSection.Name + " {");

            foreach (var entry in entries.Where(n => n.DictionarySectionID == curSection.ID))
            {
                if (!dicIdVsKeys.ContainsKey(entry.ID))
                {
                    dicIdVsKeys.Add(entry.ID, new List<string>());
                }
                dicIdVsKeys[entry.ID].Add(entry.Key);

                if (!string.IsNullOrEmpty(entry.Notes))
                {
                    fs.WriteLine("/// <summary>");
                    fs.WriteLine("/// " + entry.Notes);
                    fs.WriteLine("/// </summary>");
                }

                string fixedKeyName = entry.Key + (reservedWords.Contains(entry.Key) ? "_" : "");

                fs.WriteLine("public const string " + fixedKeyName + " = \"" + keyName(entry) + "\";");
            }

            if (sections.Any(n => n.ParentID == curSection.ID))
            {
                foreach (var childSection in sections.Where(n => n.ParentID == curSection.ID))
                {
                    recurseSection(sections, entries, ref fs, childSection);
                }
            }
            fs.WriteLine("}");

        }

        static void recurseSection2(List<DictionarySection> sections, IEnumerable<DictionaryEntry> entries, ref StreamWriter fs, DictionarySection curSection)
        {

            string sectionName = curSection.Name;
            int parentid = curSection.ParentID;
            int fallbackCounter = 0;
            while (parentid > 0 && fallbackCounter < 4)
            {
                var parentSection = sections.FirstOrDefault(n => n.ID == parentid);
                if (parentSection == null)
                    break;

                sectionName = parentSection.Name + sectionName;
                parentid = parentSection.ParentID;
            }

            foreach (var entry in entries.Where(n => n.DictionarySectionID == curSection.ID))
            {

                if (!string.IsNullOrEmpty(entry.Notes))
                {
                    fs.WriteLine("/// <summary>");
                    fs.WriteLine("/// " + entry.Notes);
                    fs.WriteLine("/// </summary>");
                }
                var entryKeyName = entry.Key.Replace(".", "_");
                entryKeyName = sectionName + entryKeyName.Substring(0, 1).ToUpper() + entryKeyName.Substring(1);

                //fs.WriteLine("public static readonly DicKey " + entryKeyName + " = new DicKey() { Name = \"" + keyName(entry) + "\" };");
                fs.WriteLine("public const string " + entryKeyName + " = \"" + keyName(entry) + "\";");
                if (!dicIdVsKeys.ContainsKey(entry.ID))
                {
                    dicIdVsKeys.Add(entry.ID, new List<string>());
                }
                dicIdVsKeys[entry.ID].Add(entryKeyName);
            }

            if (sections.Any(n => n.ParentID == curSection.ID))
            {
                foreach (var childSection in sections.Where(n => n.ParentID == curSection.ID))
                {
                    recurseSection2(sections, entries, ref fs, childSection);
                }
            }

        }

        static void outputLog()
        {
            Dictionary<string, int> dicHitCount = new Dictionary<string, int>();

            int counter = 0;
            string cache = "";
            var paths = new List<string>() {
                @"waAssembly\",
                @"DAL\",
                @"DALData\"
            };

            List<string> replacements = new List<string>() {
                "DicValue(\"{0}\")", "DicValue(TKeys.{0})", "Html.T(TKeys.{0})",
                "DicValue(\"{0}\", ", "DicValue(TKeys.{0}, ", "Html.T(TKeys.{0}, " // "locale" variations
				, "[TransDisplayName(TKeys.{0})]"
             };
            List<string> files = new List<string>();
            foreach (var path in paths)
            {
                w("Loading .cshtml file names from " + path);
                files.AddRange(System.IO.Directory.GetFiles(basepath + path, "*.cshtml", SearchOption.AllDirectories));
                files.AddRange(System.IO.Directory.GetFiles(basepath + path, "*.cs", SearchOption.AllDirectories));
            }

            foreach (var file in files)
            {
                w("caching file " + ++counter + "/" + files.Count + ": " + file);

                if (System.IO.Path.GetFileName(file).ToLower() == "tkey.cs")
                {
                    continue; // don't eval output file...dummy
                }

                cache += System.IO.File.ReadAllText(file);
            }

            foreach (var id in dicIdVsKeys.Keys)
            {
                w("dic key: " + id);
                var entry = entries.FirstOrDefault(m => m.ID == id);
                if (entry == null)
                {
                    continue;
                }

                foreach (var value in dicIdVsKeys[id])
                {
                    string searchValue = "Html.T(TKeys." + keyName(entry) + ")";

                    foreach (var replacement in replacements)
                    {
                        searchValue = String.Format(replacement, keyName(entry));
                        if (!dicHitCount.ContainsKey(keyName(entry)))
                        {
                            dicHitCount.Add(keyName(entry), 0);
                        }
                        while (cache.Contains(searchValue))
                        {
                            dicHitCount[keyName(entry)] += 1;
                            string tCache = cache.Substring(0, cache.IndexOf(searchValue)) + cache.Substring(cache.IndexOf(searchValue) + searchValue.Length);
                            cache = tCache;
                        }
                    }

                    if (entry.Clientside && dicHitCount[keyName(entry)] == 0)
                    {
                        dicHitCount[keyName(entry)] = 999;
                    }

                    if (entry.DictionarySectionID.HasValue)
                    {
                        var section = sections.FirstOrDefault(m => m.ID == entry.DictionarySectionID);
                        if (section == null)
                        {
                            continue;
                        }
                        var entryKeyName = entry.Key.Replace(".", "_");
                        entryKeyName = section.Name + entryKeyName.Substring(0, 1).ToUpper() + entryKeyName.Substring(1);

                        foreach (var replacement in replacements)
                        {
                            searchValue = String.Format(replacement, entryKeyName);

                            if (!dicHitCount.ContainsKey(keyName(entry)))
                            {
                                dicHitCount.Add(keyName(entry), 0);
                            }
                            while (cache.Contains(searchValue))
                            {
                                dicHitCount[keyName(entry)] += 1;
                                string tCache = cache.Substring(0, cache.IndexOf(searchValue)) + cache.Substring(cache.IndexOf(searchValue) + searchValue.Length);
                                cache = tCache;
                                //cache = cache.Replace(searchValue, "-");
                            }
                        }
                    }


                }
            }

            using (var fs = System.IO.File.CreateText(basepath + @"dictionaryKeyUsage.txt"))
            {
                w("writing output file");
                foreach (var key in dicHitCount.Keys.OrderBy(m => m))
                {
                    fs.WriteLine(key + ": " + dicHitCount[key]);
                }
            }

            using (var fs = System.IO.File.CreateText(basepath + @"dictionaryKeyUsageZero.txt"))
            {
                w("writing output file with orphans");
                foreach (var key in dicHitCount.Keys.OrderBy(m => m))
                {
                    if (dicHitCount[key] == 0)
                    {
                        fs.WriteLine(key);
                    }
                }
            }


        }


    }
}



