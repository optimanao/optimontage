﻿using API.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        public Bitmap CopyDataToBitmap(byte[] data, PixelFormat pixelFormat)
        {
            
            //Here create the Bitmap to the know height, width and format
            Bitmap bmp = new Bitmap(352, 288, pixelFormat);
            try
            {
                //Create a BitmapData and Lock all pixels to be written 
                BitmapData bmpData = bmp.LockBits(
                             new Rectangle(0, 0, bmp.Width, bmp.Height),
                             ImageLockMode.WriteOnly, bmp.PixelFormat);

                //Copy the data from the byte array into BitmapData.Scan0
                Marshal.Copy(data, 0, bmpData.Scan0, data.Length);


                //Unlock the pixels
                bmp.UnlockBits(bmpData);
            }
            catch (Exception) {

            }

            //Return the bitmap 
            return bmp;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            string t = @"E:\Development\clients\tye\tye_dk_2.0\img\x.png";

            var b = System.IO.File.ReadAllBytes(t);
            string q = "";

            foreach (var bit in b) {
                q += bit + ",";
            }

            string w = "";
            //var dal = new DAL();

            //byte[] bytes = dal.PictureFromNav("79226100100000004001111", 1);
            //var bitmap = CopyDataToBitmap(bytes);

            //pictureBox1.Image = bitmap;
            //return;
            //using (var ms = new MemoryStream(bytes))
            //{
            //    pictureBox1.Image = Image.FromStream(ms);
            //}

            //dal.Dispose();
        }

        private Image fixit(byte[] arr, PixelFormat pixelFormat) {
            int width = 400;
            int height = 400;

            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(width, height, pixelFormat);

            System.Drawing.Imaging.BitmapData bmd = bmp.LockBits(new System.Drawing.Rectangle(0, 0, width, height), System.Drawing.Imaging.ImageLockMode.WriteOnly, pixelFormat);

            Marshal.Copy(arr, 0, bmd.Scan0, arr.Length);
            bmp.UnlockBits(bmd);
            using (MemoryStream ms = new MemoryStream())
            {
                bmp.Save(ms, System.Drawing.Imaging.ImageFormat.Png); // here it is the trick
                arr = ms.GetBuffer(); // now I replace my incorrect image format bytes for the correct ones
                System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true, false);
                return image;
                //image.Save(, System.Drawing.Imaging.ImageFormat.Png);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var dal = new DAL();

            byte[] bytes = dal.PictureFromNav("80315001000000001001112", 1);

            //using (var ms = new MemoryStream(bytes.Skip(4).ToArray()))
            //{
            //    using (var gzip = new GZipStream(ms, CompressionMode.Decompress, true))
            //    {
            //        pictureBox1.Image = Image.FromStream(gzip);
            //    }
            //}


            //for (var i = 1; i < 5; i++) {
            //    try {
            //        using (Stream input = new DeflateStream(new MemoryStream(bytes.Skip(i).ToArray()), CompressionMode.Decompress))
            //        {
            //            using (MemoryStream output = new MemoryStream())
            //            {
            //                //input.CopyTo(output);
            //                pictureBox1.Image = Image.FromStream(input, true, false);

            //            }
            //        }
            //    } catch (Exception ex) {
            //        MessageBox.Show("Error " + i + " " + ex.Message);
            //    }
            //}

            //return;

            foreach (PixelFormat pf in Enum.GetValues(typeof(PixelFormat))) {
                label1.Text = "" + pf;
                Bitmap bitmap = null;

                if (pf == PixelFormat.Format1bppIndexed) {
                    continue;
                }
                if (pf == PixelFormat.Format16bppGrayScale)
                {
                    continue;
                }

                

                try
                {
                    var image = fixit(bytes, pf);
                    pictureBox1.Image = image;

                    //    bitmap = CopyDataToBitmap(bytes, (PixelFormat)pf);
                    //    pictureBox1.Image = bitmap;
                    //    if (bitmap == null)
                    //    {
                    //        label1.Text += " error";
                    //    }
                    //    else {

                    //    }
                }
                catch (Exception ex)
                {
                    label1.Text += " error";
                }
                Application.DoEvents();
                System.Threading.Thread.Sleep(1000);
                if (bitmap != null) {
                    bitmap.Dispose();
                }

                label1.Text = "Done...everything sucks";
            }
        }
    }
}
