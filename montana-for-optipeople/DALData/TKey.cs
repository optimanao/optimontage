using System;
using API;
namespace API.Data {
public static class TKeys {
public const string save = "save";
public const string cancel = "cancel";
public const string saveAndClose = "saveAndClose";
public const string user = "user";
public const string workstation = "workstation";
public const string createNew = "createNew";
public const string nameFirst = "nameFirst";
public const string nameLast = "nameLast";
public const string jobTitle = "jobTitle";
public const string back = "back";
public const string delete = "delete";
public const string confirm = "confirm";
public const string confirmDelete = "confirmDelete";
public const string name = "name";
public const string placeholderSearch = "placeholderSearch";
public const string description = "description";
public const string nodata = "nodata";
public const string checkListItem = "checkListItem";
public const string barcode = "barcode";
public const string start = "start";
public const string statusCode = "statusCode";
public const string choose = "choose";
public const string type = "type";
public const string checkList = "checkList";
public const string modifiedOn = "modifiedOn";
public const string modifiedBy = "modifiedBy";
public const string all = "all";
public const string end = "end";
public const string duration = "duration";
public const string stats = "stats";
public const string closingTime = "closingTime";
public const string parts = "parts";
public const string print = "print";
public const string materials = "materials";
public const string partOrder = "partOrder";
public const string partsNothingOrdered = "partsNothingOrdered";
public const string quantity = "quantity";
public const string partIsPicked = "partIsPicked";
public const string partIsDelivered = "partIsDelivered";
public const string partsRunner = "partsRunner";
/// <summary>
/// tekst på knap øverst til højre for igangværende ordre
/// </summary>
public const string gotoActiveOrder = "gotoActiveOrder";
public const string login = "login";
public const string logout = "logout";
public const string comments = "comments";
public const string statusCodeTopMost = "statusCodeTopMost";
public static class Order {
public const string order = "Order.order";
public const string parked = "Order.parked";
public const string route = "Order.route";
public const string number = "Order.number";
public const string productionWeek = "Order.productionWeek";
public const string inProgress = "Order.inProgress";
public const string workedBy = "Order.workedBy";
public const string inProgressRightNow = "Order.inProgressRightNow";
public const string markCompleted = "Order.markCompleted";
public const string markCompletedInvalid = "Order.markCompletedInvalid";
public const string parkWithoutStatus = "Order.parkWithoutStatus";
public const string search = "Order.search";
public const string continue_ = "Order.continue";
public const string discard = "Order.discard";
public const string done = "Order.done";
}
public static class Product {
public const string product = "Product.product";
public const string normTime = "Product.normTime";
public const string model = "Product.model";
public const string unit = "Product.unit";
public const string front = "Product.front";
public const string back = "Product.back";
public const string depth = "Product.depth";
public const string mounts = "Product.mounts";
}
public static class Stats {
public const string isCompleted = "Stats.isCompleted";
public const string mostRecentTimelogStart = "Stats.mostRecentTimelogStart";
}
public static class StatusMessage {
public const string timerStopped = "StatusMessage.timerStopped";
public const string noOrderInProgress = "StatusMessage.noOrderInProgress";
public const string orderParked = "StatusMessage.orderParked";
public const string closingTimeLoggedOut = "StatusMessage.closingTimeLoggedOut";
public const string notLoggedIn = "StatusMessage.notLoggedIn";
}
public static class Timer {
public const string start = "Timer.start";
public const string stop = "Timer.stop";
public const string workstationLeave = "Timer.workstationLeave";
public const string workstationError = "Timer.workstationError";
public const string log = "Timer.log";
public const string userTimeLogGap = "Timer.userTimeLogGap";
}
public static class User {
public const string unknown = "User.unknown";
public const string change = "User.change";
public const string isAdmin = "User.isAdmin";
public const string updatePassword = "User.updatePassword";
public const string notLoggedIn = "User.notLoggedIn";
}
public const string OrderOrder = "Order.order";
public const string OrderParked = "Order.parked";
public const string OrderRoute = "Order.route";
public const string OrderNumber = "Order.number";
public const string OrderProductionWeek = "Order.productionWeek";
public const string OrderInProgress = "Order.inProgress";
public const string OrderWorkedBy = "Order.workedBy";
public const string OrderInProgressRightNow = "Order.inProgressRightNow";
public const string OrderMarkCompleted = "Order.markCompleted";
public const string OrderMarkCompletedInvalid = "Order.markCompletedInvalid";
public const string OrderParkWithoutStatus = "Order.parkWithoutStatus";
public const string OrderSearch = "Order.search";
public const string OrderContinue = "Order.continue";
public const string OrderDiscard = "Order.discard";
public const string OrderDone = "Order.done";
public const string ProductProduct = "Product.product";
public const string ProductNormTime = "Product.normTime";
public const string ProductModel = "Product.model";
public const string ProductUnit = "Product.unit";
public const string ProductFront = "Product.front";
public const string ProductBack = "Product.back";
public const string ProductDepth = "Product.depth";
public const string ProductMounts = "Product.mounts";
public const string StatsIsCompleted = "Stats.isCompleted";
public const string StatsMostRecentTimelogStart = "Stats.mostRecentTimelogStart";
public const string StatusMessageTimerStopped = "StatusMessage.timerStopped";
public const string StatusMessageNoOrderInProgress = "StatusMessage.noOrderInProgress";
public const string StatusMessageOrderParked = "StatusMessage.orderParked";
public const string StatusMessageClosingTimeLoggedOut = "StatusMessage.closingTimeLoggedOut";
public const string StatusMessageNotLoggedIn = "StatusMessage.notLoggedIn";
public const string TimerStart = "Timer.start";
public const string TimerStop = "Timer.stop";
public const string TimerWorkstationLeave = "Timer.workstationLeave";
public const string TimerWorkstationError = "Timer.workstationError";
public const string TimerLog = "Timer.log";
public const string TimerUserTimeLogGap = "Timer.userTimeLogGap";
public const string UserUnknown = "User.unknown";
public const string UserChange = "User.change";
public const string UserIsAdmin = "User.isAdmin";
public const string UserUpdatePassword = "User.updatePassword";
public const string UserNotLoggedIn = "User.notLoggedIn";
}
}
