﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using API;

namespace API.Data {
	/// <summary>
	/// Validates a string as a valid URL and checks that the domain exists (reponds to dns call)
	/// </summary>
	public class UrlDomainExistAttribute : ValidationAttribute {
		public UrlDomainExistAttribute() { }

		public override bool IsValid(object value) {
			var text = value as string;

			if (string.IsNullOrEmpty(text))
				return true;
			else {
				// http://stackoverflow.com/questions/3674281/validating-uri-from-string
				Uri uri;
				string uriString = "" + value.ToString();

				if (!uriString.Contains("://")) {
					uriString = "http://" + uriString;
				}
				if (Uri.TryCreate(uriString, UriKind.RelativeOrAbsolute, out uri)) {
					if (!string.IsNullOrWhiteSpace(text)) {
						try {
							if (System.Net.Dns.GetHostAddresses(uri.DnsSafeHost).Length > 0) {
								return true;
							} else {
								return false;
							}
						} catch (Exception) {
							return false;
						}
					}
				}
				return false;
				//return (!string.IsNullOrWhiteSpace(text) && Uri.TryCreate(text, UriKind.RelativeOrAbsolute, out uri)); // this REQUIRES a protocol in the text
			}
		}

	}
}