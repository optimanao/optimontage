﻿using dotUtils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace API {
	public static class APIDataStatics {

		//public static string DicValue(API.Data.DicKey TranslationKey) {
		//	return DicValue(TranslationKey.Name);
		//}

		public static string DicValue(string TranslationKey) {
			Dictionary dic = (Dictionary)CacheHandler.GetItem("Dictionary");
			string value = "not translated: " + TranslationKey;
			var entry = (dic != null ? dic.GetEntry(TranslationKey) : null);

			if (entry != null) {
				value = entry.GetValue(System.Threading.Thread.CurrentThread.CurrentCulture.Name);
				if (!entry.IsRichText) {
					value = value.Replace(Environment.NewLine, "<br />");
				}
			}
				// dic.GetValue(TranslationKey, System.Threading.Thread.CurrentThread.CurrentCulture.Name);

			if (String.IsNullOrEmpty(value))
				value = "not translated: " + TranslationKey;

			foreach (Match m in Regex.Matches(value, "{@\\w.+}")) {
				string repValue = dic.GetValue(m.Value.Replace("{@", "").Replace("}", ""), System.Threading.Thread.CurrentThread.CurrentCulture.Name);
				if (!repValue.StartsWith("not translated:")) {
					value = value.Replace(m.Value, repValue);
				}
			}
			
			return value;
		}

	}
}
