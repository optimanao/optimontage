﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using API.Data;

// version 2.0 - 2015-01-16. It appears Dictionary lookup is much faster than List lookup
// http://geekswithblogs.net/BlackRabbitCoder/archive/2011/06/16/c.net-fundamentals-choosing-the-right-collection-class.aspx

namespace API {
	public class Dictionary {

		private Dictionary<string, DictionaryEntry> _keys = new Dictionary<string, DictionaryEntry>();

		public void ParseEntries(List<DictionarySection> Sections, List<DictionaryEntry> Entries) {
			Dictionary<int, int> _sectionParents = new Dictionary<int, int>(); // sectionid, parentsectionid
			Sections.ForEach(n => _sectionParents.Add(n.ID, n.ParentID));

			foreach (var entry in Entries.Where(n => !n.DictionarySectionID.HasValue || n.DictionarySectionID == 0)) {
				this.Add(entry);
			}

			foreach (var entry in Entries.Where(n => n.DictionarySectionID.HasValue && n.DictionarySectionID > 0)) {
				string key = entry.Key;
				int sectionid = entry.DictionarySectionID.Value;
				if (!_sectionParents.ContainsKey(sectionid))
					continue;

				if (sectionid > 0) {
					key = Sections.FirstOrDefault(n => n.ID == sectionid).Name + "." + key;
					sectionid = (_sectionParents.ContainsKey(sectionid) ? _sectionParents[sectionid] : 0);
				}
				if (sectionid > 0) {
					while (_sectionParents.ContainsKey(sectionid)) {
						key = Sections.FirstOrDefault(n => n.ID == sectionid).Name + "." + key;
						sectionid = _sectionParents[sectionid];
					}
				}

				_keys.Add(key, entry);
			}

		}
		/// <summary>

		/// Return alle dictionary keys that has clientside entries
		/// </summary>
		public List<string> ClientsideKeys {
			get {
				return _keys.Where(n => n.Value.Clientside).Select(n => n.Key).ToList();
			}
		}

		public List<string> Keys {
			get {
				return _keys.Keys.ToList();
			}
		}

		public void Add(DictionaryEntry Entry) {
			if (_keys.ContainsKey(Entry.Key))
				_keys[Entry.Key] = Entry;
			else
				_keys.Add(Entry.Key, Entry);
		}

		public bool EntryExists(string Key) {
			return _keys.ContainsKey(Key);
		}

		public DictionaryEntry GetEntry(string Key) {
			return EntryExists(Key) ? _keys[Key.ToString()] : null;
		}

		public string GetValue(string Key, string Language) {
			string strReturn = "";
			if (EntryExists(Key)) {
				strReturn = GetEntry(Key).GetValue(Language);
			}
			return strReturn;
		}

	}
}
