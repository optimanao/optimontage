﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using API;

namespace API.Data {
    public class TransDisplayNameAttribute : DisplayNameAttribute
    {

        private readonly string resourceName;
        //	private readonly DicKey resourceKey = DicKey.none;
        public TransDisplayNameAttribute(string DictionaryKey)
            : base()
        {
            this.resourceName = DictionaryKey;
        }

        public override string DisplayName
        {
            get
            {
                return APIDataStatics.DicValue(this.resourceName);
            }
        }
    }
}