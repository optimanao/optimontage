﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data.ViewModels
{
	public class TestPocoModel : IValidatableObject
	{
		[Required]
		[StringLength(20)]
		public string Name { get; set; }

		public int? FavoriteIceCreamID { get; set; }

		public int? FavoriteIceCreamTypeID { get; set; }

		public int Age { get; set; } = 10;

		[Url]
		public string Email { get; set; }

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (Age == 10) { 
				yield return new ValidationResult("Skrub af, skrid, du er for lille!", new List<string>() { "Age" });
			}
		}
	}
}
