﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API.Data {

	public static class enums {
		/// <summary>
		/// For permissions
		/// </summary>
		public enum UserType {
			SBA = 1,					// can do everything
			Administrator = 2,	// can do something
			Company = 3,			// this is shit...am I mixing shit together??!
			User = 4					// is just plain old basic user - no special rights
		}

		public enum CacheEntryKeys {
			HolidaysDk
		}

		public enum CaseStatus {
			InProgress,
			Cancelled,
			Deferred,
			Resolved
		}

		public enum CacheKeys {
			LanguagesAll,
            LanguagesActive,
            UserList,
			ExchangeRates
		}

		public enum TempDataKeys {
			ShowOk,           // for showing the "clientside.showPageOk"-thingy
			ShowError,           // for showing the "clientside.showPageOk"-thingy
			MessageInfo,          // for showing message on next request/page
            MessageSuccess,          // for showing "green" message on next request/page
            MessageWarning,
            MessageError
        }

		public enum SettingsKeys {
			//EncryptionKey,
			AuthCookieDomain,
			CultureCookieName,
			DisableAuthentication,
			DomainBase,
			CompanyName,
            WorkstationCookieName,
            AdminPassword,
            AndonFolder
        }

		public enum SessionKeys {
			CurrentUser,
			CurrentUserReloadTimeStamp,
			CurrentLanguage,
			ClientsideCookiePersistTime,
            LastPageVisited
		}
	}
}
