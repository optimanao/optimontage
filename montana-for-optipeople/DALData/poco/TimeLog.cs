﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class TimeLog {
		
		public TimeLog() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

		[Required]
		[TransRequired]
		public long UserID { get; set; }
		
		public User User {
			 get; set;
		}

		[Required]
		[TransRequired]
		[TransDisplayName("startTimeUtc")]
		public DateTime StartTimeUtc {
			 get; set;
		}

		[TransDisplayName("duration")]
		public int? Duration {
			 get; set;
		}

		[Required]
		[TransRequired]
		[TransDisplayName("inProgress")]
		public bool InProgress {
			 get; set;
		}

        public string Barcode { get; set; }

        public long? StatusCodeID { get; set; }

        public StatusCode StatusCode { get; set; }

        public long WorkstationID { get; set; }

        public Workstation Workstation { get; set; }

        public bool OrderCompleted { get; set; }

        public bool IsRepair { get; set; }

    }
}
