﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
    public class NavComment
    {
        public string Comment { get; set; }
        public string SpecAtt { get; set; }
        public string ShippingComment { get; set; }
    }
}
