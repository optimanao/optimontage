﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class LinkProductCheckListItem {
		
		public LinkProductCheckListItem() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}
		[Required]
		[TransRequired]
		public long ProductID { get; set; }
		
		public Product Product {
			 get; set;
		}
		[Required]
		[TransRequired]
		public long CheckListItemID { get; set; }
		
		public CheckListItem CheckListItem {
			 get; set;
		}
		[TransDisplayName("sortOrder")]
		public int? SortOrder {
			 get; set;
		}
	}
}
