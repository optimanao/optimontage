﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class Order {
		
		public Order() { }
		
		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("barcode")]
		public string Barcode {
			 get; set;
		}
		[Required]
		[TransRequired]
		[TransDisplayName("isCompleted")]
		public bool IsCompleted {
			 get; set;
		}
		[Required]
		[TransRequired]
		[TransDisplayName("isDiscarded")]
		public bool IsDiscarded {
			 get; set;
		}
		[Required]
		[TransRequired]
		public long CreatedByUserID { get; set; }
		
		[Required]
		[TransRequired]
		[TransDisplayName("createdOnUtc")]
		public DateTime CreatedOnUtc {
			 get; set;
		}

        [Required]
        [TransRequired]
        public long WorkstationID { get; set; }

        public Workstation Workstation { get; set; }

        public decimal NormTime { get; set; }

    }
}
