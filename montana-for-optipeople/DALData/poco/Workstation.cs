﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class Workstation {
		
		public Workstation() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}
		[Required]
		[TransRequired]
		[StringLength(100)]
		[TransDisplayName("name")]
		public string Name {
			 get; set;
		}
		[TransDisplayName("description")]
		public string Description {
			 get; set;
		}
        [TransDisplayName("deletedOnUtc")]
        public DateTime? DeletedOnUtc
        {
            get; set;
        }
    }
}
