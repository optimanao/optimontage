﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class StatusCode {
		
		public StatusCode() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}
		[Required]
		[TransRequired]
		public long StatusCodeTypeID { get; set; }
		
		public StatusCodeType StatusCodeType {
			 get; set;
		}
		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("name")]
		public string Name {
			 get; set;
		}

		[TransDisplayName("deletedOnUtc")]
		public DateTime? DeletedOnUtc {
			 get; set;
		}

        public bool TopMost { get; set; }

	}
}
