﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class Cache {
		
		public Cache() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

        [Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("name")]
		public string CacheKey {
			 get; set;
		}

        public string CacheValue
        {
            get; set;
        }

        public DateTime CreatedOnUtc { get; set; }

    }
}
