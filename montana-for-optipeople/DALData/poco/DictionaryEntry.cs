﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace API.Data {
	public class DictionaryEntry {

		public DictionaryEntry() {
			//			this.Key = " ";
			this.Values = " ";
		}

		public int ID { get; set; }
		public string Key { get; set; }
		public string Values { get; set; }
		public bool IsRichText { get; set; }
		public bool Clientside { get; set; }
		public int? DictionarySectionID { get; set; }
		public string DictionarySectionName { get; set; }
		public string RequiredMergeFields { get; set; }

		public List<string> MergeFields { 
			get { 
				return (string.IsNullOrEmpty(RequiredMergeFields) ? 
					new List<string>() : 
					RequiredMergeFields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList()); 
			} 
		}

		[StringLength(200)]
		public string Notes { get;set; }

		public Dictionary<string, string> dic = new Dictionary<string, string>();

		public void SetValue(string Language, string Value) {
			if (!dic.ContainsKey(Language))
				dic.Add(Language, Value);
			else
				dic[Language] = Value;
		}

		public string GetValue(string Language) {
			string strReturnValue = "";
			if (dic.ContainsKey(Language))
				strReturnValue = dic[Language].ToString();
			return strReturnValue;
		}

	}
}
