﻿using System;
using System.ComponentModel.DataAnnotations;

namespace API.Data
{
    public partial class OrderChangeLog
    {
        public OrderChangeLog() { }

        public long ID { get; set; }

        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("barcode")]
        public string Barcode
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [TransDisplayName("isParked")]
        public bool IsParked
        {
            get; set;
        }

        public long? ParkedStatusCodeID { get; set; }

        public StatusCode ParkedStatusCode { get; set; }

        [Required]
        [TransRequired]
        [TransDisplayName("isCompleted")]
        public bool IsCompleted
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [TransDisplayName("isDiscarded")]
        public bool IsDiscarded
        {
            get; set;
        }

        [Required]
        [TransRequired]
        public long CreatedByUserID { get; set; }

        public User CreatedByUser { get; set; }

        [Required]
        [TransRequired]
        [TransDisplayName("createdOnUtc")]
        public DateTime CreatedOnUtc
        {
            get; set;
        }

        public long WorkstationID { get; set; }

        public Workstation Workstation { get; set; }

    }
}
