﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class Part {
		
		public Part() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

		[Required]
		[TransRequired]
		[StringLength(100)]
		[TransDisplayName("name")]
		public string Name {
			 get; set;
		}

		[StringLength(50)]
		[TransDisplayName("barcode")]
		public string Barcode {
			 get; set;
		}

		[TransDisplayName("notes")]
		public string Notes {
			 get; set;
		}

        public PartCategory PartCategory { get; set; }

        public int? PartCategoryID { get; set; }
	}
}
