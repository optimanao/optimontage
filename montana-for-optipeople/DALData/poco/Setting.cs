﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class Setting {
		
		public Setting() { }
		
		[Required]
		[TransRequired]
		public int ID {
			 get; set;
		}
		
		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("name")]
		public string Name {
			 get; set;
		}

        [Required]
        [TransRequired]
        public string SettingValue
        {
            get; set;
        }

        [Required]
        [TransRequired]
        public string Description
        {
            get; set;
        }

    }
}
