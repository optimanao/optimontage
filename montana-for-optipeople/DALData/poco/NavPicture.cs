﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
    public partial class NavPicture
    {
        public string Barcode { get; set; }
        public byte[] Picture_1 { get; set; }
        public byte[] Picture_2 { get; set; }
    }
}
