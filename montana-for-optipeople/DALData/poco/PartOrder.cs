﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class PartOrder {
		
		public PartOrder() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

		[Required]
		[TransRequired]
		[TransDisplayName("createdOn")]
		public DateTime CreatedOn {
			 get; set;
		}

        [Required]
		[TransRequired]
		[TransDisplayName("createdOnUtc")]
		public DateTime CreatedOnUtc {
			 get; set;
		}

        [Required]
		[TransRequired]
		public long PartID { get; set; }

        public Part Part { get; set; }
		
		[Required]
		[TransRequired]
		[TransDisplayName("quantity")]
		public int Quantity {
			 get; set;
		}

        [Required]
		[TransRequired]
		[TransDisplayName("isDelivered")]
        public bool IsDelivered {
			 get; set;
		}


        [Required]
        [TransRequired]
        [TransDisplayName("isPicked")]
        public bool IsPicked
        {
            get; set;
        }

        [Required]
		[TransRequired]
		public long CreatedByUserID { get; set; }

        public User CreatedByUser { get; set; }

        [Required]
		[TransRequired]
		public long WorkstationID { get; set; }
	
        public Workstation Workstation { get; set; }

        public DateTime? DeliveredOnUtc { get; set; }


    }
}
