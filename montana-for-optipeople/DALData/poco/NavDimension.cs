﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
    public class NavDimension
    {
        public string Barcode { get; set; }
        public int LineNo { get; set; }
        public string Sides { get; set; }
        public string Back { get; set; }
        public string Front { get; set; }
    }
}
