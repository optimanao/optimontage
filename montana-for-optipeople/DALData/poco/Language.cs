﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class Language {

		public Language() { }
		//private int _id = 0;
		//private string _locale = "";
		//private string _name = "";
		//private string _local_name = "";
		[Required]
		public int ID {
			get;
			set;
			/*get { return _id; } set { _id = value; }*/
		}
		
		[Required]
		[StringLength(10)]
		public string Locale {
			get;
			set;
			/*get { return _locale; } set { _locale = value; }*/
		}
		
		public bool Active { get; set; }

		[Required]
		[StringLength(50)]
		public string Name {
			get;
			set;
			/*get { return _name; } set { _name = value; }*/
		}
		
		[Required]
		[StringLength(50)]
		public string LocalName {
			get;
			set;
			/*get { return _local_name; } set { _local_name = value; }*/
		}
	}
}
