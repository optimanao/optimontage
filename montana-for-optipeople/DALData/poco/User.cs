﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class User {
		
		public User() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}
		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("nameFirst")]
		public string NameFirst {
			 get; set;
		}
		[StringLength(50)]
		[TransDisplayName("nameLast")]
		public string NameLast {
			 get; set;
		}
		[TransDisplayName("deletedOnUtc")]
		public DateTime? DeletedOnUtc {
			 get; set;
		}

        public string NameFull { get; set; }

        [StringLength(50)]
        [TransDisplayName("jobTitle")]
        public string JobTitle { get; set; }

        [StringLength(50)]
        [TransDisplayName("password")]
        public string Password { get; set; }

        [TransDisplayName("User.isAdmin")]
        public bool IsAdmin { get; set; }

    }
}
