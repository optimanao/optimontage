﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class OrderParking {
		
		public OrderParking() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}
		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("barcode")]
		public string Barcode {
			 get; set;
		}
		[Required]
		[TransRequired]
		[TransDisplayName("modifiedOnUtc")]
		public DateTime ModifiedOnUtc {
			 get; set;
		}
		[Required]
		[TransRequired]
		public long ModifiedByUserID { get; set; }

        public User ModifiedByUser { get; set; }
	
        public long? StatusCodeID { get; set; }

        public StatusCode StatusCode { get; set; }

    }
}
