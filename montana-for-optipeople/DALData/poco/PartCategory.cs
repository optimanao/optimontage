﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class PartCategory
    {
		
		public PartCategory() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

		[Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("name")]
		public string Name {
			 get; set;
		}
        
	}
}
