﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
    public partial class NavInfo
    {
        public string Barcode { get; set; }
        public string OrderNo { get; set; }
        public int Position { get; set; }
        public decimal Quantity { get; set; }
        public string Master { get; set; }
        public string Depth { get; set; }
        public string Color_sides { get; set; }
        public string Color_back { get; set; }
        public string Color_front { get; set; }
        public string Mounts { get; set; }
        //public byte[] Picture_1 { get; set; }
        //public byte[] Picture_2 { get; set; }
        public string RoutePlan { get; set; }
        public string Department { get; set; }
        public string ProductionWeekString { get; set; }
        public decimal NormTime { get; set; }
    }
}
