﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public class LocalizedValue {
		
		public LocalizedValue() { }
		
		[Required]
		[TransRequired]
		public long ID {
			 get; set;
		}

		[Required]
		[TransRequired]
		public int LanguageID { get; set; }
		
		public Language Language {
			 get; set;
		}
		[Required]
		[TransRequired]
		[TransDisplayName("text")]

		public string Text {
			 get; set;
		}

        [Required]
		[TransRequired]
		[StringLength(50)]
		[TransDisplayName("sourceEntity")]
		public string SourceEntityName {
			 get; set;
		}

        public long SourceEntityID { get; set; }

    }
}
