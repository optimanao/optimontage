﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API.Data {
	public class DictionarySection {
		
		public int ID { get; set; }
		public string Name { get; set; }
		public int ParentID { get; set; }
		/// <summary>
		/// number of dictionary key in section
		/// </summary>
		public int EntryCount { get; set; }
	}
}
