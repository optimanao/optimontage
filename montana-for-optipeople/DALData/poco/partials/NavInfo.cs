﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data
{
    public partial class NavInfo
    {
        public List<NavDimension> Dimensions { get; set; } = new List<NavDimension>();

        public NavComment Comments { get; set; } = new NavComment();

        public int ProductionYear
        {
            get
            {
                if (string.IsNullOrEmpty(ProductionWeekString))
                {
                    return 2000;
                }

                return
                    int.Parse(
                            DateTime.Now.Year.ToString().Substring(0, 2)
                            + ProductionWeekString.Replace("UGE", "").Substring(0, 2)
                            );
            }
        }

        public int ProductionWeek
        {
            get
            {
                if (string.IsNullOrEmpty(ProductionWeekString))
                {
                    return 0;
                }

                return
                    int.Parse(
                            ProductionWeekString.Substring(5)
                            );
            }
        }

        // view prop for list
        public bool IsParked { get; set; }

    }
}
