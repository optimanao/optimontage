﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class CheckListItem {

        public List<LocalizedValue> LocalizedValues { get; set; } = new List<LocalizedValue>();

	}
}
