﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data {
	public partial class User {

        public string FullName
        {
            get
            {
                return NameFirst + (!String.IsNullOrEmpty(NameLast) ? " " : "") + NameLast;
            }
        }

        public List<string> Roles { get; set; } = new List<string>();
    }
}
