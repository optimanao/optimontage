﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data {
	public sealed class TransRequiredAttribute : RequiredAttribute {

		public override string FormatErrorMessage(string name) {
			return APIDataStatics.DicValue("validationErrorMessage").Replace("{0}", name);
		}
	}
}
