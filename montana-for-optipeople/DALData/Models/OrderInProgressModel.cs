﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class OrderInProgressModel
    {
        public string Barcode { get; set; }
        public bool InProgress { get; set; }
        public DateTime ModifiedOn { get; set; }
        public long ModifiedByUserID { get; set; }
        public User ModifiedByUser { get; set; }
        public long WorkstationID { get; set; }
        public Workstation Workstation { get; set; }
    }
}
