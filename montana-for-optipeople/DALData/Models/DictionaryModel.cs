﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace waAssembly.Models {
	public class DictionaryModel {

		private List<API.Data.Language> _AvailableLanguages = new List<API.Data.Language>();
		public List<API.Data.Language> AvailableLanguages {
			get { return _AvailableLanguages; }
			set { _AvailableLanguages = value; }
		}

		private List<API.Data.DictionarySection> _sections = new List<API.Data.DictionarySection>();
		public List<API.Data.DictionarySection> Sections {
			get { return _sections; }
			set { _sections = value; }
		}

		//private List<API.Data.DictionaryEntry> _entries = new List<API.Data.DictionaryEntry>();
		//public List<API.Data.DictionaryEntry> Entries {
		//	get { return _entries; }
		//	set { _entries = value; }
		//}

		public string SearchPhrase { get; set; }
		
		/// <summary>
		/// The selected category section
		/// </summary>
		public int DictionarySectionID { get; set; }

		public bool SearchAllSections { get; set; }

		public int[] VisibleLanguages { get; set; }
	}
}