﻿using System;

namespace API.Data.Models
{
    public class DashboardOrderChartModel
    {
        public int OrdersCompleted { get; set; }

        public int OrdersRepaired { get; set; }

        public int OrdersDiscarded { get; set; }

        public DateTime Date { get; set; }
    }
}
