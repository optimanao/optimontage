﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class ProductEditModel
    {
        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("name")]
        public string Name
        {
            get; set;
        }

        public List<CheckListItem> CheckListItems { get; set; } = new List<CheckListItem>();

        public List<long> CheckListItemsPostback { get; set; } = new List<long>();

    }
}
