﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace waAssembly.Models {
	public class DictionaryEntryModel : IValidatableObject {

		public API.Data.DictionaryEntry Entry { get; set; }

		private List<API.Data.DictionarySection> _sections = new List<API.Data.DictionarySection>();
		public List<API.Data.DictionarySection> Sections {
			get { return _sections; }
			set { _sections = value; }
		}

		private List<API.Data.Language> _AvailableLanguages = new List<API.Data.Language>();
		public List<API.Data.Language> AvailableLanguages {
			get { return _AvailableLanguages; }
			set { _AvailableLanguages = value; }
		}

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext) {

			if (!Regex.IsMatch(Entry.Key, "^[a-zA-Z0-9]+$")) {
				yield return new ValidationResult("Key must contain only letters and numbers!");
			}

			if (!String.IsNullOrEmpty(Entry.RequiredMergeFields)) {
				var fields = Entry.RequiredMergeFields.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);

				foreach (var dicKey in Entry.dic.Keys) {
					var dicValue = Entry.dic[dicKey];
					if (String.IsNullOrEmpty(dicValue))
						continue; // must be allowed to save empty values

					foreach (var field in fields) {
						if (!dicValue.Contains("{" + field.Trim() + "}")) {
							yield return new ValidationResult(dicKey + " value must contain '" + field + "' merge field");
						}
					}
				}
			} // if
		} // validate

		///// <summary>
		///// The selected category section
		///// </summary>
		//public int DictionarySectionID { get; set; }
		
	}
}