﻿using System;

namespace API.Data.Models
{
    public class DashboardProductivityChartModel
    {
        /// <summary>
        /// hours
        /// </summary>
        public int TimeLoggedIn { get; set; }

        public decimal Norms { get; set; }

        public DateTime Date { get; set; }
    }
}
