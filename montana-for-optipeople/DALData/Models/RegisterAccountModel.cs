﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace API.Data.Models {
	public class RegisterAccountModel {

		[TransRequired]
		[TransDisplayName("nameFirst")]
		public string NameFirst { get; set; }

		[TransRequired]
		[TransDisplayName("password")]
		public string Password { get; set; }

		[TransDisplayName("passwordRepeat")]
		public string PasswordRepeat { get; set; }

		[TransRequired]
		[TransDisplayName("email")]
		public string Email { get; set; }


		public int? TimeZoneOffset { get; set; }

	}
}
