﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class WorkstationEditModel
    {
        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }
        
        [StringLength(100)]
        [TransDisplayName("name")]
        public string Name
        {
            get; set;
        }

        [StringLength(100)]
        [TransDisplayName("description")]
        public string Description
        {
            get; set;
        }
        
    }
}
