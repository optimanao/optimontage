﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class TimeLogSearchModel
    {
        public List<User> AvailableUsers { get; set; } = new List<User>();

        public List<Workstation> AvailableWorkstations { get; set; } = new List<Workstation>();

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public bool ExportToExcel { get; set; }

        public bool IsPostback { get; set; }

        public List<TimeLog> Timelogs { get; set; } = new List<TimeLog>();

        public long? UserID { get; set; }

        public long? WorkstationID { get; set; }
    }
}
