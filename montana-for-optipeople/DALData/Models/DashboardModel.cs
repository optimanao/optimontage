﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class DashboardModel
    {
        public int ChartSecsTotal { get; set; }

        public int ChartSecsMounting { get; set; }

        public int ChartSecsPause { get; set; }

        /// <summary>
        /// Time unaccounted for
        /// </summary>
        public int ChartSecsWaste { get; set; }

        public Dictionary<string, decimal> NormCountsDic { get; set; } = new Dictionary<string, decimal>();

        public Dictionary<string, DashboardOrderChartModel> OrderChartDic { get; set; } = new Dictionary<string, DashboardOrderChartModel>();

        /// <summary>
        /// For Productivity chart
        /// </summary>
        //public Dictionary<string, DashboardProductivityChartModel> ProductivityChartDic { get; set; } = new Dictionary<string, DashboardProductivityChartModel>();

        public Dictionary<string, decimal> ProductivityLoggedInDic { get; set; } = new Dictionary<string, decimal>();

        public int WSChartSecsTotal { get; set; }

        public int WSChartSecsMounting { get; set; }

        public int WSChartSecsPause { get; set; }

        /// <summary>
        /// Time unaccounted for
        /// </summary>
        public int WSChartSecsWaste { get; set; }

        /// <summary>
        /// For line chart - number of norms per day
        /// </summary>
        public Dictionary<string, decimal> WSNormCountsDic { get; set; } = new Dictionary<string, decimal>();

        /// <summary>
        /// For column chart - orders by type per day
        /// </summary>
        public Dictionary<string, DashboardOrderChartModel> WSOrderChartDic { get; set; } = new Dictionary<string, DashboardOrderChartModel>();


        public int WSNormTarget { get; set; } = 0;

        /// <summary>
        /// For Productivity chart
        /// </summary>
        //public Dictionary<string, DashboardProductivityChartModel> WSProductivityChartDic { get; set; } = new Dictionary<string, DashboardProductivityChartModel>();

        public Dictionary<string, decimal> WSProductivityLoggedInDic { get; set; } = new Dictionary<string, decimal>();

    }
}
