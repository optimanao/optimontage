﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class PartEditModel
    {
        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("name")]
        public string Name
        {
            get; set;
        }

        [StringLength(50)]
        [TransDisplayName("barcode")]
        public string Barcode
        {
            get; set;
        }

        [TransDisplayName("description")]
        public string Notes
        {
            get; set;
        }

        [TransDisplayName("partCategory")]
        public int? PartCategoryID { get; set; }

        public List<PartCategory> AvailableCategories { get; set; } = new List<PartCategory>();
    }
}
