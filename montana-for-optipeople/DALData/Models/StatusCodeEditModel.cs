﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class StatusCodeEditModel
    {

        public List<StatusCodeType> AvailableCodeTypes { get; set; } = new List<StatusCodeType>();
        public List<Language> AvailableLanguages { get; set; } = new List<Language>();
        public List<LocalizedValue> LocalizedValues { get; set; } = new List<LocalizedValue>();

        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [TransDisplayName(TKeys.type)]
        public long StatusCodeTypeID { get; set; }

        
        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("name")]
        public string Name
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [TransDisplayName("statusCodeTopMost")]
        public bool TopMost { get; set; }

    }
}
