﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class UserEditModel
    {
        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }
        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("nameFirst")]
        public string NameFirst
        {
            get; set;
        }
        [StringLength(50)]
        [TransDisplayName("nameLast")]
        public string NameLast
        {
            get; set;
        }

        [StringLength(50)]
        [TransDisplayName("jobTitle")]
        public string JobTitle { get; set; }


        [TransDisplayName("User.isAdmin")]
        public bool IsAdmin { get; set; }

    }
}
