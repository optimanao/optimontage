﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace API.Data.Models {

	/// <summary>
	/// Model for registering new user accounts
	/// </summary>
	public class RegisterUserModel : RegisterAccountModel {
		[TransDisplayName("nameMiddle")]
		public string NameMiddle { get; set; }

		[TransRequired]
		[TransDisplayName("nameLast")]
		public string NameLast { get; set; }

	}
}
