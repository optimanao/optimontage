﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class PartCategoryEditModel
    {
        [Required]
        [TransRequired]
        public long ID
        {
            get; set;
        }

        [Required]
        [TransRequired]
        [StringLength(50)]
        [TransDisplayName("name")]
        public string Name
        {
            get; set;
        }

    }
}
