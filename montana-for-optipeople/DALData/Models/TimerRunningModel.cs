﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class TimerRunningModel
    {
        public TimeLog TimeLog { get; set; }

        public NavInfo NavInfo { get; set; }

        public List<CheckListItem> CheckListItems { get; set; } = new List<CheckListItem>();

        public List<StatusCode> StatusCodesError { get; set; } = new List<StatusCode>();
    }
}
