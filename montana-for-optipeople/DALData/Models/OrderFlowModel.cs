﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Models
{
    public class OrderFlowModel
    {
        public string Barcode { get; set; }

        public bool MarkedCompleted { get; set; }

        public string RecentWorkstation {
            get {
                return TimeLogs?.OrderByDescending(m => m.InProgress).ThenByDescending(m => m.StartTimeUtc).FirstOrDefault()?.Workstation?.Name ?? "-";
            }
        }

        public bool CurrentlyInProgress { get {
                return TimeLogs?.Any(m => m.InProgress) ?? false;
            } }

        public List<TimeLog> TimeLogs { get; set; } = new List<TimeLog>();

        public List<OrderChangeLog> ChangeLogs { get; set; } = new List<OrderChangeLog>();

        public string NavInfoDepth { get; set; }

        public string NavInfoColorSides { get; set; }

    }
}
