﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using API;

namespace API.Data {
	public class UrlAttribute : ValidationAttribute {
		public UrlAttribute() { }

		public override bool IsValid(object value) {
			var text = value as string;

			if (string.IsNullOrEmpty(text))
				return true;
			else {
				// http://stackoverflow.com/questions/3674281/validating-uri-from-string
				Uri uri;
				string uriString = "" + value.ToString();

				if (!uriString.Contains("://")) {
					uriString = "http://" + uriString;
				}
				if (Uri.TryCreate(uriString, UriKind.RelativeOrAbsolute, out uri)) {
					if (!string.IsNullOrWhiteSpace(text)) {
						return true;
					}
				}
				return false;
				//return (!string.IsNullOrWhiteSpace(text) && Uri.TryCreate(text, UriKind.RelativeOrAbsolute, out uri)); // this REQUIRES a protocol in the text
			}
		}

	}
}