﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API;
using API.Data.ViewModels;
using waAssembly.Services.Models;

namespace waAssembly.Services
{
	public class ValidationTestControllerService
	{

		private interfaces.ISessionContext pSession;
		private ValidationResultData pValidationResult = new ValidationResultData();

		public ValidationTestControllerService(interfaces.ISessionContext _sessionContext)
		{
			pSession = _sessionContext;
		}

		public bool Test()
		{
			return pSession.GetSession(API.Data.enums.SessionKeys.CurrentUser) == null;
		}

		public ValidationResultData ValidateModelState(object model)
		{
			pValidationResult = new ValidationResultData();
			var results = new List<ValidationResult>();
			var context = new ValidationContext(model, null, null);
			if (!Validator.TryValidateObject(model, context, results, true))
			{
				// results will contain all the failed validation errors.
				results.ForEach(m => m.MemberNames.ToList().ForEach(n => pValidationResult.AddError(n, m.ErrorMessage)));
			}
			
			return pValidationResult;
		}

		public ValidationResultData ValidateSave(TestPocoModel model, bool IncludeModelState = true)
		{
			pValidationResult = new ValidationResultData();
			if (IncludeModelState) {
				pValidationResult = ValidateModelState(model);
			}
			if (model.FavoriteIceCreamID.HasValue && !model.FavoriteIceCreamTypeID.HasValue)
			{
				pValidationResult.AddError("FavoriteIceCreamTypeID", "Cannot have ice cream id without ice cream type id");
			}
			return pValidationResult;
		}

		//public ValidationTestControllerService()


		// jeg skal bruge:
		// - CurrentUser/SessionUser for validering af rettigheder til data
		// - settings context
		// - ModelStateValidering
		// - logikvalidering
		// - fejl-logging
		// - DAL

	}

}
