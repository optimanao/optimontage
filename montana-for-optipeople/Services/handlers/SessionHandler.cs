﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using API;
using API.Data;
using API.web;
using waAssembly.Services.interfaces;

namespace waAssembly
{
	public class SessionHandler : ISessionContext
	{
		public API.web.UserIdentityData CurrentUser
		{
			get
			{
				try
				{
					return ((API.web.UserIdentity)System.Web.HttpContext.Current.User.Identity).UserData;
				} catch (Exception)
				{
					return null;
				}
			}
			set { }
		}

        public long? WorkstationID
        {
            get
            {
                try
                {
                    var cookie = HttpContext.Current.Request.Cookies[statics.App.GetSetting(enums.SettingsKeys.WorkstationCookieName)];
                    if (cookie == null) {
                        throw new Exception("Cannot determine current workstation");
                    }
                    return long.Parse("" + cookie.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
            set { }
        }


        public object GetSession(enums.SessionKeys SessionKey)
		{
			if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[SessionKey.ToString()] != null)
			{
				return System.Web.HttpContext.Current.Session[SessionKey.ToString()];
			}
			else
			{
				return null;
			}
		}
		public T GetSession<T>(enums.SessionKeys SessionKey)
		{
			if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null && System.Web.HttpContext.Current.Session[SessionKey.ToString()] != null)
			{
				var value = System.Web.HttpContext.Current.Session[SessionKey.ToString()];
				try
				{
					return (T)value;
				} catch (Exception)
				{
					return default(T);
				}
			}
			else
			{
				return default(T);
			}
		}
		public string GetSessionString(enums.SessionKeys SessionKey)
		{
			var o = GetSession(SessionKey);
			if (o != null)
				return o.ToString();
			else
				return string.Empty;
		}
		public void SetSession(enums.SessionKeys SessionKey, object o)
		{
			if (System.Web.HttpContext.Current != null && System.Web.HttpContext.Current.Session != null)
			{
				var session = System.Web.HttpContext.Current.Session;
				if (session[SessionKey.ToString()] == null)
					session.Add(SessionKey.ToString(), o);
				else
					session[SessionKey.ToString()] = o;
			}
		}

	}
}