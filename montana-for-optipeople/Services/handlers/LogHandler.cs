﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using waAssembly.Services.interfaces;

namespace waAssembly
{
    public class LogHandler : ILogContext
    {
        public void Debug(params object[] args)
        {
            API.statics.log.Debug(String.Join(", ", args.Select(m => m.ToString())));
        }

        public void Error(params object[] args)
        {
            API.statics.log.Error(String.Join(", ", args.Select(m => m.ToString())));
        }

        public void Fatal(params object[] args)
        {
            API.statics.log.Fatal(String.Join(", ", args.Select(m => m.ToString())));
        }

        public void Info(params object[] args)
        {
            //if (!string.IsNullOrEmpty(property)) {
            //	API.statics.log.Info(property);
            //}
            //https://stackoverflow.com/questions/171970/how-can-i-find-the-method-that-called-the-current-method
            API.statics.log.Info(Environment.NewLine + String.Join(", ", args.Select(m => m.ToString())));
        }

        public void Trace(params object[] args)
        {
            API.statics.log.Trace(String.Join(", ", args.Select(m => m.ToString())));
        }

        public void Warn(params object[] args)
        {
            API.statics.log.Warn(String.Join(", ", args.Select(m => m.ToString())));
        }

    }
}
