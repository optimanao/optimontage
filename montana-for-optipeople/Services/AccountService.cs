﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API;
using API.Data.ViewModels;
using waAssembly.Services.Models;
using System.Web.Security;
using API.Data;
using System.Web;

namespace waAssembly.Services
{
	public class AccountService
	{

		private DAL ipa = null;
		private interfaces.ISessionContext pSession;
		private ValidationResultData pvd = new ValidationResultData();
		private interfaces.ILogContext log;

        public AccountService(interfaces.ISessionContext _sessionContext, interfaces.ILogContext _logContext)
        {
            pSession = _sessionContext;
            log = _logContext;
            ipa = new DAL();
        }

        public User FormsAuthTicketSetup(long id, bool persistanceFlag, HttpResponseBase Response, long? ImpersonatingUserID = null)
        {
            User user = null;
            using (var ipa = new DAL())
            {
                user = ipa.UserGetSingle(id);
            }

            var uid = new API.web.UserIdentityData()
            {
                ID = user.ID,
                //Email = user.Email,
                //UserTypeId = user.UserTypeId,
                Name = user.NameFull,
                Roles = user.Roles,
                IsAdmin = user.IsAdmin
                //, CompanyID = user.CompanyID
            };
            if (ImpersonatingUserID.HasValue)
            {
                uid.ImpersonatingUserID = ImpersonatingUserID.Value;
            }

            var userData = statics.ToJson(uid);
            var authTicket = new FormsAuthenticationTicket(1, //version
                                                        "user" + user.ID,                           // user name
                                                        DateTime.Now,                     //creation
                                                        DateTime.Now.AddDays(15),  //Expiration
                                                        persistanceFlag,                  //Persistent
                                                        userData);

            var encTicket = FormsAuthentication.Encrypt(authTicket);
            var authCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encTicket);
            string cookieDomain = statics.App.GetSetting(enums.SettingsKeys.AuthCookieDomain);
            if (!cookieDomain.Contains("localhost"))
            {
                authCookie.Domain = cookieDomain;
            }
            Response.Cookies.Add(authCookie);

            return user;
        }

        public void FormsAuthTicketDestroy(HttpRequestBase Request, HttpResponseBase Response)
        {
            // to make sure we're logged out
            if (Request.Cookies[FormsAuthentication.FormsCookieName] != null)
            {
                HttpCookie myCookie = new HttpCookie(FormsAuthentication.FormsCookieName);
                myCookie.Expires = DateTime.UtcNow.AddDays(-1);
                Response.Cookies.Add(myCookie);
                Request.Cookies.Remove(FormsAuthentication.FormsCookieName);
            }
            FormsAuthentication.SignOut();
        }

        public void StopRunningTimer(long UserID)
        {
            // this is 'plural' just in case
            using (var ipa = new DAL())
            {
                var runnings = ipa.TimeLogGetCollectionRunning(UserID);
                foreach (var running in runnings)
                {
                    running.InProgress = false;
                    running.Duration = statics.Duration(running.StartTimeUtc);
                    ipa.TimeLogSave(running);
                }
            }
        }

        // jeg skal bruge:
        // - CurrentUser/SessionUser for validering af rettigheder til data
        // - settings context
        // - ModelStateValidering
        // - logikvalidering
        // - fejl-logging
        // - DAL


    }
}
