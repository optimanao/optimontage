﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Data;

namespace waAssembly.Services.interfaces
{
	public interface ISessionContext
	{
		object GetSession(enums.SessionKeys SessionKey);

		T GetSession<T>(enums.SessionKeys SessionKey);

		string GetSessionString(enums.SessionKeys SessionKey);

		void SetSession(enums.SessionKeys SessionKey, object o);
		
		API.web.UserIdentityData CurrentUser { get; set; }

        long? WorkstationID { get; set; }

	}
}
