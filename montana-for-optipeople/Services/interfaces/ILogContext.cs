﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace waAssembly.Services.interfaces
{
	public interface ILogContext
	{
		void Trace(params object[] args);


		//void Debug(object arg);
		void Debug(params object[] args);

		//https://stackoverflow.com/questions/171970/how-can-i-find-the-method-that-called-the-current-method
		void Info(params object[] args);


		void Error(params object[] args);


		void Warn(params object[] args);


		void Fatal(params object[] args);


	}
}
