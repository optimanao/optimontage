﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace waAssembly.Services.Models
{
	public class JsonResultData
	{

		public JsonResultData() { }
		public JsonResultData(ResultStatus Status)
		{
			this.Status = Status;
		}

		public enum ResultStatus
		{
			Ok = 0,
			Warning = 1,
			Error = 2
		}
		public bool StatusOk { get { return this.Status == ResultStatus.Ok; } }
		public ResultStatus Status { get; set; }
		public string StatusText { get { return this.Status.ToString(); } }
		public string Message { get; set; }
		public object Data { get; set; }
		//public IEnumerable<string> Errors { get; set; }
	}
}
