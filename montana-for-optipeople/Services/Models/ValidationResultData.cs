﻿using System.Collections.Generic;
using waAssembly.Services.interfaces;

namespace waAssembly.Services.Models
{
	public class ValidationResultData
	{
		public ValidationResultData() { }
		public ValidationResultData(ResultStatus Status)
		{
			this.Status = Status;
		}

		public enum ResultStatus
		{
			Ok = 0,
			Warning = 1,
			Error = 2
		}

		#region dto
		private IDictionary<string, object> _data { get; set; } = new Dictionary<string, object>();

		/// <summary>
		/// Add data to the return object. For passing data from service to controller.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void DataAdd(string key, object value)
		{
			if (!_data.ContainsKey(key))
			{
				_data.Add(key, null);
			}
			_data[key] = value;
		}

		public void DataRemove(string key)
		{
			if (_data.ContainsKey(key))
			{
				_data.Remove(key);
			}
		}

		public bool DataExists(string key)
		{
			return _data.ContainsKey(key) && _data[key] != null;
		}

		public object DataGet(string key)
		{
			if (DataExists(key))
			{
				return _data[key];
			}
			return null;
		}

		public T DataGet<T>(string key)
		{
			if (DataExists(key))
			{
				var value = DataGet(key);
				if (value is T)
				{
					return (T)_data[key];
				}
			}
			return default(T);
		}

		#endregion

		public IDictionary<string, string> ErrorDictionary { get; set; } = new Dictionary<string, string>();

		public IDictionary<string, string> WarningDictionary { get; set; } = new Dictionary<string, string>();

		/// <summary>
		/// Add model error and set status to "Error"
		/// </summary>
		/// <param name="Key"></param>
		/// <param name="Message"></param>
		public void AddError(string Key, string Message)
		{
			Status = ResultStatus.Error;
			if (!ErrorDictionary.ContainsKey(Key))
			{
				ErrorDictionary.Add(Key, Message);
			}
		}

		/// <summary>
		/// When validating workflows we need to warn the user of something stupid...which is not stupid enough to prevent workflow execution
		/// </summary>
		/// <param name="Key"></param>
		/// <param name="Message"></param>
		public void AddWarning(string Key, string Message)
		{
			if (Status == ResultStatus.Ok)
			{
				Status = ResultStatus.Warning;
			}

			if (!WarningDictionary.ContainsKey(Key))
			{
				WarningDictionary.Add(Key, Message);
			}
		}

		public bool IsValid
		{
			get
			{
				return ErrorDictionary.Count == 0;
			}
		}
		public bool HasWarnings
		{
			get
			{
				return WarningDictionary.Count > 0;
			}
		}

		public bool StatusOk { get { return this.Status == ResultStatus.Ok && IsValid; } }

		private ResultStatus _status = ResultStatus.Ok;
		public ResultStatus Status
		{
			get
			{
				return (IsValid ? _status : ResultStatus.Error);
			}
			set
			{
				_status = value;
			}
		}

		public object Data { get; set; }

	}
}
