﻿using System.Collections.Generic;
using System.Linq;
using waAssembly.Services.interfaces;

// http://stackoverflow.com/questions/8487800/service-layer-validation
// http://www.asp.net/mvc/overview/older-versions-1/models-%28data%29/validating-with-a-service-layer-cs

// this one is more apropriate
// http://codereview.stackexchange.com/questions/7845/model-validation-from-within-the-service-layer

// http://stackoverflow.com/questions/4776396/validation-how-to-inject-a-model-state-wrapper-with-ninject/4851953#4851953

namespace waAssembly.Services.Models
{
	public class __ModelErrorDictionary
	{
		Dictionary<string, string> _modelState = new Dictionary<string, string>();

		public __ModelErrorDictionary() {}

		public void AddError(string key, string errorMessage)
		{
			_modelState.Add(key, errorMessage);
		}

		public bool IsValid
		{
			get { return !_modelState.Any(); }
		}

	}
}
