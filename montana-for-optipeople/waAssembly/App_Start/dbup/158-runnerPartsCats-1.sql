﻿IF NOT (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = 'mtaPartCategory'))
	BEGIN
		CREATE TABLE dbo.mtaPartCategory
			(
			ID int NOT NULL IDENTITY (1, 1),
			Name nvarchar(50) NOT NULL
			)  ON [PRIMARY]
		
		ALTER TABLE dbo.mtaPartCategory ADD CONSTRAINT
			PK_mtaPartCategory PRIMARY KEY CLUSTERED 
			(
			ID
			) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

		ALTER TABLE dbo.mtaPartCategory SET (LOCK_ESCALATION = TABLE)
		COMMIT
	END