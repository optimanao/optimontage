﻿IF (COL_LENGTH('mtaPart', 'PartCategoryID') IS NULL)
BEGIN
		ALTER TABLE dbo.mtaPart ADD	PartCategoryID int NULL
		
		COMMIT

		ALTER TABLE dbo.mtaPart ADD CONSTRAINT	FK_mtaPart_mtaPartCategory FOREIGN KEY
	(
	PartCategoryID
	) REFERENCES dbo.mtaPartCategory
	(
	ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  SET NULL 
	
	COMMIT

	ALTER TABLE dbo.mtaPart SET (LOCK_ESCALATION = TABLE)
	COMMIT
END