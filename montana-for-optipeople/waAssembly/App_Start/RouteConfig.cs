﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace waAssembly {
	public class RouteConfig {
		public static void RegisterRoutes(RouteCollection routes) {
			routes.LowercaseUrls = true;
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				 "Display",
				 "d/{*DisplayKey}",
				 new { controller = "D", action = "Index" }
			);

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);

			
		}
	}
}