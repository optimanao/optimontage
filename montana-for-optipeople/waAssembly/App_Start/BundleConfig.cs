﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace waAssembly {
	public class AsIsBundleOrderer : IBundleOrderer {
		public virtual IEnumerable<System.IO.FileInfo> OrderFiles(BundleContext context, IEnumerable<System.IO.FileInfo> files) {
			return files;
		}
	}

	public class BundleConfig {
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles) {
			bundles.Add(new StyleBundle("~/cssbundles/waAssembly").Include(
				"~/css/waAssembly.less",
				"~/css/dictionary.css"
				));

			var jswaAssembly = new ScriptBundle("~/bundles/waAssembly");
			jswaAssembly.Orderer = new AsIsBundleOrderer();
			jswaAssembly.Include(
						"~/js/includer.js",
						"~/js/base/modernizr.custom.js",
						"~/js/mousetrap.1.4.6.js",
						"~/js/spin.js",
						"~/js/spectrum.js",
						"~/js/waAssembly.js",
						"~/js/default-events.js",
						"~/js/inline-confirm.js",
						"~/js/clientside.js");
			bundles.Add(jswaAssembly);


			//bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
			//			"~/Scripts/jquery-{version}.js"));

			//bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
			//			"~/Scripts/jquery-ui-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/js/jquery.unobtrusive-ajax.min.js",
						"~/js/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			//bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
			//			"~/Scripts/modernizr-*"));

			//bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

			//	bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
			//				"~/Content/themes/base/jquery.ui.core.css",
			//				"~/Content/themes/base/jquery.ui.resizable.css",
			//				"~/Content/themes/base/jquery.ui.selectable.css",
			//				"~/Content/themes/base/jquery.ui.accordion.css",
			//				"~/Content/themes/base/jquery.ui.autocomplete.css",
			//				"~/Content/themes/base/jquery.ui.button.css",
			//				"~/Content/themes/base/jquery.ui.dialog.css",
			//				"~/Content/themes/base/jquery.ui.slider.css",
			//				"~/Content/themes/base/jquery.ui.tabs.css",
			//				"~/Content/themes/base/jquery.ui.datepicker.css",
			//				"~/Content/themes/base/jquery.ui.progressbar.css",
			//				"~/Content/themes/base/jquery.ui.theme.css"));
		}
	}
}