﻿using System.Web;
using System.Web.Mvc;

namespace waAssembly {
	public class FilterConfig {
		public static void RegisterGlobalFilters(GlobalFilterCollection filters) {
			filters.Add(new HandleErrorAttribute());
			filters.Add(new GlobalIdentityInjector());
		}
	}
}