[assembly: WebActivator.PostApplicationStartMethod(typeof(waAssembly.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace waAssembly.App_Start
{
	using System.Reflection;
	using System.Web.Mvc;
	using Services.interfaces;
	using Services.Models;
	using SimpleInjector;
	using SimpleInjector.Extensions;
	using SimpleInjector.Integration.Web;
	using SimpleInjector.Integration.Web.Mvc;

    public static class SimpleInjectorInitializer
    {
        /// <summary>Initialize the container and register it as MVC3 Dependency Resolver.</summary>
        /// 

        public static Container container;

        public static void Initialize()
        {
            container = new Container();

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            InitializeContainer(container);

            container.RegisterMvcControllers(Assembly.GetExecutingAssembly());

            container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
        }

        private static void InitializeContainer(Container container)
        {
            container.Register<ISessionContext, SessionHandler>();
            container.Register<ILogContext, LogHandler>();
            //container.Register<IMailHandler, MailHandler>();
        }
    }
}