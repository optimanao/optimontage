﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using System.Web.Mvc;
using dotless.Core.Loggers;

namespace dotnector.Bundling
{
    public class CustomBundlesConfig
    {




        /// <summary>
        /// Compile javascript and stylesheet (dotLess supported) bundles to cache.
        /// </summary>
        /// <param name="CssRootPath">The absolute path for the "root" for stylesheets. Relevant for dotLess-files only</param>
        public void Configure(string CssRootPath)
        {
            this.CssRootPath = CssRootPath;
            bundles.Add(new bundle(bundle.BundleType.js, "waAssembly.js",
                               "~/assets/js/base/jquery.3.3.1.min.js",
                               //"~/assets/js/base/bootstrap.3.3.7min.js",
                               "~/assets/js/bootstrap4/popper.min.js",
                               "~/assets/js/bootstrap4/bootstrap.min.js",
                                "~/assets/js/clientside.js",
                                "~/assets/js/includer.js",
                                "~/assets/js/base/modernizr.custom.js",
                                "~/assets/js/base/jquery.doTimeout.js",
                                "~/assets/js/base/jquery.cookie.js",
                                "~/assets/js/base/jquery.deparam.js",
                                "~/assets/js/base/inline-confirm.js",
                                "~/assets/js/default-events.js",
                                "~/assets/js/base/bootstrap-tab-persist.js",
                                "~/assets/js/base/date-inputs.js",
                                "~/assets/js/page/modal-user-change.js"
                              //, "~/assets/js/base/jquery-ui-1.11.2.min.js"
                              ));

            bundles.Add(new bundle(bundle.BundleType.js, "dictionary.js",
                "/assets/js/base/knockout-3.0.0.js",
                "/assets/js/base/knockout-mapping-2.4.1.js",
                "/assets/js/base/jquery.doTimeout.js",
                "/assets/js/base/jquery.deparam.js",
                "/assets/js/base/jquery.cookie.js",
                "/assets/js/page/dictionary.js"
                )
            );

            bundles.Add(new bundle(bundle.BundleType.css, "print.css", "~/assets/css/print.less.css"));

            bundles.Add(new bundle(bundle.BundleType.css, "waAssembly.css",
                "~/assets/css/fontawesome/css/fontawesome-all.min.css",
                //"~/assets/css/base/bootstrap.min.css",
                "~/assets/css/bootstrap4/bootstrap.min.css",
                "~/assets/css/waAssembly.less",
                "~/assets/css/forms.less.css",
                "~/assets/css/menu.less.css"
                )
            );

            this.save();
        }







        /// <summary>
        /// The absolute path for the "root" for stylesheets. Relevant for dotLess-files only
        /// </summary>
        private string CssRootPath { get; set; }

        public static readonly string OutputPath = "~/assets/bundles/{bundleType}/";
        public static List<bundle> bundles = new List<bundle>();

        public class BundleFile
        {
            public enum IncludeAction
            {
                DefaultMinify,
                CustomMinify,
                AsIs
            }
            public BundleFile(string url, IncludeAction action)
            {
                this.url = url;
                this.action = action;
            }
            public string url { get; set; }
            public IncludeAction action { get; set; } = IncludeAction.DefaultMinify;

        }

        public class bundle
        {
            public enum BundleType
            {
                js,
                css
            }
            public bundle(BundleType bundleType)
            {
                Urls = new List<BundleFile>();
                this.OutputType = bundleType;
            }
            public bundle(BundleType bundleType, string Name, params BundleFile[] Urls)
            {
                //Urls = new List<string>();
                this.Urls = new List<BundleFile>();
                this.OutputType = bundleType;
                this.Name = Name;
                this.Urls.AddRange(Urls);
            }

            public bundle(BundleType bundleType, string Name, params string[] Urls)
            {
                //Urls = new List<string>();
                this.Urls = new List<BundleFile>();
                this.OutputType = bundleType;
                this.Name = Name;
                foreach (var url in Urls)
                {
                    this.Urls.Add(new BundleFile(url, BundleFile.IncludeAction.DefaultMinify));
                }
            }

            public string Name { get; set; }
            public List<BundleFile> Urls { get; set; }

            public BundleType OutputType { get; set; }

        }

        private void save()
        {
            if (!bundles.Any())
            {
                return;
            }
            var doubles = bundles.GroupBy(m => m.Name).Where(m => m.Count() > 1);
            if (doubles.Any())
            {
                string names = doubles.Select(m => m.Key).Aggregate((a, b) => a + ", " + b);
                throw new Exception("Bundle names specified more than once: " + names);
            }


            foreach (var item in bundles)
            {
                var path = HostingEnvironment.MapPath(OutputPath.Replace("{bundleType}", item.OutputType.ToString()));
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                // <handle wildcards>
                var urlsWithWildcard = item.Urls.Where(m => m.url.Contains("*.")).ToList();
                if (urlsWithWildcard.Any())
                {
                    var baseFolder = HostingEnvironment.MapPath("~/").ToLower();

                    var newUrls = new List<BundleFile>();
                    foreach (var wildling in urlsWithWildcard)
                    {
                        // parse string
                        var searchPattern = wildling.url.Substring(wildling.url.IndexOf("*"));
                        if (string.IsNullOrEmpty(searchPattern))
                        {
                            continue;
                        }
                        // find the folder references
                        var filePath = wildling.url.Substring(0, wildling.url.Length - searchPattern.Length);
                        var fileFolder = HostingEnvironment.MapPath(filePath);
                        if (string.IsNullOrEmpty(fileFolder) || !Directory.Exists(fileFolder))
                        {
                            continue;
                        }

                        foreach (var realFile in Directory.GetFiles(fileFolder, searchPattern))
                        {
                            newUrls.Add(new BundleFile("~/" + realFile.ToLower().Replace(baseFolder, "").Replace("\\", "/"), wildling.action));
                        }

                        item.Urls.Remove(wildling); // now remove original
                    }
                    item.Urls.AddRange(newUrls);
                }
                // </handle wildcards>

                using (var writer = System.IO.File.CreateText(Path.Combine(path, item.Name)))
                {

                    var minifier = new Microsoft.Ajax.Utilities.Minifier();

                    if (item.OutputType == bundle.BundleType.js)
                    {
                        foreach (var file in item.Urls)
                        {
                            var urlPath = HostingEnvironment.MapPath(file.url);
                            if (!File.Exists(urlPath))
                            {
                                continue;
                            }

                            string minifiedString = System.IO.File.ReadAllText(urlPath);

                            switch (file.action)
                            {
                                case BundleFile.IncludeAction.CustomMinify:
                                    minifiedString = customMinify(minifiedString);
                                    break;
                                case BundleFile.IncludeAction.DefaultMinify:
                                    minifiedString = minifier.MinifyJavaScript(minifiedString, new Microsoft.Ajax.Utilities.CodeSettings() { TermSemicolons = true });
                                    break;
                            }
                            writer.WriteLine("/*" + file.url + "*/");
                            writer.Write(minifiedString);
                            if (!string.IsNullOrEmpty(minifiedString) && !minifiedString.EndsWith(";"))
                            {
                                writer.Write(";");
                            }
                            writer.WriteLine("");
                        }
                    }
                    else
                    {
                        // css
                        foreach (var file in item.Urls)
                        {
                            var urlPath = HostingEnvironment.MapPath(file.url);
                            if (!File.Exists(urlPath))
                            {
                                continue;
                            }

                            string minifiedString = System.IO.File.ReadAllText(urlPath);
                            if (file.url.ToLower().EndsWith(".less.css") || file.url.ToLower().EndsWith(".less"))
                            {
                                var currentAppDirectory = Directory.GetCurrentDirectory();
                                Directory.SetCurrentDirectory(CssRootPath);

                                minifiedString = dotless.Core.Less.Parse(minifiedString, new dotless.Core.configuration.DotlessConfiguration()
                                {
                                    RootPath = CssRootPath,
                                    Logger = typeof(CustomBundlesConfigLogger),
                                    KeepFirstSpecialComment = false,
                                    MinifyOutput = true
                                });
                                Directory.SetCurrentDirectory(currentAppDirectory);
                            }
                            else
                            {  // plain css file
                                minifiedString = minifier.MinifyStyleSheet(minifiedString, new Microsoft.Ajax.Utilities.CssSettings() { TermSemicolons = true });
                            }

                            writer.WriteLine("/*" + file.url + "*/");
                            writer.Write(minifiedString);
                            if (!string.IsNullOrEmpty(minifiedString))
                            {
                                writer.Write(Environment.NewLine);
                            }
                            writer.WriteLine("");
                        }

                    }
                }
            }
        }

        /// <summary>
        /// un-tested
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string customMinify(string data)
        {
            return data
            .Replace(Environment.NewLine, "")
            .Replace("\t", "")
            .Replace("\n", "")
            .Replace("\r", "");
        }
    }
}


public static class BundlingExtensions
{
    public static MvcHtmlString RenderBundle(this UrlHelper helper, string BundleName)
    {
        if (dotnector.Bundling.CustomBundlesConfig.bundles == null || !dotnector.Bundling.CustomBundlesConfig.bundles.Any())
        {
            return null;
        }

        var bundle = dotnector.Bundling.CustomBundlesConfig.bundles.FirstOrDefault(m => m.Name == BundleName);
        if (bundle == null)
        {
            API.statics.log.Error("Bundle not found: " + BundleName);
            return new MvcHtmlString("<!-- bundle not found: " + BundleName + " -->");
        }

        string b = "<script type=\"text/javascript\" src=\"{0}\"></script>";
        if (bundle.OutputType == dotnector.Bundling.CustomBundlesConfig.bundle.BundleType.css)
        {
            b = "<link rel=\"stylesheet\" href=\"{0}\" />";
        }
        var compilation = (CompilationSection)ConfigurationManager.GetSection("system.web/compilation");
        if (compilation.Debug)
        {
            string output = "";
            foreach (var url in bundle.Urls)
            {
                output += string.Format(b, url.url.Replace("~", "") + "?v=" + Guid.NewGuid()) + Environment.NewLine;
            }
            return new MvcHtmlString(output);
        }
        else
        {
            return new MvcHtmlString(string.Format(b, (dotnector.Bundling.CustomBundlesConfig.OutputPath.Replace("{bundleType}", bundle.OutputType.ToString()) + bundle.Name + "?v=" + API.statics.App.Version)).Replace("~", ""));
        }

    }
}

public class CustomBundlesConfigLogger : ILogger
{
    public void Log(LogLevel level, string message)
    {
        API.statics.log.Info("dotLess generic: " + message);
    }
    public void Info(string message)
    {
        API.statics.log.Info("dotLess: " + message);
    }

    public void Debug(string message)
    {
        API.statics.log.Debug("dotLess: " + message);
    }

    public void Warn(string message)
    {
        API.statics.log.Warn("dotLess: " + message);
    }

    public void Error(string message)
    {
        API.statics.log.Error("dotLess: " + message);
    }


    public void Info(string message, params object[] args)
    {
        API.statics.log.Info("dotLess: " + message + ". args: " + args);
    }

    public void Debug(string message, params object[] args)
    {
        API.statics.log.Debug("dotLess: " + message + ". args: " + args);
    }

    public void Warn(string message, params object[] args)
    {
        API.statics.log.Warn("dotLess: " + message + ". args: " + args);
    }

    public void Error(string message, params object[] args)
    {
        API.statics.log.Error("dotLess: " + message + ". args: " + args);
    }


    public HttpResponseBase Response { get; set; }
}