﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using API.Data;

namespace waAssembly.Models
{
	public class ValidationTestModel : IValidatableObject
	{

		public int ID { get; set; }

		[TransRequired]
		public string Name { get; set; }

		public int? CategoryID { get; set; }

		[Range(1.0, 10.0, ErrorMessage = "Sub category value must be between 1 and 10")]
		public int? SubCategoryID { get; set; }


		public List<ValidationTestCategoryModel> AvailableCategories { get; set; } = new List<ValidationTestCategoryModel>();

		public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
		{
			if (SubCategoryID.HasValue && !CategoryID.HasValue) {
				yield return new ValidationResult("Must select sub category when main category selected");
			}
		}

	}

	public class ValidationTestCategoryModel { 
		public int ID { get; set; }
		public string Name { get; set; }

	}
}