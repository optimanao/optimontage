﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace waAssembly.Models {
	public class ButtonsModel
	{

		public ButtonsModel()
		{
			OmitButtonNumber = 0;
		}

		public int ButtonCount { get; set; }

		public int OmitButtonNumber { get; set; }

		public bool TwoButtons { get; set; }

		public bool HideIcons { get; set; }

		public bool OmitSpacer { get; set; }

		/// <summary>
		/// Prevent floating
		/// </summary>
		public bool Fixed { get; set; }

		public string CancelUrl { get; set; }

	}
}