﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using API.Data;

public static class ExtensionMethodsObject
{

	// http://stackoverflow.com/questions/5474460/get-displayname-attribute-of-a-property-in-strongly-typed-way
	public static string GetDisplayName<TModel, TProperty>(
	this TModel model
 , Expression<Func<TModel, TProperty>> expression)
	{
		return ModelMetadata.FromLambdaExpression<TModel, TProperty>(
			 expression,
			 new ViewDataDictionary<TModel>(model)
			 ).DisplayName;
	}
}

public static class ExtensionMethods
{
	public static string NoSlash(this string s)
	{
		return (string.IsNullOrEmpty(s)
			? s
			: System.Text.RegularExpressions.Regex.Replace(s.Replace("/", "_").Replace(" ", "-"), @"[^A-Za-z0-9\-\s]", "")
		);
	}


}

public static class ModelStateDictionaryExtensions
{
	public static void Merge(this ModelStateDictionary modelState, IDictionary<string, string> dictionary, string prefix = "")
	{
		foreach (var item in dictionary)
		{
			var key = (string.IsNullOrEmpty(prefix) ? "" : (prefix + ".")) + item.Key;
			if (modelState.ContainsKey(key))
			{
				if (modelState[key].Errors.Count == 0) { 
					modelState.AddModelError(key, item.Value);
				}
			}
			else {
				modelState.AddModelError(key, item.Value);
			}
		}
	}
}