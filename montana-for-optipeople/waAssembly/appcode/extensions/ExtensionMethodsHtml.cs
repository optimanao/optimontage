﻿using API;
using System;
using System.ComponentModel;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;


public static class ExtensionMethodsHtml
{

    public static MvcHtmlString ImgForBoolean(this HtmlHelper helper, bool? boolean)
    {
        bool b = boolean ?? false;
        string ret = "<img src=\"/img/o" + (b ? "n" : "ff") + ".png\" alt=\"\" />";
        return new MvcHtmlString(ret);
    }

    public static MvcHtmlString BooleanFaIcon(this HtmlHelper helper, bool? boolean)
    {
        return BooleanFaIcon(boolean ?? false);
    }

    public static MvcHtmlString BooleanFaIcon(this bool? boolean)
    {
        return BooleanFaIcon(boolean ?? false);
    }

    public static MvcHtmlString BooleanFaIcon(this bool boolean)
    {
        bool b = boolean;
        string ret = "<i class=\"fa fa-" + (b ? "check-circle green" : "circle red") + " fa-lg\"></i>";
        return new MvcHtmlString(ret);
    }


    /// <summary>
    /// Produce the return url to use in links
    /// </summary>
    /// <param name="url"></param>
    /// <param name="WithParam">Indicates that a querystring variable should be returned. True = "/?ru=query". False = "query".</param>
    /// <param name="IncludeExistingRu"></param>
    /// <returns></returns>
    public static string MakeReturnUrl(this UrlHelper url, bool WithParam = false, bool IncludeExistingRu = false)
    {
        var context = url.RequestContext.HttpContext;
        string pq = context.Request.Url.LocalPath;

        string ru = pq;
        foreach (var key in context.Request.QueryString.AllKeys)
        {
            if (key == "ru" && !IncludeExistingRu)
                continue;
            ru += (ru == pq ? "?" : "&");
            ru += key + "=" + context.Request.QueryString[key];
        }

        if (ru.EndsWith("&"))
            ru = ru.StripFromLast("&");

        if (WithParam)
            return (context.Request.QueryString.Count == 0 ? "?" : "&") + "ru=" + HttpUtility.UrlEncode(ru);
        else
            return HttpUtility.UrlEncode(ru);
    }

    /// <summary>
    /// Gets any passed return url from the current query
    /// </summary>
    /// <param name="url"></param>
    /// <param name="UrlIfEmpty"></param>
    /// <returns></returns>
    public static string ReturnUrl(this UrlHelper url, string UrlIfEmpty = "")
    {
        var context = url.RequestContext.HttpContext;
        if (context.Request.QueryString["ru"] != null)
        {
            return context.Server.UrlDecode(context.Request.QueryString["ru"]);
        }
        else
        {
            return (UrlIfEmpty != "" ? UrlIfEmpty : String.Empty);
        }
    }

    //Builds URL by finding the best matching route that corresponds to the current URL,
    //with given parameters added or replaced.
    public static MvcHtmlString CurrentUrl(this UrlHelper helper, object substitutes)
    {
        //get the route data for the current URL e.g. /Research/InvestmentModelling/RiskComparison
        //this is needed because unlike UrlHelper.Action, UrlHelper.RouteUrl sets includeImplicitMvcValues to false
        //which causes it to ignore current ViewContext.RouteData.Values
        var rd = new RouteValueDictionary(helper.RequestContext.RouteData.Values);

        //get the current query string e.g. ?BucketID=17371&amp;compareTo=123
        var qs = helper.RequestContext.HttpContext.Request.QueryString;

        //add query string parameters to the route value dictionary
        foreach (string param in qs)
            if (!string.IsNullOrEmpty(qs[param]))
                rd[param] = qs[param];

        //override parameters we're changing
        foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(substitutes.GetType()))
        {
            var value = "" + property.GetValue(substitutes);
            if (string.IsNullOrEmpty(value)) rd.Remove(property.Name); else rd[property.Name] = value;
        }
        //UrlHelper will find the first matching route
        //(the routes are searched in the order they were registered).
        //The unmatched parameters will be added as query string.
        var url = helper.RouteUrl(rd);
        return new MvcHtmlString(url);
    }

    //[Obsolete("Use with API.Data.DictionaryKeyStrongType instead")]
    public static MvcHtmlString T(this HtmlHelper helper, string TranslationKey)
    {
        return new MvcHtmlString(statics.DicValue(TranslationKey).Replace(Environment.NewLine, "<br />"));
    }

    public static MvcHtmlString T(this HtmlHelper helper, string TranslationKey, bool ToLower)
    {
        if (ToLower)
            return new MvcHtmlString(statics.DicValue(TranslationKey).ToLower());
        else
            return new MvcHtmlString(statics.DicValue(TranslationKey));
    }

    public static IHtmlString Traw(this HtmlHelper helper, string TranslationKey)
    {
        return helper.Raw(statics.DicValue(TranslationKey));
    }


    //public static MvcHtmlString T(this HtmlHelper helper, string TranslationKey) {
    //	return new MvcHtmlString(statics.DicValue(TranslationKey).Replace(Environment.NewLine, "<br />"));
    //}

    //public static MvcHtmlString T(this HtmlHelper helper, string TranslationKey, bool ToLower) {
    //	if (ToLower)
    //		return new MvcHtmlString(statics.DicValue(TranslationKey).ToLower());
    //	else
    //		return new MvcHtmlString(statics.DicValue(TranslationKey));
    //}

    //public static IHtmlString Traw(this HtmlHelper helper, string TranslationKey) {
    //	return helper.Raw(statics.DicValue(TranslationKey));
    //}

    public static string IfEmpty(this string Text, string DisplayIfEmpty)
    {
        return (String.IsNullOrEmpty(Text) ? DisplayIfEmpty : Text);
    }

    public static string IfEmpty(this string Text, string DisplayIfEmpty, string PrependIfNotEmpty)
    {
        return (String.IsNullOrEmpty(Text) ? DisplayIfEmpty : PrependIfNotEmpty + Text);
    }

    public static string ToDefString(this DateTime dt, bool IncludeTimeStamp = false)
    {
        var date = dt;
        if (dt == null)
            date = DateTime.Now;

        return dt.ToString("yyyy-MM-dd" + (IncludeTimeStamp ? " HH:mm" : ""));
    }

    public static string ToTimeString(this DateTime dt)
    {
        var date = dt;
        if (dt == null)
            date = DateTime.Now;

        return dt.ToString("HH:mm");
    }

    public static string ToTimeString(this TimeSpan ts, bool IncludeSeconds = false)
    {
        var time = ts;
        if (time == null)
            return "";

        string s = "";
        int days = ts.Days;
        int hours = ts.Hours;
        int minutes = ts.Minutes;

        s += (days > 0 ? days.ToString().PadLeft(2, '0') + ":" : "") 
            + hours.ToString().PadLeft(2, '0') + ":" 
            + minutes.ToString().PadLeft(2, '0') 
            + (IncludeSeconds ? ":" + ts.ToString("ss") : "");
        return s;
    }

    public static IHtmlString ToHMTimeString(this TimeSpan ts)
    {
        var time = ts;
        if (time == null)
            return MvcHtmlString.Empty;

        string s = "";
        int hours = (int)Math.Floor(ts.TotalHours);
        int minutes = ts.Minutes;
        if (ts.Seconds > 30)
            minutes += 1;
        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
        string strMinutes = (minutes < 10 ? "0" : "") + minutes;
        s += hours.ToString() + "<span class=\"note-hm\">h</span>" + strMinutes + "<span class=\"note-hm\">min</span>";
        return MvcHtmlString.Create(s);
    }

    public static IHtmlString ToHMTimeStringPlain(this TimeSpan ts)
    {
        var time = ts;
        if (time == null)
            return MvcHtmlString.Empty;

        string s = "";
        int hours = (int)Math.Floor(ts.TotalHours);
        int minutes = ts.Minutes;
        if (ts.Seconds > 30)
            minutes += 1;

        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
        string strMinutes = (minutes < 10 ? "0" : "") + minutes;

        s += hours.ToString() + ":" + strMinutes + "";
        return MvcHtmlString.Create(s);
    }

    public static IHtmlString ToHMTimeStringPlainTicks(this TimeSpan ts)
    {
        var time = ts;
        if (time == null)
            return MvcHtmlString.Empty;

        string s = "";
        int hours = (int)Math.Floor(ts.TotalHours);
        int minutes = ts.Minutes;
        if (ts.Seconds > 30)
            minutes += 1;

        if (minutes == 60)
        {
            minutes = 0;
            hours++;
        }
        string strMinutes = (minutes < 10 ? "0" : "") + minutes;

        s += hours.ToString() + "\" " + strMinutes + "'";
        return MvcHtmlString.Create(s);
    }

    public static string JsEncode(this string JsStringValue)
    {
        if (string.IsNullOrEmpty(JsStringValue))
            return JsStringValue;
        else
            return JsStringValue.Replace("'", "").Replace("\r\n", "<br />").Replace("\n", "<br />");
    }

    public static string TrimIfLong(this string Text, int MaxLength)
    {
        return (!String.IsNullOrEmpty(Text) && Text.Length > MaxLength ? Text.Substring(0, MaxLength) + "..." : Text);
    }

    public static string WrapInTag(this string Text, string Tag, string CssClass = "")
    {
        return "<" + Tag + (String.IsNullOrEmpty(CssClass) ? "" : " class=\"" + CssClass + "\"") + ">" + Text + "</" + Tag + ">";
    }

    public static string AppVersion(this HtmlHelper html)
    {
        var appInstance = html.ViewContext.HttpContext.ApplicationInstance;
        //given that you should then be able to do 
        var assemblyVersion = appInstance.GetType().BaseType.Assembly.GetName().Version;
        //note the use of the BaseType
        return assemblyVersion.ToString();

        //@ViewContext.HttpContext.ApplicationInstance.GetType().BaseType.Assembly.GetName().Version.ToString();
    }

    public static int PercentageOf(this long Value, long OtherValue)
    {
        if (Value == 0 || OtherValue == 0)
            return 0;
        var b = ((int)Math.Round(((decimal)Value / (decimal)OtherValue) * 100));
        return b;
    }

    public static string Capitalize(this string Text)
    {
        return (String.IsNullOrEmpty(Text) ? String.Empty : Text.Substring(0, 1).ToUpper() + Text.Substring(1));
    }
    public static MvcHtmlString ToUSDstring(this double AmountUSD)
    {
        if (AmountUSD < 0)
        {
            return new MvcHtmlString("($" + Math.Round(Math.Abs((double)AmountUSD / 100), 2).ToString("N2") + ")");
        }
        else
        {
            return new MvcHtmlString("$" + Math.Round((double)AmountUSD / 100, 2).ToString("N2"));
        }
    }
    public static MvcHtmlString ToUSDstring(this int AmountUSD)
    {
        if (AmountUSD < 0)
        {
            return new MvcHtmlString("($" + Math.Round(Math.Abs((double)AmountUSD / 100), 2).ToString("N2") + ")");
        }
        else
        {
            return new MvcHtmlString("$" + Math.Round((double)AmountUSD / 100, 2).ToString("N2"));
        }
    }

    public static decimal PercentageOf(this int Value, int Total)
    {
        if (Value <= 0 || Total <= 0) {
            return 0;
        }

        var diff = (decimal)Total - (decimal)Value;
        var p = (diff / Total) * 100M;
        return (100M - p);
    }

}
