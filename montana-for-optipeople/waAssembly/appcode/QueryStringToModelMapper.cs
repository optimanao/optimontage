﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace waAssembly {
	public static class QueryStringToModelMapper {
		public static T QueryStringMap<T>(this object O, NameValueCollection QS) where T : class {
			var props = O.GetType().GetProperties();
			foreach (var key in QS.AllKeys) {
				var prop = props.FirstOrDefault(n => n.Name == key);
				if (prop != null) {
					var converter = TypeDescriptor.GetConverter(prop.PropertyType);
					var result = converter.ConvertFrom(QS[key]);
					try {
						O.GetType().GetProperty(prop.Name).SetValue(O, result);
					} catch (Exception) { }
				}
			}
			return (T)O;
		}

	}
}