﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API.Data;

namespace API {
	/// <summary>
	/// A base view type for easy access to current language, user etc
	/// </summary>
	public abstract class SiteView<T> : WebViewPage<T> {

        private API.web.UserIdentityData _currentUser = null;
        private long? _currentWorkstationId = null;

		public API.web.UserIdentityData CurrentUser {
			get {
                if (_currentUser == null) {
                    _currentUser = (new API.SiteController()).CurrentUser;
                }
                return _currentUser;
			}
		}

        public long? CurrentWorkstationID
        {
            get
            {
                if ((_currentWorkstationId ?? 0) < 1)
                {
                    _currentWorkstationId = (new API.SiteController()).CurrentWorkstationID;
                }
                return _currentWorkstationId;
            }
        }

        //public string Culture {
        //	get { 
        //		return System.Threading.Thread.CurrentThread.CurrentCulture.Name; 
        //	}
        //}

    }
}