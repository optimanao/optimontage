﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using API;
using API.Data;
using WebMatrix.WebData;
using waAssembly.App_Start;
using waAssembly.Services;
using dotUtils;

namespace API
{

	public class SiteController : Controller, IDisposable
	{
       
        protected override void Initialize(RequestContext requestContext)
		{
			var context = requestContext.HttpContext;
			var cookie = context.Request.Cookies[statics.App.GetSetting(enums.SettingsKeys.CultureCookieName)];
			if (cookie != null)
			{
				SetCulture(cookie.Value, context);
			}
			else
			{
                var langs = statics.Cache.LanguagesActive();

                //string[] langs = new string[] { "da-DK", "en-US" }; // remember this when adding new language
				if (context.Request.UserLanguages != null && langs.Any(m => m.Locale == context.Request.UserLanguages[0]))
				{
					SetCulture(context.Request.UserLanguages[0], context);
				}
				else
				{
					SetCulture("da-DK", context);
				}
			}

            // <update auth cookie>
            if (requestContext?.HttpContext?.Session != null && CurrentUser != null)
            {
                var cscTimestamp = statics.App.GetSession<DateTime>(enums.SessionKeys.ClientsideCookiePersistTime);

                if (cscTimestamp != null && DateTime.UtcNow.Subtract(cscTimestamp).TotalMinutes > 15)
                {
                    statics.App.SetSession(enums.SessionKeys.ClientsideCookiePersistTime, DateTime.UtcNow);
                    var accountControllerService = SimpleInjectorInitializer.container.GetInstance<AccountService>();
                    var _response = Response ?? requestContext.HttpContext.Response;
                    if (_response != null)
                    {
                        accountControllerService.FormsAuthTicketSetup(CurrentUser.ID, true, _response, CurrentUser.ImpersonatingUserID);
                    }
                }
            }
            // </update auth cookie>

            base.Initialize(requestContext);
		}

		public void SetCulture(string lang, HttpContextBase context = null)
		{
            var langs = statics.Cache.LanguagesActive();
            if (!langs.Any(m => m.Locale == lang)) {
                lang = langs.First().Locale;
            }

            CultureInfo ci = new CultureInfo(lang);
			System.Threading.Thread.CurrentThread.CurrentUICulture = ci;
			System.Threading.Thread.CurrentThread.CurrentCulture = ci; // CultureInfo.CreateSpecificCulture(ci.Name);

			//// Force a valid culture in the URL
			//RouteData.Values["lang"] = lang;

			string cookieDomain = statics.App.GetSetting(enums.SettingsKeys.AuthCookieDomain);

			// save the localization into cookie
			HttpCookie _cookie = new HttpCookie(statics.App.GetSetting(enums.SettingsKeys.CultureCookieName), Thread.CurrentThread.CurrentCulture.Name);
			_cookie.Expires = DateTime.Now.AddYears(1);
			if (cookieDomain != null && !cookieDomain.Contains("localhost"))
				_cookie.Domain = cookieDomain;

			(context ?? HttpContext).Response.SetCookie(_cookie);
		}

		public API.web.UserIdentityData CurrentUser
		{
			get
			{
				try
				{
					return ((API.web.UserIdentity)System.Web.HttpContext.Current.User.Identity).UserData;
				}
				catch (Exception)
				{
					return null;
				}
			}
		}

        public long? CurrentWorkstationID
        {
            get
            {
                try
                {
                    var cookie = System.Web.HttpContext.Current.Request.Cookies[statics.App.GetSetting(enums.SettingsKeys.WorkstationCookieName)];
                    if (cookie == null)
                    {
                        return null; //throw new Exception("Cannot determine current workstation");
                    }
                    return long.Parse("" + cookie.Value);
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public static string DicValue(string TranslationKey)
		{
			return API.statics.DicValue(TranslationKey);
		}

		public static void DictionaryReload(bool Force = false)
		{
			if (Force || CacheHandler.ItemExpired("Dictionary"))
			{
				Dictionary dic = new Dictionary();
				using (var db = new API.Data.DAL())
				{
					dic.ParseEntries(db.DictionarySectionGetCollection(), db.DictionaryEntryGetCollection().ToList());
				}
				CacheHandler.AddItem("Dictionary", dic, 60);

				var path = System.Web.Hosting.HostingEnvironment.MapPath("~/assets/js");
				foreach (var language in statics.Cache.LanguagesActive())
				{

					using (var fs = System.IO.File.CreateText(path + "\\dictionary-" + language.Locale + ".js"))
					{
						fs.Write("clientside.dictionary = { ");

						/*foreach (Match m in Regex.Matches(value, "{@\\w.+}")) {
				string repValue = dic.GetValue(m.Value.Replace("{@", "").Replace("}", ""), System.Threading.Thread.CurrentThread.CurrentCulture.Name);
				if (!repValue.StartsWith("not translated:")) {
					value = value.Replace(m.Value, repValue);
				}
			}*/
						foreach (var key in dic.ClientsideKeys)
						{
							//fs.Write("'" + key.Replace(".", "_") + "': '" + HttpUtility.JavaScriptStringEncode(dic.GetValue(key, language.Locale)) + "',");
							fs.Write("'" + key.Replace(".", "_") + "': '" + HttpUtility.JavaScriptStringEncode(statics.DicValue(key, language.Locale)) + "',");
						}
						fs.Write("_eof: null };");
					}
				}
			}
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="ViewName"></param>
		/// <param name="ID">marked as object to take both int and long. Q'n'D FTW!</param>
		/// <returns></returns>
		public ActionResult ReturnToView(string ViewName, object ID = null, string IndexView = "", object IndexModel = null)
		{
			var returnUrl = Request["ru"];
			if (String.IsNullOrEmpty(returnUrl))
			{
				returnUrl = Request["ReturnUrl"];
			}
			var saveAction = Request["saveAction"];
			if (!String.IsNullOrEmpty(saveAction) && saveAction.ToLower() == "save")
			{
				return RedirectToAction("Edit", new
				{
					ID = ID
				});
			}
			else
			{
				if (!String.IsNullOrEmpty(returnUrl))
				{
					var returnUrlDecode = HttpUtility.UrlDecode(returnUrl).ToLower();
					if (returnUrlDecode != Request.Url.AbsolutePath.ToLower())
					{
						return Redirect(HttpUtility.UrlDecode(returnUrl));
					}
					else
					{
						if (!string.IsNullOrEmpty(IndexView))
						{
							return RedirectToAction(IndexView, IndexModel);
						}
						else
						{
							return RedirectToAction("Index");
						}
					}
				}
				if (!string.IsNullOrEmpty(IndexView))
				{
					return RedirectToAction(IndexView, IndexModel);
				}
				else
				{
					return RedirectToAction("Index");
				}
			}
		}
		//public ActionResult ReturnToView(string ViewName, object ID = null)
		//{
		//	var returnUrl = Request["ru"];
		//	if (String.IsNullOrEmpty(returnUrl))
		//	{
		//		returnUrl = Request["ReturnUrl"];
		//	}
		//	var saveAction = Request["saveAction"];
		//	if (!String.IsNullOrEmpty(saveAction) && saveAction.ToLower() == "save")
		//	{
		//		return RedirectToAction("Edit", new { ID = ID });
		//	}
		//	else
		//	{
		//		if (!String.IsNullOrEmpty(returnUrl))
		//		{
		//			var returnUrlDecode = HttpUtility.UrlDecode(returnUrl).ToLower();
		//			if (returnUrlDecode != Request.Url.AbsolutePath.ToLower())
		//			{
		//				return Redirect(HttpUtility.UrlDecode(returnUrl));
		//			}
		//			else
		//			{
		//				return RedirectToAction("Index");
		//			}
		//		}
		//		return RedirectToAction("Index");
		//	}

		//}

		public ActionResult RedirToErrorPage(string Message)
		{
			TempData[enums.TempDataKeys.MessageError.ToString()] = Message;
			return RedirectToAction("Index", "Home");
		}



        public void Thumbs(bool Up, string Message = "")
        {
            if (Up)
            {
                ThumbsUp(Message);
            }
            else
            {
                ThumbsDown(Message);
            }
        }

        public void ThumbsUp(string SuccessMessage = "")
		{
			TempData[enums.TempDataKeys.ShowOk.ToString()] = true;

            if (!String.IsNullOrEmpty(SuccessMessage))
            {
                TempData[enums.TempDataKeys.MessageSuccess.ToString()] = SuccessMessage;
            }
        }

		public void ThumbsDown(string ErrorMessage = "")
		{
			TempData[enums.TempDataKeys.ShowError.ToString()] = true;

            if (!String.IsNullOrEmpty(ErrorMessage))
            {
                TempData[enums.TempDataKeys.MessageError.ToString()] = ErrorMessage;
            }
        }

		public void ThumbsUpCancel()
		{
			TempData[enums.TempDataKeys.ShowOk.ToString()] = null;
		}

		public void ThumbsDownCancel()
		{
			ViewBag.Error = null;
			TempData[enums.TempDataKeys.ShowError.ToString()] = null;
		}
		
		public void MessageWarning(string Message)
		{
			ViewBag.Warning = Message;
			TempData[enums.TempDataKeys.MessageWarning.ToString()] = Message;
		}

        public void MessageInfo(string Message)
        {
            ViewBag.Warning = Message;
            TempData[enums.TempDataKeys.MessageInfo.ToString()] = Message;
        }

        public void MessageSuccess(string Message)
        {
            ViewBag.Warning = Message;
            TempData[enums.TempDataKeys.MessageSuccess.ToString()] = Message;
        }

        //public bool CompanyIDMismatch(object Entity) {
        //	if (Entity == null) {
        //		return true;
        //	}
        //	bool ismatch = true;

        //	var prop = Entity.GetType().GetProperty("CompanyID");
        //	if (prop != null) {
        //		var value = prop.GetValue(Entity);
        //		ismatch = !value.Equals(CurrentUser.CompanyID);
        //	} else {
        //		statics.log.Error(Entity.GetType() + " type doesn't contain CompanyID property.");
        //	}
        //	return ismatch;
        //}

    }
}