﻿using API.Data;
using System.Web;
using System.Web.Mvc;

namespace waAssembly
{
    /// <summary>
    /// 
    /// </summary>
    public class dotAuthorizeAttribute : AuthorizeAttribute
    {

        /// <summary>
        /// Leave blank to allow all users
        /// </summary>
        //public enums.UserType[] AllowedRoles;

        public bool OnlyAdmin { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {

            if (httpContext == null)
            {
                return false;
            }

            var session = httpContext.Session;
            if (session == null)
            {
                return false;
            }

            var isHardwiredAdmin = ("" + session["adminOk"] == "true");
            if (OnlyAdmin && isHardwiredAdmin)
            {
                return true;
            }

            var identity = httpContext.User.Identity as API.web.UserIdentity;
            if (identity == null)
            {
                return false;
            }
            else
            {
                var userdata = identity.UserData;
                if (userdata == null)
                {
                    return false;
                }

                if (OnlyAdmin)
                {
                    return userdata.IsAdmin;
                }

            }
            return true;

        }
    }
}