﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace waAssembly.Filters {
	public class ResetViewBagAttribute : ActionFilterAttribute {

		public override void OnActionExecuting(ActionExecutingContext filterContext) {
			base.OnActionExecuting(filterContext);

			var controller = filterContext.Controller;
			controller.ViewBag.Message 
				= controller.ViewBag.Error 
				= controller.ViewBag.Warning 
				= controller.ViewBag.Success 
				= String.Empty;

			// IRL this is never going to work :-(
			//// don't invok "PopulateViewBag" for any other actions than Create and Edit
			//if (!new string[] { "Create", "Edit" }.Contains(filterContext.ActionDescriptor.ActionName))
			//	return;

			//var pvb = controller.GetType().GetMethod("PopulateViewBag");
			//if (pvb != null) {
			//	pvb.Invoke(filterContext.Controller, new object[] { null });
			//}

		}

	}
}