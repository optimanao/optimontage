﻿using API;
using API.Data;
using System.Web.Mvc;

namespace waAssembly 
{
    public class WorkstationRequiredAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!filterContext.HttpContext.Request.RawUrl.ToLower().StartsWith("/home/error"))
            {
                var c = new SiteController();
                if (c.CurrentWorkstationID == null)
                {
                    filterContext.Controller.TempData[enums.TempDataKeys.MessageError.ToString()] = "Ukendt arbejdsstation";
                    filterContext.Result = new RedirectResult("/home/error");
                }
            }

        }
    }
}