﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace waAssembly {
	public class GlobalIdentityInjector : ActionFilterAttribute, IAuthorizationFilter {
		public void OnAuthorization(AuthorizationContext filterContext) {
			var identity = filterContext.HttpContext.User.Identity;

			// do some stuff here and assign a custom principal:
			//var principal = new API.dotPrincipal(identity, null);
			// here you can assign some custom property that every user 
			// (even the non-authenticated have)

			// set the custom principal
			//filterContext.HttpContext.User = principal;
		}
	}
}