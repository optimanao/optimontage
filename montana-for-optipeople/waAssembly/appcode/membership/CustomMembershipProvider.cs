﻿using System;
using System.Linq;
using System.Configuration;
using System.Collections.Specialized;
using System.Configuration.Provider;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;
using System.Text;
using System.Web.Configuration;
using System.Web.Security;
using API;
using System.Collections.Generic;
using WebMatrix.WebData;

namespace waAssembly {
	//http://www.mattwrock.com/post/2009/10/14/implementing-custom-membership-provider-and-role-provider-for-authinticating-aspnet-mvc-applications.aspx

	//http://bojanskr.blogspot.dk/2011/12/custom-membership-provider.html

	public sealed class CustomMembershipProvider : ExtendedMembershipProvider {

		#region Class Variables

		//private int newPasswordLength = 8;
		//private string connectionString;
		private string applicationName;
		private bool enablePasswordReset;
		private bool enablePasswordRetrieval;
		private bool requiresQuestionAndAnswer;
		private bool requiresUniqueEmail;
		private int maxInvalidPasswordAttempts;
		private int passwordAttemptWindow;
		private MembershipPasswordFormat passwordFormat = MembershipPasswordFormat.Encrypted;
		private int minRequiredNonAlphanumericCharacters;
		private int minRequiredPasswordLength;
		private string passwordStrengthRegularExpression;
		//private MachineKeySection machineKey; //Used when determining encryption key values.

		#endregion

		#region Properties

		public override string ApplicationName {
			get {
				return applicationName;
			}
			set {
				applicationName = value;
			}
		}

		public override bool EnablePasswordReset {
			get {
				return enablePasswordReset;
			}
		}

		public override bool EnablePasswordRetrieval {
			get {
				return enablePasswordRetrieval;
			}
		}

		public override bool RequiresQuestionAndAnswer {
			get {
				return requiresQuestionAndAnswer;
			}
		}

		public override bool RequiresUniqueEmail {
			get {
				return requiresUniqueEmail;
			}
		}

		public override int MaxInvalidPasswordAttempts {
			get {
				return maxInvalidPasswordAttempts;
			}
		}

		public override int PasswordAttemptWindow {
			get {
				return passwordAttemptWindow;
			}
		}

		public override MembershipPasswordFormat PasswordFormat {
			get {
				return passwordFormat;
			}
		}

		public override int MinRequiredNonAlphanumericCharacters {
			get {
				return minRequiredNonAlphanumericCharacters;
			}
		}

		public override int MinRequiredPasswordLength {
			get {
				return minRequiredPasswordLength;
			}
		}

		public override string PasswordStrengthRegularExpression {
			get {
				return passwordStrengthRegularExpression;
			}
		}

		#endregion

		#region MembershipProvider overrides

		public override void Initialize(string name, NameValueCollection config) {
			if (config == null) {
				string configPath = "~/web.config";
				Configuration NexConfig = WebConfigurationManager.OpenWebConfiguration(configPath);
				MembershipSection section = (MembershipSection)NexConfig.GetSection("system.web/membership");
				ProviderSettingsCollection settings = section.Providers;
				NameValueCollection membershipParams = settings[section.DefaultProvider].Parameters;
				config = membershipParams;
			}

			if (name == null || name.Length == 0) {
				name = "CustomMembershipProvider";
			}

			if (String.IsNullOrEmpty(config["description"])) {
				config.Remove("description");
				config.Add("description", "Custom Membership Provider");
			}

			//Initialize the abstract base class.
			base.Initialize(name, config);

			applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
			maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
			passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
			minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredAlphaNumericCharacters"], "1"));
			minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));
			passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], String.Empty));
			enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
			enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
			requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
			requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));

			string temp_format = config["passwordFormat"];
			if (temp_format == null) {
				temp_format = "Encrypted";
			}

			switch (temp_format) {
				case "Hashed":
					passwordFormat = MembershipPasswordFormat.Hashed;
					break;
				case "Encrypted":
					passwordFormat = MembershipPasswordFormat.Encrypted;
					break;
				case "Clear":
					passwordFormat = MembershipPasswordFormat.Clear;
					break;
				default:
					throw new ProviderException("Password format not supported.");
			}

			//ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];

			//if ((ConnectionStringSettings == null) || (ConnectionStringSettings.ConnectionString.Trim() == String.Empty)) {
			//	throw new ProviderException("Connection string cannot be blank.");
			//}

			//connectionString = ConnectionStringSettings.ConnectionString;

			//Get encryption and decryption key information from the configuration.
			System.Configuration.Configuration cfg = WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
			//machineKey = cfg.GetSection("system.web/machineKey") as MachineKeySection;

			//if (machineKey.ValidationKey.Contains("AutoGenerate")) {
			//	if (PasswordFormat != MembershipPasswordFormat.Clear) {
			//		throw new ProviderException("Hashed or Encrypted passwords are not supported with auto-generated keys.");
			//	}
			//}
		}

		public override bool ChangePassword(string username, string oldPassword, string newPassword) {
			throw new NotImplementedException();

			//if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(oldPassword) || string.IsNullOrWhiteSpace(newPassword)) return false;

			//if (oldPassword == newPassword) return false;

			//using (var ipa = new API.Data.DAL()) {
			//	API.Data.User user = GetUser(username);

			//	if (user == null) return false;

			//	if (string.IsNullOrWhiteSpace(user.Password)) return false;

			//	user.Password = EncodePassword(newPassword);

			//	//ipa.UserSave(user);
			//}
			//return true;
		}

		public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer) {
			throw new NotImplementedException();
		}

		/// <summary>
		/// Create custom CustomMembershipUser.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="email"></param>
		/// <param name="passwordQuestion"></param>
		/// <param name="passwordAnswer"></param>
		/// <param name="isApproved"></param>
		/// <param name="providerUserKey"></param>
		/// <param name="status"></param>
		/// <param name="companyID"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public API.Data.User CreateUser(
				  string username,
				  string password,
				  string email,
				  string passwordQuestion,
				  string passwordAnswer,
				  bool isApproved,
				  object providerUserKey,
				  out MembershipCreateStatus status,
				  int companyID,
				  string name,
				  string phoneNumber) {

			//ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

			//OnValidatingPassword(args);

			//if (args.Cancel) {
			//	status = MembershipCreateStatus.InvalidPassword;
			//	return null;
			//}

			//if ((RequiresUniqueEmail && (GetUserNameByEmail(email) != String.Empty))) {
			//	status = MembershipCreateStatus.DuplicateEmail;
			//	return null;
			//}

			//CustomMembershipUser CustomMembershipUser = GetUser(username);

			//if (CustomMembershipUser == null) {
			//	try {
			//		using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//			User user = new User();
			//			user.CompanyFK = companyID;
			//			user.Name = name;
			//			user.UserName = username;
			//			user.Password = EncodePassword(password);
			//			user.Email = email.ToLower();
			//			user.CreatedOn = DateTime.Now;
			//			user.ModifiedOn = DateTime.Now;
			//			user.Phone = phoneNumber;
			//			_db.Users.InsertOnSubmit(user);

			//			_db.SubmitChanges();

			//			status = MembershipCreateStatus.Success;

			//			return GetUser(username);
			//		}

			//	} catch {
			//		status = MembershipCreateStatus.ProviderError;
			//	}
			//} else {
			//	status = MembershipCreateStatus.DuplicateUserName;
			//}

			status = MembershipCreateStatus.ProviderError;

			return null;
		}

		/// <summary>
		/// Createa MembershipUser.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="email"></param>
		/// <param name="passwordQuestion"></param>
		/// <param name="passwordAnswer"></param>
		/// <param name="isApproved"></param>
		/// <param name="providerUserKey"></param>
		/// <param name="status"></param>
		/// <returns></returns>
		public override MembershipUser CreateUser(
			 string username,
			 string password,
			 string email,
			 string passwordQuestion,
			 string passwordAnswer,
			 bool isApproved,
			 object providerUserKey,
			 out MembershipCreateStatus status) {
			ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

			//OnValidatingPassword(args);

			//if (args.Cancel) {
			//	status = MembershipCreateStatus.InvalidPassword;
			//	return null;
			//}

			//if ((RequiresUniqueEmail && (GetUserNameByEmail(email) != String.Empty))) {
			//	status = MembershipCreateStatus.DuplicateEmail;
			//	return null;
			//}

			//MembershipUser membershipUser = GetUser(username, false);

			//if (membershipUser == null) {
			//	try {
			//		using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//			User user = new User();
			//			user.CompanyFK = 0;
			//			user.Name = "";
			//			user.UserName = username;
			//			user.Password = EncodePassword(password);
			//			user.Email = email.ToLower();
			//			user.CreatedOn = DateTime.Now;
			//			user.ModifiedOn = DateTime.Now;

			//			_db.Users.InsertOnSubmit(user);

			//			_db.SubmitChanges();

			//			status = MembershipCreateStatus.Success;

			//			return GetUser(username, false);
			//		}

			//	} catch {
			//		status = MembershipCreateStatus.ProviderError;
			//	}
			//} else {
			//	status = MembershipCreateStatus.DuplicateUserName;
			//}
			status = MembershipCreateStatus.ProviderError;
			return null;
		}

		///// <summary>
		///// Delete user
		///// </summary>
		///// <param name="username"></param>
		///// <param name="deleteAllRelatedData"></param>
		///// <returns></returns>
		public override bool DeleteUser(string username, bool deleteAllRelatedData) {
			return false;
			//bool ret = false;

			//using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//	try {
			//		User user = (from u in _db.Users
			//						 where u.UserName == username && u.DeletedOn == null
			//						 select u).FirstOrDefault();

			//		if (user != null) {
			//			_db.Users.DeleteOnSubmit(user);

			//			_db.SubmitChanges();

			//			ret = true;
			//		}
			//	} catch {
			//		ret = false;
			//	}
			//}

			//return ret;
		}

		public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords) {
			throw new NotImplementedException();
		}

		public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords) {
			throw new NotImplementedException();
		}

		public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords) {
			throw new NotImplementedException();
		}

		public override int GetNumberOfUsersOnline() {
			throw new NotImplementedException();
		}

		public override string GetPassword(string username, string answer) {
			//using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//	try {
			//		var pass = (from p in _db.Users
			//						where p.UserName == username && p.DeletedOn == null
			//						select p.Password).FirstOrDefault();
			//		if (!string.IsNullOrWhiteSpace(pass))
			//			return UnEncodePassword(pass);
			//	} catch { }
			//}
			return null;
		}
		/*
		public API.Data.User GetUser(string username) {
			API.Data.User CustomMembershipUser = null;
			using (var ipa = new API.Data.DAL()) {
				//CustomMembershipUser = ipa.UserGetSingle(username);
			}
			

			return CustomMembershipUser;
		}*/

		/// <summary>
		/// Get MembershipUser.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="userIsOnline"></param>
		/// <returns></returns>
		public override MembershipUser GetUser(string username, bool userIsOnline) {
			MembershipUser membershipUser = null;
			//using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//	try {
			//		User user = (from u in _db.Users
			//						 where u.UserName == username && u.DeletedOn == null
			//						 select u)
			//						 .FirstOrDefault();

			//		if (user != null) {
			//			membershipUser = new MembershipUser(this.Name,
			//				 user.UserName,
			//				 null,
			//				 user.Email,
			//				 "",
			//				 "",
			//				 true,
			//				 false,
			//				 user.CreatedOn,
			//				 DateTime.Now,
			//				 DateTime.Now,
			//				 default(DateTime),
			//				 default(DateTime));
			//		}
			//	} catch {
			//	}
			//}

			return membershipUser;
		}

		public override MembershipUser GetUser(object providerUserKey, bool userIsOnline) {
			throw new NotImplementedException();
		}

		public override string GetUserNameByEmail(string email) {
			throw new NotImplementedException();
		}

		public override string ResetPassword(string username, string answer) {
			throw new NotImplementedException();
		}

		public override bool UnlockUser(string userName) {
			throw new NotImplementedException();
		}

		public override void UpdateUser(MembershipUser user) {
			//using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//	try {
			//		User userToEdit = (from u in _db.Users
			//								 where u.UserName == user.UserName && u.DeletedOn == null
			//								 select u).FirstOrDefault();

			//		if (userToEdit != null) {

			//			// submit changes
			//			//_db.SubmitChanges();
			//		}
			//	} catch {
			//	}
			//}
		}

		public void UpdateCustomUser(API.Data.User user) {
			//using (CustomDataDataContext _db = new CustomDataDataContext()) {
			//	try {
			//		User userToEdit = (from u in _db.Users
			//								 where u.UserName == user.UserName && u.DeletedOn == null
			//								 select u).FirstOrDefault();

			//		if (userToEdit != null) {
			//			userToEdit.Name = user.Name;
			//			userToEdit.Email = user.Email;
			//			userToEdit.CompanyFK = user.CompanyFK;


			//			// submit changes
			//			_db.SubmitChanges();
			//		}
			//	} catch {
			//	}
			//}
		}

		/// <summary>
		/// Validate user.
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public override bool ValidateUser(string username, string password) {
			// todo: uncomment everything below

			using (var repo = new API.Data.DAL()) {
				//string phrase = @"Email.ToLower() == @0";
				
				//List<object> args = new List<object>();
				//args.Add(username);

				//// todo: needed. Yes!
				//var records = repo.UserSearch(phrase, args.ToArray()).ToList();
				
				bool blnUserOk = false;
				//foreach (var user in records) {
				//	string pwd = user.Password;
				//	string decPwd = (new Simple()).DecryptString(user.Password);
				//	if (decPwd == password) {
				//		blnUserOk = true;
				//		break;
				//	}
				//}
				return blnUserOk;

			}
		}

		#endregion

		#region Utility Methods

		/// <summary>
		/// Check the password format based upon the MembershipPasswordFormat.
		/// </summary>
		/// <param name="password">Password</param>
		/// <param name="dbpassword"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		private bool CheckPassword(string password, string dbpassword) {
			string pass1 = password;
			string pass2 = dbpassword;

			switch (PasswordFormat) {
				case MembershipPasswordFormat.Encrypted:
					pass2 = UnEncodePassword(dbpassword);
					break;
				case MembershipPasswordFormat.Hashed:
					pass1 = EncodePassword(password);
					break;
				default:
					break;
			}

			if (pass1 == pass2) {
				return true;
			}

			return false;
		}

		/// <summary>
		/// UnEncode password.
		/// </summary>
		/// <param name="encodedPassword">Password.</param>
		/// <returns>Unencoded password.</returns>
		private string UnEncodePassword(string encodedPassword) {
			string password = encodedPassword;

			switch (PasswordFormat) {
				case MembershipPasswordFormat.Clear:
					break;
				case MembershipPasswordFormat.Encrypted:
					password =
					  Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
					break;
				case MembershipPasswordFormat.Hashed:
					//HMACSHA1 hash = new HMACSHA1();
					//hash.Key = HexToByte(machineKey.ValidationKey);
					//password = Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));

					throw new ProviderException("Not implemented password format (HMACSHA1).");
				default:
					throw new ProviderException("Unsupported password format.");
			}

			return password;
		}

		/// <summary>
		/// Get config value.
		/// </summary>
		/// <param name="configValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		private string GetConfigValue(string configValue, string defaultValue) {
			if (String.IsNullOrEmpty(configValue)) {
				return defaultValue;
			}

			return configValue;
		}

		/// <summary>
		/// Encode password.
		/// </summary>
		/// <param name="password">Password.</param>
		/// <returns>Encoded password.</returns>
		private string EncodePassword(string password) {
			string encodedPassword = password;

			return (new dotUtils.Encryption.Simple()).EncryptString(password);

			//switch (PasswordFormat) {
			//	case MembershipPasswordFormat.Clear:
			//		throw new ProviderException("No chance in hell!");
			//		break;
			//	case MembershipPasswordFormat.Encrypted:
			//		break;
			//	case MembershipPasswordFormat.Hashed:
			//		HMACSHA1 hash = new HMACSHA1();
			//		hash.Key = HexToByte(machineKey.ValidationKey);
			//		encodedPassword =
			//		  Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
			//		break;
			//	default:
			//		throw new ProviderException("Unsupported password format.");
			//}

			//return encodedPassword;
		}

		/// <summary>
		/// Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration
		/// </summary>
		/// <param name="hexString"></param>
		/// <returns></returns>
		/// <remarks></remarks>
		private byte[] HexToByte(string hexString) {
			byte[] returnBytes = new byte[hexString.Length / 2];
			for (int i = 0; i < returnBytes.Length; i++)
				returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
			return returnBytes;
		}

		#endregion

		public override bool ConfirmAccount(string accountConfirmationToken) {
			throw new NotImplementedException();
		}

		public override bool ConfirmAccount(string userName, string accountConfirmationToken) {
			throw new NotImplementedException();
		}

		public override string CreateAccount(string userName, string password, bool requireConfirmationToken) {
			throw new NotImplementedException();
		}

		public override string CreateUserAndAccount(string userName, string password, bool requireConfirmation, IDictionary<string, object> values) {
			throw new NotImplementedException();
		}

		public override bool DeleteAccount(string userName) {
			throw new NotImplementedException();
		}

		public override string GeneratePasswordResetToken(string userName, int tokenExpirationInMinutesFromNow) {
			throw new NotImplementedException();
		}

		public override ICollection<OAuthAccountData> GetAccountsForUser(string userName) {
			throw new NotImplementedException();
		}

		public override DateTime GetCreateDate(string userName) {
			throw new NotImplementedException();
		}

		public override DateTime GetLastPasswordFailureDate(string userName) {
			throw new NotImplementedException();
		}

		public override DateTime GetPasswordChangedDate(string userName) {
			throw new NotImplementedException();
		}

		public override int GetPasswordFailuresSinceLastSuccess(string userName) {
			throw new NotImplementedException();
		}

		public override int GetUserIdFromPasswordResetToken(string token) {
			throw new NotImplementedException();
		}

		public override bool IsConfirmed(string userName) {
			throw new NotImplementedException();
		}

		public override bool ResetPasswordWithToken(string token, string newPassword) {
			throw new NotImplementedException();
		}
	}
}
