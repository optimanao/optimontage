﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using waAssembly.Services.interfaces;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using API;
using API.Data;

namespace waAssembly
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {

            App_Start.SimpleInjectorInitializer.Initialize();

            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            //BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            API.AutoMapperConfig.ConfigureMaps();
            (new dotnector.Bundling.CustomBundlesConfig()).Configure(System.Web.Hosting.HostingEnvironment.MapPath("~/assets/css"));

            API.statics.DictionaryReload();

            var config = dotless.Core.configuration.DotlessConfiguration.GetDefault();
            config.Logger = null;

            using (var dal = new DAL())
            {
                var upgradeEngine = DbUp.DeployChanges.To
                    .SqlDatabase(dal.ConnectionString)
                    .JournalToSqlTable("dbo", "mtaSchemaVersions")
                    .WithScriptsEmbeddedInAssemblies(new[]
                        {
                            System.Reflection.Assembly.GetExecutingAssembly()
                        },
                        (string s) => s.StartsWith("waAssembly.App_Start.dbup.")
                    )
                    .LogToConsole()
                    .Build();
                var q = upgradeEngine.GetScriptsToExecute();
                upgradeEngine.PerformUpgrade();
            }

            //var element = container.GetInstance<SessionManager>();
            //element.GetSession<string>(API.Data.enums.SessionKeys.CurrentUser);
        }

        public override void Init()
        {
            this.PostAuthenticateRequest += MvcApplication_PostAuthenticateRequest;
            base.Init();
        }

        void MvcApplication_PostAuthenticateRequest(object sender, EventArgs e)
        {
            {
                var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authCookie != null)
                {
                    string encTicket = authCookie.Value;
                    if (!String.IsNullOrEmpty(encTicket))
                    {
                        var ticket = FormsAuthentication.Decrypt(encTicket);
                        var id = new API.web.UserIdentity(ticket);
                        //var userRoles = Roles.GetRolesForUser(id.Name);
                        var prin = new GenericPrincipal(id, new string[] { });
                        HttpContext.Current.User = prin;
                    }
                }
            }
            // workstation cookie

            if (!string.IsNullOrEmpty(Request.QueryString["WorkstationID"]))
            {
                long workstationId = long.Parse(Request.QueryString["WorkstationID"]);
                if (workstationId < 0)
                {
                    // that's not good at all
                }

                var wsCookie = HttpContext.Current.Request.Cookies[statics.App.GetSetting(enums.SettingsKeys.WorkstationCookieName)];
                if (wsCookie == null)
                {
                    wsCookie = new HttpCookie(statics.App.GetSetting(enums.SettingsKeys.WorkstationCookieName));
                    wsCookie.Expires = DateTime.Now.AddYears(1);

                    string cookieDomain = statics.App.GetSetting(enums.SettingsKeys.AuthCookieDomain);
                    if (!cookieDomain.Contains("localhost"))
                    {
                        wsCookie.Domain = cookieDomain;
                    }
                }
                wsCookie.Value = workstationId.ToString();
                HttpContext.Current.Response.Cookies.Add(wsCookie);
            }

            // check for cookie? how about workstation validity

        }

        protected void Application_Error()
        {
            try
            {
                var ex = Server.GetLastError();
                if (ex.ToString().Contains("was not found or does not implement IController."))
                {
                    return;
                }
                statics.log.Fatal(ex);
            }
            catch (Exception)
            {

            }
        }

    }
}