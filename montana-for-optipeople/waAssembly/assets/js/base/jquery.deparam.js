﻿jQuery.deparam = function (params) {
    var o = {};
    if (!params) return o;
    var a = params.split('&');
    for (var i = 0; i < a.length; i++) {
        var pair = a[i].split('=');
        o[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1]);
    }
    return o;
}