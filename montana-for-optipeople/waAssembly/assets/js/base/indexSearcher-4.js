﻿/// (c) 2015 dotnector
/// version 4.0, just plain mcv partial view loader
/// version 4.1, cookie value restore cancel if "value" attribute of input has value
/// version 4.2, callbacks + persistSearchPhrase property

(function ($) {
	$.indexSearcher = function (element, options) {
		var defaults = {
			input: $(element),
			templateContainer: null,
			endpoint: '',
			ajaxParameter: 'Phrase',
			ajaxAdditionalData: {},
			recentSearchPhrase: '_jibberish',
			resultsElement: '#results',
			events: {
				onFilter: function (settings) { },
				onSelect: function (settings, selectedItem) { },
                onClear: function (settings) { },
                onDone: function (settings) { }
            },
            persistSearchPhrase: true // automatically save and restore search phrase with cookie
		};

		var plugin = this,
			ns = 'indexSearcher' // namespace
		;

		plugin.settings = {};
		plugin.methods = {
			search: function () {
				var postData = {}, ps = plugin.settings, val = ps.input.val();
				if (ps.recentSearchPhrase == val) {
					return;
				}

				// save search value to cookie
                if (ps.persistSearchPhrase) { 
                    clientside.cookie.setValue('indexSearch' + clientside.url.relative(), encodeURIComponent(val));
                }

				ps.recentSearchPhrase = val;

				postData[ps.ajaxParameter] = val;
				if (ps.ajaxAdditionalData != null) {
					postData = $.extend(postData, ps.ajaxAdditionalData);
				}
				console.log(postData);
				ps.input.addClass('input-loading');
                
                $(plugin.settings.resultsElement).load(ps.endpoint, postData, function () {
                    
					ps.input.removeClass('input-loading');
                    $(plugin.settings.resultsElement).closest('.pnl').removeClass('hidden').show();
                    plugin.methods.reassignClickUrls();
                    if (typeof ps.events.onDone === 'function') {
                        ps.events.onDone.call(ps);
                    }
				});
            },

			reassignClickUrls: function (targetCount) {
				var ps = plugin.settings;
				if ($(ps.templateContainer).children().length != targetCount) {
					setTimeout(function () {
						plugin.methods.reassignClickUrls.call(this, targetCount);
					}, 2);
				} else {
					var tc = $(ps.templateContainer);
					assignClickUrls(tc.parent().parent()); // re-assign clickurls
					tc.find('.inline-confirm').inlineConfirm();
				}
			}
		}

		plugin.init = function () {
			plugin.settings = $.extend({}, defaults, options);
			var ps = plugin.settings;
			//ps.input = $(element).find('.index-searcher-input');

            if (ps.persistSearchPhrase && ps.input.val() == '') { 
			    ps.input.val(
				    decodeURIComponent(
					    clientside.cookie.getValue('indexSearch' + clientside.url.relative())
				    )
			    );
            }

			setTimeout(function () {
				plugin.methods.search.call();
			}, 200);

			ps.input.on('keyup input', function () {
				$.doTimeout('IndexQuickFind', 500, function () {
					plugin.methods.search.call();
				});
			});

			ps.input.on('reload', function () {
				plugin.settings.recentSearchPhrase = "something-fake-and-fakey";
				plugin.methods.search.call();
			});

			var btnClear = ps.input.parent().parent().find('.index-search-clear');
			if (btnClear.length > 0) {
				btnClear.on('click', function () {
                    ps.input.val('').trigger('input');
                    setTimeout(function () { ps.input.focus(); }, 200);
				});
			}
		}

		plugin.init();
	}

	$.fn.indexSearcher = function (options) {
		return this.each(function () {
			if (undefined == $(this).data('indexSearcher')) {
				var plugin = new $.indexSearcher(this, options);
				$(this).data('indexSearcher', plugin);
				return plugin;
			}
		});
	}

})(jQuery);