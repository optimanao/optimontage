﻿//$(function () {
//	$(document).on('click', '.inline-confirm', function () {
//		var t = $(this);
//		// hide this, show next
//		t.show().slideUp('fast', function () { t.next().hide().removeClass('hidden').slideDown('fast'); });
//		// switch back in two seconds if not confirmed
//		setTimeout(function () { t.next().slideUp(function () { t.slideDown(); }); }, 2000);
//	});
//});

//$(function () {
//	$.fn.inlineConfirm = function () {
//		return this.each(function () {
//            var t = $(this);
//            t.on('click', function () {
//                var next = t.next();
//                next.hide().removeClass('hidden');

//                if (t.hasClass('inline-confirm-size')) {
//                    var w = t.outerWidth(), nw = next.outerWidth();
//                    if (w > nw) {
//                        next.css('width', w + 'px');
//                    }
//                }
//                t.show().slideUp('fast', function () { next.slideDown('fast'); });
//                setTimeout(function () { next.slideUp(function () { t.slideDown(); }); }, 2000);
//            });
//		});
//	};
//	$('.inline-confirm').inlineConfirm();
//});

// 2018-03-05: inline confirm like the above is not good with ajax loading and such
$(document).on('click', '.inline-confirm', function () {
    var t = $(this);
    var next = t.next();
    next.hide().removeClass('hidden');

    if (t.hasClass('inline-confirm-size')) {
        var w = t.outerWidth(), nw = next.outerWidth();
        if (w > nw) {
            next.css('width', w + 'px');
        }
    }
    t.show().slideUp('fast', function () { next.slideDown('fast'); });
    setTimeout(function () { next.slideUp(function () { t.slideDown(); }); }, 2000);
});