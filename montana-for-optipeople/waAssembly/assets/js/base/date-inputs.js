﻿function modernizeInputs() {
	
	if (!Modernizr.inputtypes.date) {
		var dateinputs = $('input[type=datetime],input[type=date],input[data-dateinput]');
		var timeinputs = $('input[type=time]');
		
		var fnc = function () {
			this.each(function (index, element) {
				var t = $(element);
				if (t.data('datetimeControlsInitialzed') == "1") {
					//console.log('input already has datepicker....skipping');
					return;
				}
				//console.log('init datepicker');
				t.data('datetimeControlsInitialzed', '1');
				t.datepicker({
					dateFormat: 'yy-mm-dd',
					firstDay: 1,
					showButtonPanel: true,
					nextText: '',
					prevText: ''
				});
			});
		};
		var fncTime = function () {
			this.inputmask("hh:mm");
		};
		if (dateinputs.length < 1 && timeinputs.length < 1) { // don't bother with jQuery UI if not required
			//return;
		} else {
			dateinputs.attr('autocomplete', 'off');
			timeinputs.attr('autocomplete', 'off');
		}

		if (dateinputs.length > 0) {
			include('/css/base/jquery.ui.datepicker.css');
			if (jQuery.ui) {
				fnc.call(dateinputs);
			} else {
				
				include('/js/base/jquery-ui-1.11.2.min.js', function () { fnc.call(dateinputs); });
			}
		}

		if (timeinputs.length > 0) {
			if (typeof inputmask != "undefined") {
				fncTime.call(timeinputs);
			} else {
				include('/js/base/jquery.inputmask.bundle.js', function () { fncTime.call(timeinputs); });
			}
		}
	} else {
		// make sure we're setting the input type on data-dateinputs when native datepicker available
		$('input[type=text][data-dateinput]').attr('type', 'date');
	}
}
modernizeInputs();
