﻿$(function () {
	$('[data-navtabspersist]').on('click', 'li', function () {
		var t = $(this),
			parent = t.parent(),
            persistKey = parent.attr('data-navtabspersist') + clientside.url.relativeWithoutId(),
			indexValue = parent.find('> .active').index();
        if (indexValue < 0) {
            indexValue = 0;
        }
        clientside.cookie.setValue("navtabs-" + persistKey, parent.find('> li').index(t));
        $(window).trigger('resize'); // to trigger eval of sticky buttons row
	}).each(function () {

		// load
		var t = $(this);
        var persistValue = clientside.cookie.getValue('navtabs-' + t.attr('data-navtabspersist') + clientside.url.relativeWithoutId());
		persistValue = parseInt(persistValue, 10);
		if (!isNaN(persistValue)) {
			var child = t.find('li')[persistValue];
			if (typeof child !== "undefined") {
				$(child).find('a').tab('show');

			}
		}
	});


});