﻿// copyright (c) 2014 dotnector
// version 1.2 2014-03-17

// 1.2: - add support for comma separated "include"-syntax:
//        include('/js/file1.js,/js/file2.js')
//		  - add clearing of scripts array upon load to allow calling the "includer()" function more than once
// 1.3  - add "includerDone" and evals
//		  - add clearing of onload_methods upon completing. Failing to do so would mess with the post-load single include feature

var includerDeferredScripts = new Array(),
	 onload_methods = [],
	 onload_counter = 0,
	 includerStartTime = null
includerDone = false;

var includerScript = function (paramUrl, paramCallback) {
	this.url = paramUrl;
	this.callback = paramCallback;
};

function includerDebug(v) {
	return;
	if ($('#includerDebug').length == 0) {
		var debug = $('<div id="includerDebug"></div>');
		$('body').append(debug);
		debug.attr('style', 'background-color:#ffffff; position:absolute;top:20px;left:200px;width:800px;height:600px;z-index:2000;opacity:0.5;filter:alpha(opacity=100);overflow:scroll;'); //q&d
	}
	$('#includerDebug').html($('#includerDebug').html() + "<br />" + v);
}

function includerGetScript(url, success, caller) {
	if (url.toLowerCase().indexOf('.css') == url.length - 4) {
		//css
		var script = document.createElement('link');
		script.setAttribute('rel', 'stylesheet');
		script.setAttribute('href', url);
		var head = document.getElementsByTagName('head')[0],
					done = false;
		head.appendChild(script);
		caller.apply();
	} else {
		//js
		onload_counter += 1;
		var script = document.createElement('script');
		script.src = url;
		var head = document.getElementsByTagName('head')[0],
					done = false;
		includerDebug('adding script ' + url);
		script.onload = script.onreadystatechange = function () {
			if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {
				done = true;
				includerDebug('script "' + script.src + '" completed');
				if (typeof (success) != "undefined") {
					includerDebug('executing success function of script: ' + url);
					success.apply();
				}
				script.onload = script.onreadystatechange = null;
				head.removeChild(script);
				onload_counter -= 1;
				caller.apply();
			}
		};
		head.appendChild(script);
	}
}

// alias
function include(url, success) {
	if (url.indexOf(',')) {
		var scripts = url.split(',');
		for (var i = 0, len = scripts.length; i < len; i++) {
			if (i == 0) { // only include success callback on last one (push = reverse order!)
				includerDeferredScripts.push(new includerScript(scripts[i], success));
			} else {
				includerDeferredScripts.push(new includerScript(scripts[i]));
			}
		}
	} else {
		includerDeferredScripts.push(new includerScript(url, success));
	}

	if (includerDone) {
		includer();
	}
}

function includer(callback) {
	includerStartTime = new Date();

	// to preserve script load order have the method shift and call itself
	var load_a_script = function () {
		if (includerDeferredScripts.length <= 0)
			return;

		var script = includerDeferredScripts.shift();
		includerGetScript(script.url, script.callback, load_a_script);
	}

	load_a_script.apply();

	var onload_complete = function () {
		try {
			jquery_stripeTables();
		} catch (err) { }
		try {
			assignClickUrls();
		} catch (err) {
		}
		if (typeof (onload_methods) != "undefined") {
			for (var i = 0, len = onload_methods.length; i < len; i++) {
				includerDebug("<strong>applying onload method:</strong> " + onload_methods[i]);
				onload_methods[i].apply();
				if (i == (len - 1) && typeof (callback) == "function") {
					callback.apply();
				}
			}
			onload_methods = [];
		}
		includerDone = true;
	};

	var allGood = function () {
		var timeSpent = ((new Date()) - includerStartTime);
		includerDebug('are we good to go loop?: scripts remaining: ' + onload_counter + ". timeSpent: " + timeSpent);
		includerDebug("time spent: " + timeSpent);
		if (onload_counter != 0 && timeSpent <= 2500) {
			setTimeout(function () { allGood(); }, 200);
		} else {
			onload_complete();
		}
	};

	setTimeout(function () {
		includerDebug('starting loop: ' + onload_counter);
		allGood();
	}, 100);

	includerDebug('are we good to go?: scripts remaining: ' + onload_counter);

} // includer()
