﻿
var dictionary = (function () {
	//Private variables
	var module = {};
	module.settings = {
		locales: null,
		activeLocales: [],
		sectionID: 0,
		searchPhrase: '',
		searchAllSections: false
	}
	module.getSearchData = function () {
		return {
			Phrase: module.settings.searchPhrase,
			SectionID: module.settings.sectionID || 0,
			SearchAllSections: module.settings.searchAllSections
		}
	}
	module.koModel = function () {
		var self = this;
		self.LocaleVisible = function (data, locale) {
			return !$.inArray(locale, module.settings.activeLocales)
		}
		self.Records = ko.observableArray([]);
		self.ValueDisplay = function (data, locale) {
			var value = data.dic[locale];
			if(value.length > 300) {
				value = value.substr(0, 300) + "...";
			}
			return value;
		}
	}

	return module;
}());

var dictionary = (function (module) {
	module.init = function () {
		$('.create-new').each(function () {
			var t = $(this);
			t.attr('href', t.attr('data-defaulturl') + "&SectionID=0");
		});
		module.events();
		module.sectionKeyCounts();

		module.koModelInstance = new module.koModel();
		ko.applyBindings(module.koModelInstance);
		$('#dictionarySearch').val(module.clientside.cookie.getValue('phrase'));
		$('#dictionarySearch').val(module.clientside.cookie.getValue('phrase'));
		if (module.settings.sectionID == 0) {
			var cv = module.clientside.cookie.getValue('sectionID');
			if (cv == "") {
				cv = "0";
			}
			module.changeSection(cv);
		} else {
			module.changeSection(module.settings.sectionID); // onload
		}
		
		var locs = module.clientside.cookie.getValue('locales');
		if (locs != "") {
			locs = locs.split(',');
			for (var i = 0, len = locs.length; i < len; i++) {
				if (locs[i] != null && locs[i] != '') {
					module.settings.activeLocales.push(locs[i]);
				}
			}
		} else {
			module.settings.activeLocales = module.settings.locales;
		}
		for (var i = 0, len = module.settings.activeLocales.length; i < len; i++) {
			$('#LocaleSelection [data-locale="' + module.settings.activeLocales[i] + '"]').addClass('highlight');
		}
		
		module.settings.searchAllSections = (module.clientside.cookie.getValue('SearchAllSections') == "true");
		if (typeof module.settings.searchAllSections != "boolean") {
			module.settings.searchAllSections = false;
		}
		if (module.settings.searchAllSections && !$('#SearchAllSections').is(':checked')) {
			$('#SearchAllSections').prop('checked', true);
		}
		module.localeDisplay();
	}


	
	module.events = function () {
		$('#dictionarySections').on('click', 'a', function () {
			var section = $(this);
			module.changeSection(section.attr('data-sectionid'));
			//module.search();
		});

		$('#dictionarySearch').on('keyup input', function () {
			var input = $(this);
			$.doTimeout('dictionarySearch', 500, function () {
				if(module.clientside.charInputIgnore(input.val())) {
					return;
				}
				module.clientside.cookie.setValue('phrase', input.val());
				module.clientside.cookie.setValue('sectionID', module.settings.sectionID);
				module.settings.searchPhrase = input.val();
				module.search();
			});
		});

		$('#dictionarySearchClear').on('click', function () {
			$('#dictionarySearch').val('');
			module.search();
		});

		$('#SearchAllSections').on('click', function () {
			//var t = $(this);
			module.settings.searchAllSections = $(this).is(':checked');
			module.search();
			module.clientside.cookie.setValue('SearchAllSections', module.settings.searchAllSections);
		});

		// show/hide language/locale on click
		$('#LocaleSelection li a').on('click', function (event) {
			var t = $(this);
			t.toggleClass('highlight');
			if (t.hasClass('highlight')) {
				module.settings.activeLocales.push(t.data('locale'));
			} else {
				module.settings.activeLocales = jQuery.grep(module.settings.activeLocales, function (value) {
					return value != t.data('locale');
				});
			}
			event.stopPropagation();
			module.localeDisplay();
		});
	}

	module.sectionKeyCounts = function () {
		module.clientside.ajax('Dictionary/SectionKeyCount', {}, function (data) {
			if (data.Data == null || data.Data.length == 0) {
			} else {
				for (var i = 0, len = data.Data.length; i < len; i++) {
					console.log(data.Data[i]['Count']);
					$('#dictionarySections a[data-sectionid=' + data.Data[i]['ID'] + ']').next().html('(' + data.Data[i]['Count'] + ')');
				}
			}
		});
	}
	// methods

	module.localeDisplay = function() {
		$('td[data-locale],th[data-locale]').hide();
		var cookieValue = "";
		for (var i = 0, len = module.settings.activeLocales.length; i < len; i++) {
			$('td[data-locale="' + module.settings.activeLocales[i] + '"]').show();
			$('th[data-locale="' + module.settings.activeLocales[i] + '"]').show();
			cookieValue += module.settings.activeLocales[i] + ",";
		}

		module.clientside.cookie.setValue('locales', cookieValue);
	}

	module.changeSection = function (sectionid) {
		$('.create-new').each(function () {
			var t = $(this);
			t.attr('href', t.attr('data-defaulturl') + "&SectionID=" + sectionid);
			module.settings.sectionID = sectionid;
		});
		$('#dictionarySections *[data-sectionid]').removeClass('active');
		$('#dictionarySections [data-sectionid=' + sectionid + ']').addClass('active');
		$('#dictionarySearch').trigger('input');
	}

	module.search = function () {
		var input = $('#dictionarySearch');
		input.addClass('dictionary-loading');
		module.clientside.ajax('Dictionary/Search', module.getSearchData(), function (data) {
			module.koModelInstance.Records(data.Data);
			input.removeClass('dictionary-loading');
			if (data.Data == null || data.Data.length == 0) {
				input.closest('.form-group').addClass('has-error');
				setTimeout(function () {
					input.closest('.form-group').removeClass('has-error');
				}, 1000);
			} else {
				setTimeout(function () { module.localeDisplay();}, 200);
			}
		});
	}

	return module;

}(dictionary));





var dictionary = (function (module) {

	module.clientside = {
		ajaxBaseUrl: '/',
		ajax: function (url, data, onSuccess, options) {
			var d = data;
			options = (typeof options == "undefined" ? {} : options);

			if (typeof options.addAaft != "undefined" && options.addAaft) {
				d = clientside.aaft(d);
			}
			if (typeof options.stringify == "undefined" || typeof options.stringify != "undefined" && options.stringify) {
				d = JSON.stringify(d);
			}
			var ajaxUrl = url;
			if (typeof options.baseUrl != "undefined" && options.baseUrl != "")
				ajaxUrl = options.baseUrl + ajaxUrl;
			else
				ajaxUrl = clientside.ajaxBaseUrl + ajaxUrl;

			var ajaxOptions =
				{
					url: ajaxUrl,
					data: d, //data: clientside.aaft(data),
					type: (typeof options.type != "undefined" ? options.type : 'POST'),
					contentType: (typeof options.contentType != "undefined" ? options.contentType : 'application/json; charset=utf-8'), //application/x-www-form-urlencoded; charset=utf-8 // latter = jquery default
					dataType: (typeof options.dataType != "undefined" ? options.dataType : 'json'),
					traditional: (typeof options.traditional != "undefined" ? options.traditional : true),
					success: function (data) {
						onSuccess.call(this, data);
					},
					error: function () {
						clientside.thumbsDown();
						if (typeof options.onError != "undefined") {
							options.onError.call(this, data);
						}
					}
				};

			$.ajax(ajaxOptions);
		},
		cookie: {
			setValue: function (key, value) {
				if (typeof ($.cookie) == "undefined") {
					alert("jquery.cookie.js missing");
					return;
				}
				var o = this.getObject();
				if (o == null) {
					o = {};
				}
				o[key] = value;
				$.cookie('dictionary', $.param(o), { path: "/" });
			},
			getValue: function (key) {
				if (typeof ($.cookie) == "undefined") {
					alert("jquery.cookie.js missing");
					return;
				}
				var o = this.getObject();
				if (o == null)
					return "";
				return (typeof (o[key]) == "undefined" ? "" : o[key].replace(/\+/g, ' '));
			},
			hasValue: function (key) {
				if (typeof ($.cookie) == "undefined") {
					alert("jquery.cookie.js missing");
					return;
				}
				var o = this.getObject();
				if (o == null)
					return false;
				return (typeof (o[key]) == "undefined" ? false : true);
			},
			getObject: function () {
				var v = $.cookie('dictionary');
				if (v == null || ("" + v).length == 0)
					return null;

				return $.deparam(v);
			}
		},
		// return true/false indicating if pressed key is to be ignored
		// useful to disregard arrow keys and such in input fields related to ajax searches
		// returns true when key should be ignored
		charInputIgnore: function (event) {
			if (event == null)
				return false;

			var t = $(this),
				englishAlphabetAndWhiteSpace = /[A-Za-z0-9 ]/g,
				key = String.fromCharCode(event.which),
				ignoreThese = "16,17,20,35,36,37,39,45";

			// For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
			// keyCode == 8  is backspace
			// keyCode == 37 is left arrow
			// keyCode == 39 is right arrow
			// englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern

			if (ignoreThese.indexOf(event.keyCode + ",") > -1) { // == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
				return true;
			}
			return false;
		}

	};

	return module;
}(dictionary));
