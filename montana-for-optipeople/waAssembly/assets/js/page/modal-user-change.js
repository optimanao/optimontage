﻿// qnd. should be a module
function modalUserChangeSearch() {
    var modal = $('#modalUserChange'),
        container = modal.find('.modal-body .results'),
        searchInput = modal.find('.search-phrase');

    container.load('/user/searchUserChange', { Phrase: searchInput.val(), ReturnUrl: globals.currentUrl }, function (data) {
        modal.modal('show');
    });
}

// <user change modal>
$(document).on('click', '.nav-user-change', function () {
    modalUserChangeSearch();
});

$('#modalUserChange .search-phrase').on('keyup input', function () {
    $.doTimeout('modalUserChange-searchPhrase', '500', function () {
        modalUserChangeSearch();
    });
});
// </user change modal>


// <user options modal>
$(document).on('click', '.nav-user-options', function () {
    $('#modalUserOptions').modal('show');
});
// </user options modal>