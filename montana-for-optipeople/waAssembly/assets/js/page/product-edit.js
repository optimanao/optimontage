﻿//$('#inpChecklistPhrase').on('input keyup', function () {
//    $.doTimeout(400, 'searchChecklist', function () {

//    });
//});

var app = angular.module("waAssembly", [])
    .controller("editController", function ($scope) {
        var timeoutVariable;

        $scope.searchResultsChecklistItems = [];
        $scope.checklistItems = [];
        $scope.searchPhrase = "";

        $scope.init = function () {
            console.log('halløjsa');
            if (typeof undefined === typeof globals.checklistItems) {
                return;
            }
            $scope.checklistItems = globals.checklistItems;
            $scope.search.call();
        };

        $scope.search = function () {
            jQuery.doTimeout('checklistsearch', 750, function () {
                clientside.ajax(globals.angSearchUrl, { Phrase: $scope.searchPhrase }, function (data) {
                    if (data.Data.length === 0) {
                        clientside.thumbsDown();
                    } else {
                        clientside.thumbsUp();
                    }
                    $scope.$apply(function () {
                        $scope.searchResultsChecklistItems = data.Data;
                    });
                }, { baseUrl: '' });
            });
        }

        $scope.checklistAdd = function (element) {
            var arr = $scope.checklistItems;
            for (var i = 0, len = arr.length; i < len; i++) {
                if (arr[i].ID === element.ID) {
                    return;
                }
            }
            $scope.checklistItems.push(element);
        }

        $scope.checklistRemove = function (item) {
            var arr = $scope.checklistItems;
            for (var i = 0, len = arr.length; i < len; i++) {
                if (arr[i].ID === item.ID) {
                    arr.splice(i, 1);
                }
            }
        }

        $scope.checklistReorder = function (item, upOrDown) {
            var array = $scope.checklistItems,
                moveUp = upOrDown === 0,
                itemIndex = array.indexOf(item);

            array.splice(itemIndex, 1);
            array.splice(itemIndex - (moveUp ? 1 : -1), 0, item);
        }

        $scope.init();
    });

angular.module('editFeatures', ['productdata']);
