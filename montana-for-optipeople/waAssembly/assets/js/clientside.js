﻿var clientside = {
	/*
	private
	*/
    _cookiename: 'MONcookie',
    _user_agent: "",
    _is_iPad: false,
    _is_mobile_device: false,
    ajaxBaseUrl: '',
	/*
	stuff
	*/
    dateFormat: 'YYYY-MM-DD', // for momentjs
    currentUrl: "",
    enableDebug: true,
    device: {
        isTouch: function () {
            var msTouchEnabled = window.navigator.msMaxTouchPoints;
            var divs = $('div'), div = (divs.length == 0 ? document.createElement("div") : divs.first()[0]);
            var generalTouchEnabled = "ontouchstart" in div; //document.createElement("div");

            return (msTouchEnabled || generalTouchEnabled);
        },
        isIpad: function () {
            if (clientside._user_agent == "")
                clientside.isMobileDevice();
            return (clientside._user_agent.match(/iPad/i) != null);
        },
        isMobile: function () {
            if (clientside._user_agent == "") {
                clientside._user_agent = navigator.userAgent.toLowerCase(); //.match(/iPad/i) != null;
                clientside._is_mobile_device = (clientside._user_agent.match(/iPad/i) != null
                    || clientside._user_agent.match(/iPod/i) != null
                    || clientside._user_agent.match(/iPhone/i) != null
                    || clientside._user_agent.match(/android/i) != null
                    || clientside._user_agent.match(/mobile/i) != null
                );
            }
            return clientside._is_mobile_device;
        }
    },
    dictionary: null,
    dictionaryLanguage: null,
    messages: {
        confirmDelete: 'Delete?'
    },
	/* 
	general
	*/
    aaft: function (data) { //addAntiForgeryToken
        data.__RequestVerificationToken = $('#__aaft input[name=__RequestVerificationToken]').val();
        return data;
    },
    ajax: function (url, data, onSuccess, options) {
        var d = data;
        options = (typeof options == "undefined" ? {} : options);

        if (typeof options.addAaft != "undefined" && options.addAaft) {
            d = clientside.aaft(d);
        }
        if (typeof options.stringify == "undefined" || typeof options.stringify != "undefined" && options.stringify) {
            d = JSON.stringify(d);
        }
        var ajaxUrl = url;
        if (typeof options.baseUrl != "undefined" /*&& options.baseUrl != ""*/)
            ajaxUrl = options.baseUrl + ajaxUrl;
        else
            ajaxUrl = clientside.ajaxBaseUrl + ajaxUrl;

        var ajaxOptions =
            {
                url: ajaxUrl,
                data: d, //data: clientside.aaft(data),
                type: (typeof options.type != "undefined" ? options.type : 'POST'),
                contentType: (typeof options.contentType != "undefined" ? options.contentType : 'application/json; charset=utf-8'), //application/x-www-form-urlencoded; charset=utf-8 // latter = jquery default
                dataType: (typeof options.dataType != "undefined" ? options.dataType : 'json'),
                traditional: (typeof options.traditional != "undefined" ? options.traditional : true),
                success: function (data) {
                    onSuccess.call(this, data);
                },
                error: function () {
                    clientside.thumbsDown();
                    if (typeof options.onError != "undefined") {
                        options.onError.call(this, data);
                    }
                }
            };

        $.ajax(ajaxOptions);
    },
    cookie: {
        setValue: function (key, value) {
            if (typeof ($.cookie) == "undefined") {
                alert("jquery.cookie.js missing");
                return;
            }
            var o = this.getObject();
            if (o == null) {
                o = {};
            }
            o[key] = value;
            $.cookie(clientside._cookiename, $.param(o), { path: "/" });
        },
        getValue: function (key) {
            if (typeof ($.cookie) == "undefined") {
                alert("jquery.cookie.js missing");
                return;
            }
            var o = this.getObject();
            if (o == null)
                return "";
            return (typeof (o[key]) == "undefined" ? "" : o[key]);
        },
        hasValue: function (key) {
            if (typeof ($.cookie) == "undefined") {
                alert("jquery.cookie.js missing");
                return;
            }
            var o = this.getObject();
            if (o == null)
                return false;
            return (typeof (o[key]) == "undefined" ? false : true);
        },
        getObject: function () {
            var v = $.cookie(clientside._cookiename);
            if (v == null || ("" + v).length == 0)
                return null;

            return $.deparam(v);
        }
    },
    // return true/false indicating if pressed key is to be ignored
    // useful to disregard arrow keys and such in input fields related to ajax searches
    charInputIgnore: function (event) {
        if (event == null)
            return true;

        var t = $(this),
            englishAlphabetAndWhiteSpace = /[A-Za-z0-9 ]/g,
            key = String.fromCharCode(event.which),
            ignoreThese = "16,17,20,35,36,37,39,45";

        // For the keyCodes, look here: http://stackoverflow.com/a/3781360/114029
        // keyCode == 8  is backspace
        // keyCode == 37 is left arrow
        // keyCode == 39 is right arrow
        // englishAlphabetAndWhiteSpace.test(key) does the matching, that is, test the key just typed against the regex pattern

        if (ignoreThese.indexOf(event.keyCode + ",") > -1) { // == 8 || event.keyCode == 37 || event.keyCode == 39 || englishAlphabetAndWhiteSpace.test(key)) {
            return false;
        }
        return true;
    },
    describe: function (v, withvalues) {
        var q = "";
        for (var w in v) {
            q += w;
            if (typeof withvalues != "undefined" && withvalues)
                q += ": " + v[w];
            q += "\n";
        }
        return q;
    },
    debug: function (v) {
        if (!clientside.enableDebug)
            return;
        if ($('#clientsideDebuggin').length == 0) {
            var debug = $('<div id="clientsideDebuggin"></div>');
            $('body').append(debug);
            debug.attr('style', 'position:absolute;top:20px;left:500px;width:300px;z-index:200000;height:600px;opacity:.5;filter:alpha(opacity=50);overflow:scroll;'); //q&d
        }
        $('#clientsideDebuggin').html(v + '<br />' + $('#clientsideDebuggin').html());
    },
    dicValue: function (key) {
        var rs = $.grep(clientside.dictionary, function (e) { return e.Key === key; });
        if (rs.length === 1) {
            return rs[0].Value;
        } else { return ("Error occurred. You should never have seen this :-( "); }
    },
    elementDisable: function (domNode) {
        $(domNode).addClass('disabled').prop('disabled', true); // Disables visually + functionally
    },
    elementEnable: function (domNode) {
        $(domNode).removeClass('disabled').prop('disabled', false); // Disables visually + functionally
    },
    formPostAsAjax: function (form, callback, beforeSubmit) {
        var t = $(form),
            cb = (typeof callback === 'function' ? callback : null),
            before = (typeof beforeSubmit === 'function' ? beforeSubmit : null)
            ;

        if (before != null) {
            before.call(this);
        }
        //console.warn('ajax posting');

        var postData = t.serializeObject();

        clientside.ajax(t.attr('action'), postData, function (data) {
            if (data.StatusText == "Ok") {
                clientside.thumbsUp();
            } else {
                clientside.thumbsDown(data.Message);
            }
            if (cb != null) {
                cb.call(this);
            }
        }, { stringify: true });
    },

    guid: function () {
        var S4 = function () {
            return Math.floor(
                Math.random() * 0x10000 /* 65536 */
            ).toString(16);
        };
        return (
            S4() + S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + "-" +
            S4() + S4() + S4()
        );
    },
    hideLoading: function () {
        $('#loadingIcon').hide();
        $('html').removeClass('loading');
    },
    htmlEscape: function (str) {
        return String(str)
            .replace(/&/g, '&amp;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, '&#39;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;');
    },
    htmlDescape: function (str) {
        return String(str)
            .replace(/&amp;/g, '&')
            .replace(/&quot;/g, '"')
            .replace(/&#39;/g, "'")
            .replace(/&lt;/g, '<')
            .replace(/&gt;/g, '>');
    },
    newlineToBr: function (str) {
        return (typeof str == "undefined" ? "" : str.replace(/(?:\r\n|\r|\n)/g, '<br />'));
    },
    padLeft: function (v, length) {
        var t = "" + v, pad = "0000000000000000000000".substring(0, length);
        return pad.substring(0, pad.length - t.length) + t;
    },
    popDialog: function (url, width, height) {
        if (typeof (width) == "undefined")
            width = Math.min($(window).width(), 1024);
        if (typeof (height) == "undefined")
            height = Math.min($(window).height(), 500);

        window.open(url, "win" + clientside.guid(), "directories=no,width=" + width + ",height=" + height + ",scrollbars=yes,resizable=no,status=yes,titlebar=yes,toolbar=no");
    },
    rqValue: function (queryStringVariableName, ignoreCase) {
        if (typeof (ignoreCase) == "undefined")
            ignoreCase = false;

        // http://stackoverflow.com/questions/2907482/how-to-get-the-query-string-by-javascript
        var assoc = {};
        var decode = function (s) { return decodeURIComponent(s.replace(/\+/g, " ")); };
        var queryString = location.search.substring(1);
        var keyValues = queryString.split('&');
        for (var i in keyValues) {
            var key = keyValues[i].split('=');
            if (key.length > 1) {
                if (ignoreCase)
                    assoc[decode(key[0]).toLowerCase()] = decode(key[1]);
                else
                    assoc[decode(key[0])] = decode(key[1]);
            }
        }

        if (ignoreCase)
            return assoc[queryStringVariableName.toLowerCase()] || "";
        else
            return assoc[queryStringVariableName] || "";
    },
    scrollIntoView: function (o) {
        var obj = $(o);
        $('html, body').stop().animate({ scrollTop: 60 + parseInt(obj.css('top'), 10) - $(window).innerHeight() / 2 }, 'fast');
    },
    showLoading: function () {
        $('html').addClass('loading');
        //return;
        //var show = function () {
        //	var t = $('#loadingIcon');
        //	t.show().css({
        //		top: '50%',
        //		left: '50%',
        //		marginTop: (t.outerHeight() / -2) + ($(window).scrollTop()) + 'px',
        //		marginLeft: (t.outerWidth() / -2) + ($(window).scrollLeft()) + 'px'
        //	});
        //};

        //if ($('#loadingIcon').length < 1) {
        //	var loading = $('<div id="loadingIcon" style="position:absolute;top:0;width:98%;text-align:center;z-index:10001;display:block;">'
        //						+ '<div></div>'
        //						+ '</div>');
        //	$('body').append(loading);
        //	var opts = {
        //		lines: 13, // The number of lines to draw
        //		length: 0, // The length of each line
        //		width: 6, // The line thickness
        //		radius: 31, // The radius of the inner circle
        //		corners: 0.4, // Corner roundness (0..1)
        //		rotate: 0, // The rotation offset
        //		direction: 1, // 1: clockwise, -1: counterclockwise
        //		color: '#ffffff', // #rgb or #rrggbb or array of colors
        //		speed: 0.7, // Rounds per second
        //		trail: 23, // Afterglow percentage
        //		shadow: false, // Whether to render a shadow
        //		hwaccel: false, // Whether to use hardware acceleration
        //		className: 'spinner', // The CSS class to assign to the spinner
        //		zIndex: 2e9
        //	};

        //	var spinner = new Spinner(opts).spin(loading[0]);
        //}
        //show.call();
    },
    showLoadingBar: function () {
        $('#loadingSmallBar').fadeIn('300');
        setTimeout(function () { $('#loadingSmallBar').fadeOut('600'); }, 400);
    },
    currentFileName: function () { //http://befused.com/javascript/get-filename-url
        //this gets the full url
        var url = document.location.href;
        //this removes the anchor at the end, if there is one
        url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
        //this removes the query after the file name, if there is one
        url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
        //this removes everything before the last slash in the path
        url = url.substring(url.lastIndexOf("/") + 1, url.length);
        //return
        return url;
    },
    thumbs: function (boolValue) {
        try {
            if (boolValue) {
                clientside.thumbsUp();
            } else {
                clientside.thumbsDown();
            }
        } catch (err) {
            clientside.thumbsDown();
        }
    },
    thumbsUp: function () {
        clientside.osd('<i class="far fa-thumbs-up fa-4x"></i>');
    },
    thumbsDown: function (v) {
        var hasText = typeof v != "undefined" && v != null && ("" + v).length > 0;
        clientside.osd('<i class="far fa-thumbs-down fa-4x"></i>'
            + (hasText ? "<br />" + v : ""), { error: true, delay: (hasText ? 3000 : 1000) });
    },
    osd: function (msg, options) {
        var settings = { delay: 1000, error: false };
        jQuery.extend(settings, options);

        var o = $('#osd'), topvalue = ($(document).scrollTop() + 40) + 'px';
        if (settings.error) {
            o.addClass('error');
        } else {
            o.removeClass('error');
        }
        o.css('top', topvalue)
            .html(msg)
            .stop()
            .animate({ opacity: 0.8 }, 200);

        setTimeout(function () {
            o
                .animate({ opacity: 0 }, 200, function () { o.css('top', '-5em'); });
        }, settings.delay);
    },

    url: {
        fileName: function () {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes everything before the last slash in the path
            url = url.substring(url.lastIndexOf("/") + 1, url.length);
            //return
            return url;
        },
        relative: function () {
            //this gets the full url
            var url = document.location.href;
            //this removes the anchor at the end, if there is one
            url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));
            //this removes the query after the file name, if there is one
            url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));
            //this removes the hostname if any
            url = url.substring((url.indexOf("//") == -1) ? 0 : url.indexOf("//") + 2);
            //this removes everything before the first slash query after the file name, if there is one
            url = url.substring((url.indexOf("/") == -1) ? 0 : url.indexOf("/") + 1);
            return url;
        },
        fragments: function () {
            return clientside.url.relative().split("/");
        },
        relativeWithoutId: function () {
            // this omits any integer values trailing the relative url
            // so instead of "/products/edit/27" it returns "/products/edit/"

            var frags = clientside.url.fragments();
            if (frags.length == 0) {
                return clientside.url.relative();
            }
            var lastfrag = frags[frags.length - 1];
            if (parseInt(lastfrag, 10).toString() === lastfrag) {
                // it's a id
                frags.pop();
                return frags.join('/');
            } else {
                // not an id
                return clientside.url.relative();
            }

        }
    },
    renameProperty: function (obj, oldName, newName) {
        // Check for the old property name to avoid a ReferenceError in strict mode.
        if (obj.hasOwnProperty(oldName)) {
            obj[newName] = obj[oldName];
            delete obj[oldName];
        }
        return obj;
    }
};


$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};