﻿using API;
using API.Data;
using System;
using System.Web.Mvc;
using waAssembly.Services;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    public class SystemJobController : SiteController
    {
        private DAL ipa = null;
        private ValidationTestControllerService validationService;

        public SystemJobController(ValidationTestControllerService vService)
        {
            validationService = vService;
            ipa = new DAL();
        }

        public ActionResult RepopulateNormTime() {
            foreach (var order in ipa.OrderGetCollection()) {
                var navinfo = ipa.NavInfoGetSingle(order.Barcode);
                if (navinfo != null) {
                    order.NormTime = navinfo.NormTime;
                    ipa.OrderSave(order);
                }
            }
            return Content("OK");
        }
        
    }
}
