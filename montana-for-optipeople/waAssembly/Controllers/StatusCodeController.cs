﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class StatusCodeController : SiteController
    {

        private DAL ipa = null;
        public StatusCodeController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        private StatusCodeEditModel fixModel(StatusCodeEditModel model)
        {
            var entityType = typeof(StatusCode).Name;
            model.AvailableLanguages = statics.Cache.LanguagesAll();
            
            if (model.LocalizedValues == null || !model.LocalizedValues.Any())
            {
                model.LocalizedValues = ipa.LocalizedValueGetCollection(entityType, model.ID);
            }
            model.AvailableCodeTypes = ipa.StatusCodeTypeGetCollection();
            foreach (var lang in model.AvailableLanguages)
            {
                if (!model.LocalizedValues.Any(m => m.LanguageID == lang.ID))
                {
                    model.LocalizedValues.Add(new LocalizedValue()
                    {
                        ID = 0,
                        LanguageID = lang.ID,
                        SourceEntityID = model.ID,
                        SourceEntityName = entityType,
                        Text = model.Name
                    });
                }
            }
            return model;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.StatusCodeSearch(Phrase);
            return PartialView("_search", d);
        }

        public ActionResult Create()
        {
            var model = new StatusCodeEditModel();
            model.AvailableCodeTypes = ipa.StatusCodeTypeGetCollection();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(StatusCodeEditModel model)
        {
          
            if (!ModelState.IsValid) {
                ThumbsDown("");
                model.AvailableCodeTypes = ipa.StatusCodeTypeGetCollection();
                return View(model);
            }

            var code = new StatusCode();
            statics.Mapper.Map(model, code);
            code = ipa.StatusCodeSave(code);

            ThumbsUp();
            return ReturnToView("Index", code.ID);
        }

        public ActionResult Edit(long ID)
        {
            var entity = ipa.StatusCodeGetSingle(ID);
            var model = statics.Mapper.Map<StatusCodeEditModel>(entity);
            model = fixModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(StatusCodeEditModel model)
        {
            var entity = ipa.StatusCodeGetSingle(model.ID);
            
            if (!ModelState.IsValid)
            {
                model = fixModel(model);
                ThumbsDown();
                return View(model);
            }

            statics.Mapper.Map(model, entity);
            ipa.StatusCodeSave(entity);

            foreach (var localizedValue in model.LocalizedValues)
            {
                ipa.LocalizedValueSave(localizedValue);
            }


            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Delete(long ID)
        {
            var entity = ipa.StatusCodeGetSingle(ID);
            entity.DeletedOnUtc = DateTime.UtcNow;
            ipa.StatusCodeSave(entity);
            ThumbsUp();
            return RedirectToAction("Index");
        }

    }
}
