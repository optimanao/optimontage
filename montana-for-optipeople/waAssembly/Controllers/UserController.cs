﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class UserController : SiteController
    {

        private DAL ipa = null;
        public UserController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.UserSearch(Phrase);
            //if (!CurrentUser.HasRole("SBA"))
            //{
            //    d = d.Where(m => m.CompanyID == CurrentUser.CompanyID).ToList();
            //}
            return PartialView("_search", d);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SearchUserChange(string Phrase, string ReturnUrl = "")
        {
            var d = ipa.UserSearch(Phrase);
            ViewBag.ReturnUrl = ReturnUrl;
            TempData["ReturnUrl"] = ReturnUrl;
            return PartialView("_searchUserChange", d);
        }

        public ActionResult Create()
        {
            var model = new UserEditModel();
            //model.AvailableOutlets = ipa.OutletGetCollection(CurrentUser.CompanyID);
            //model.AvailableCompanies = ipa.CompanyGetCollection();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(UserEditModel model)
        {
            //if (string.IsNullOrEmpty(model.Email) || !dotUtils.RegExp.IsValidEmail(model.Email))
            //{
            //    ThumbsDown("Enter valid email");
            //    return View(model);

            //}
            //var existing = ipa.UserGetSingle(model.Email.Trim());
            //if (existing != null)
            //{
            //    ThumbsDown("Email already in use by User ID " + existing.ID);
            //    return View(model);
            //}

            if (!ModelState.IsValid) {
                ThumbsDown("");
                return View(model);
            }
            var user = new User();
            statics.Mapper.Map(model, user);
            user = ipa.UserSave(user);

            ThumbsUp();
            return ReturnToView("Index", user.ID);
        }

        public ActionResult Edit(long ID)
        {
            ViewBag.Languages = ipa.LanguageGetCollection().Where(m => m.Active).ToList();
            var user = ipa.UserGetSingle(ID);
            var model = statics.Mapper.Map<UserEditModel>(user);
            //model.CompanyLinks = ipa.CompanyUserTypeLinkGetCollectionByUser(ID);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserEditModel model)
        {
            ViewBag.Languages = ipa.LanguageGetCollection().Where(m => m.Active).ToList();
            var user = ipa.UserGetSingle(model.ID);
            
            if (!ModelState.IsValid)
            {
                ThumbsDown();
                return View(model);
            }

            statics.Mapper.Map(model, user);
            user.NameFull = user.FullName;
            ipa.UserSave(user);

            //ipa.UserClearRoles(user.ID);
            //foreach (var roleID in model.SelectedUserRoles)
            //{
            //    ipa.UserAddRole(user.ID, roleID);
            //}

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Delete(long ID)
        {
            var user = ipa.UserGetSingle(ID);
            user.DeletedOnUtc = DateTime.UtcNow;
            ipa.UserSave(user);
            ThumbsUp();
            return RedirectToAction("Index");
        }

        public ActionResult UpdatePassword(long ID) {
            var user = ipa.UserGetSingle(ID);
            if (user == null) {
                ThumbsDown("User not found");
                return RedirectToAction("Index");
            }

            return View(user);
        }

        [HttpPost]
        public ActionResult UpdatePassword(long ID, string Password, string PasswordRepeat) {
            if (string.IsNullOrEmpty(Password) || Password.Length < 6) {
                ThumbsDown("Passwords must be six (6) characters minimum");
                return RedirectToAction("UpdatePassword", new { ID = ID });
            }

            if (!Password.Equals(PasswordRepeat))
            {
                ThumbsDown("Passwords don't match");
                return RedirectToAction("UpdatePassword", new { ID = ID});
            }
            var user = ipa.UserGetSingle(ID);
            if (user == null)
            {
                ThumbsDown("User not found");
                return RedirectToAction("Index");
            }
            user.Password = Password;
            ipa.UserSave(user);

            ThumbsUp();
            return RedirectToAction("Edit", new { ID = ID });
        }

        
        //public ActionResult Impersonate(long ID)
        //{
        //    var user = ipa.UserGetSingle(ID);
        //    if (user != null && user.Roles.Contains("SBA") && !CurrentUser.HasRole("SBA"))
        //    {
        //        ThumbsDown();
        //        return RedirectToAction("Index", "User");
        //    }

        //    var accountControllerService = SimpleInjectorInitializer.container.GetInstance<AccountService>();
        //    accountControllerService.SetupFormsAuthTicket(ID, false, true, Response, CurrentUser.ID);
        //    ThumbsUp();
        //    return RedirectToAction("Index", "Home", new { area = "" });
        //}

    }
}
