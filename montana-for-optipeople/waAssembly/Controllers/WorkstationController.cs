﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class WorkstationController : SiteController
    {

        private DAL ipa = null;
        public WorkstationController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.WorkstationSearch(Phrase);
            return PartialView("_search", d);
        }

        public ActionResult Create()
        {
            var model = new WorkstationEditModel();
            //model.AvailableOutlets = ipa.OutletGetCollection(CurrentUser.CompanyID);
            //model.AvailableCompanies = ipa.CompanyGetCollection();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(WorkstationEditModel model)
        {

            if (!ModelState.IsValid) {
                ThumbsDown("");
                return View(model);
            }
            var entity = new Workstation();
            statics.Mapper.Map(model, entity);
            entity = ipa.WorkstationSave(entity);

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Edit(long ID)
        {
            var entity = ipa.WorkstationGetSingle(ID);
            var model = statics.Mapper.Map<WorkstationEditModel>(entity);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(WorkstationEditModel model)
        {
            var entity = ipa.WorkstationGetSingle(model.ID);
            
            if (!ModelState.IsValid)
            {
                ThumbsDown();
                return View(model);
            }

            statics.Mapper.Map(model, entity);
            ipa.WorkstationSave(entity);

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Delete(long ID)
        {
            var entity = ipa.WorkstationGetSingle(ID);
            entity.DeletedOnUtc = DateTime.UtcNow;
            ipa.WorkstationSave(entity);
            ThumbsUp();
            return RedirectToAction("Index");
        }

    }
}
