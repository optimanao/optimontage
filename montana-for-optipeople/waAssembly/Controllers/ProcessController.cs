﻿using API;
using API.Data;
using API.Data.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using waAssembly.Filters;
using waAssembly.Services;
using waAssembly.Services.Models;

namespace waAssembly.Controllers
{
    [dotAuthorize]
    [ResetViewBag]
    [WorkstationRequired]
    public class ProcessController : SiteController
    {
        //private DAL ipa = null;
        AccountService aService;

        public ProcessController(AccountService _accountservice)
        {
            //ipa = new DAL();
            aService = _accountservice;
        }

        /// <summary>
        /// View. Check items before completing
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Checklist(string Barcode)
        {
            var model = _getModel(Barcode);
            using (var ipa = new DAL())
            {
                model.CheckListItems = ipa.CheckListItemGetCollection(true);
            }
            return View(model);
        }

        /// <summary>
        /// View. Check items before completing
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Complete(string Barcode)
        {
            using (var ipa = new DAL())
            {
                // need to mark active time log as order completion
                var logs = ipa.TimeLogGetCollectionRunning(CurrentUser.ID);
                if (logs != null && logs.Any())
                {
                    var log = logs
                        .Where(m => m.Barcode == Barcode)
                        .OrderByDescending(m => m.StartTimeUtc.AddSeconds(m.Duration ?? 0))
                        .FirstOrDefault();
                    log.OrderCompleted = true;
                    ipa.TimeLogSave(log);
                }
                aService.StopRunningTimer(CurrentUser.ID);

                var newOrder = new Order()
                {
                    Barcode = Barcode,
                    IsCompleted = true,
                    CreatedByUserID = CurrentUser.ID,
                    CreatedOnUtc = DateTime.UtcNow,
                    WorkstationID = CurrentWorkstationID.Value
                };

                // <get norm time from nav>
                var navinfo = ipa.NavInfoGetSingle(Barcode);
                if (navinfo != null)
                {
                    newOrder.NormTime = navinfo.NormTime;
                }
                // </get norm time from nav>

                // <change log>
                ipa.OrderChangeLogSave(new OrderChangeLog()
                {
                    Barcode = Barcode,
                    CreatedByUserID = CurrentUser.ID,
                    CreatedOnUtc = DateTime.UtcNow,
                    IsCompleted = true,
                    WorkstationID = CurrentWorkstationID.Value
                });
                // </change log>

                ipa.OrderSave(newOrder);
            }
            ThumbsUp();
            return RedirectToAction("Index", "Order");
        }

        /// <summary>
        /// Response. Check items before discarding
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Discard(string Barcode)
        {
            aService.StopRunningTimer(CurrentUser.ID);
            using (var ipa = new DAL())
            {
                // <change log>
                ipa.OrderChangeLogSave(new OrderChangeLog()
                {
                    Barcode = Barcode,
                    CreatedByUserID = CurrentUser.ID,
                    CreatedOnUtc = DateTime.UtcNow,
                    IsDiscarded = true,
                    WorkstationID = CurrentWorkstationID.Value
                });
                // </change log>
            }
            ThumbsUp();
            return RedirectToAction("Index", "Order");
        }

        /// <summary>
        /// View. Error in work or product.Choose reason why.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Error(string Barcode)
        {
            if (!orderCanBeWorkedOn(Barcode))
            {
                ThumbsDown();
                return RedirectToAction("Index", "Order");
            }

            using (var ipa = new DAL())
            {
                var running = ipa.TimeLogGetRunning(CurrentUser.ID);
                if (running == null)
                {
                    ThumbsDown();
                    return RedirToErrorPage("Ingen igangværende ordre fundet");
                }
                var model = _getModel(running.Barcode);
                model.TimeLog = running;
                model.StatusCodesError = ipa.StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 1).ToList();

                return View(model);
            }
        }

        /// <summary>
        /// Helper. Get product picture from order.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <param name="no"></param>
        /// <returns></returns>
        public ActionResult OrderPicture(string Barcode, int no = 1)
        {
            using (var ipa = new DAL())
            {
                var order = ipa.NavPictureGetSingle(Barcode);
                if (order == null)
                {
                    return null;
                }

                var bytes = order.TryGetPropertyValue("Picture_" + no) as byte[];
                if (bytes != null && bytes is byte[] && bytes.Length > 0)
                {
                    return File(bytes, MimeMapping.GetMimeMapping("output.bmp"));
                }
                return File(statics.BlankPngBytes, "image/png", "noimagefound.png");
            }
        }

        /// <summary>
        /// View. Work paused. Choose reason why.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Pause(string Barcode)
        {
            if (!orderCanBeWorkedOn(Barcode))
            {
                ThumbsDown();
                return RedirectToAction("Index", "Order");
            }

            using (var ipa = new DAL())
            {
                var running = ipa.TimeLogGetRunning(CurrentUser.ID);
                if (running == null)
                {
                    ThumbsDown();
                    return RedirToErrorPage("Ingen igangværende ordre fundet");
                }
                var model = _getModel(running.Barcode);
                model.TimeLog = running;
                model.StatusCodesError = ipa.StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 2).ToList();
                return View(model);
            }
        }

        /// <summary>
        /// Response. Park order without statuscode and log out user
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult ClosingTime(string Barcode)
        {
            aService.StopRunningTimer(CurrentUser.ID);

            using (var ipa = new DAL())
            {
                ipa.OrderParkingSave(new OrderParking()
                {
                    Barcode = Barcode,
                    ModifiedByUserID = CurrentUser.ID,
                    ModifiedOnUtc = DateTime.UtcNow
                });
                aService.FormsAuthTicketDestroy(Request, Response);
            }
            ThumbsUp(DicValue(TKeys.StatusMessage.closingTimeLoggedOut));
            return RedirectToAction("Index", "Order");
        }

        public ActionResult RestartCompleted(string Barcode)
        {
            aService.StopRunningTimer(CurrentUser.ID);

            using (var ipa = new DAL())
            {
                //// remove any time logs marked as completed
                ipa.TimeLogClearOrderCompleted(Barcode);
                // remove the completed order thingy
                ipa.OrdreClearCompleted(Barcode);
            }
            return RedirectToAction("Start", new { Barcode });
        }

        public ActionResult Parts()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult partialPartsOrdered()
        {
            if (!CurrentWorkstationID.HasValue)
            {
                return Content("Fejl! Ingen arbejdsstation angivet.");
            }
            using (var ipa = new DAL())
            {
                var data = ipa.PartOrderGetCollection(CurrentWorkstationID ?? 0, CurrentUser.ID);

                // <housekeeping>
                foreach (var item in data.Where(M => M.IsDelivered && M.DeliveredOnUtc <= DateTime.UtcNow/*.AddDays(-2)*/).ToList())
                {
                    data.Remove(item);
                    ipa.PartOrderDeletePermanently(item.ID);
                }
                // </housekeeping>

                return PartialView("_partsOrdered", data);
            }
        }

        [HttpPost]
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult partialPartsSearch(string Phrase)
        {
            using (var ipa = new DAL())
            {
                var data = ipa.PartSearch(Phrase);
                return PartialView("_partsSearch", data);
            }
        }

        public ActionResult PartOrder(long PartID)
        {
            using (var ipa = new DAL())
            {
                var part = ipa.PartGetSingle(PartID);
                if (part == null || !CurrentWorkstationID.HasValue)
                {
                    return Json(new JsonResultData(JsonResultData.ResultStatus.Error) { });
                }

                var order = ipa.PartOrderUnprocessed(CurrentUser.ID, CurrentWorkstationID.Value, PartID);
                if (order != null)
                {
                    order.Quantity += 1;
                    ipa.PartOrderSave(order);
                    return Json(new JsonResultData() { });
                }

                var partorder = new PartOrder()
                {
                    CreatedByUserID = CurrentUser.ID,
                    CreatedOn = DateTime.Now,
                    CreatedOnUtc = DateTime.UtcNow,
                    PartID = PartID,
                    Quantity = 1,
                    WorkstationID = CurrentWorkstationID.Value
                };

                ipa.PartOrderSave(partorder);
                return Json(new JsonResultData() { });
            }
        }

        public JsonResult PartOrderQuantity(long OrderedPartID, bool Add)
        {
            using (var ipa = new DAL())
            {
                var partorder = ipa.PartOrderGetSingle(OrderedPartID);
                if (partorder != null)
                {
                    partorder.Quantity += (Add ? 1 : -1);

                    if (partorder.Quantity == 0)
                    {
                        ipa.PartOrderDeletePermanently(partorder.ID);
                    }
                    else
                    {
                        ipa.PartOrderSave(partorder);
                    }
                }
            }
            return Json(new JsonResultData() { });
        }

        public ActionResult PartOrderDelete(long ID)
        {
            using (var ipa = new DAL())
            {
                var part = ipa.PartOrderGetSingle(ID);
                if (part != null && part.CreatedByUserID == CurrentUser.ID && !part.IsDelivered && !part.IsPicked)
                {
                    ipa.PartOrderDeletePermanently(ID);
                }
                ThumbsUp();
                return RedirectToAction("Parts");
            }
        }




        private object GetPropertyValue(object obj, string propertyName)
        {
            try
            {
                foreach (var prop in propertyName.Split('.').Select(s => obj.GetType().GetProperty(s)))
                {
                    obj = prop.GetValue(obj, null);
                }
                return obj;
            }
            catch (NullReferenceException)
            {
                return null;
            }
        }


        public ActionResult PartsRunner(string SortBy = "", bool SortDirAsc = true)
        {
            using (var ipa = new DAL())
            {
                var items = ipa.PartOrderGetCollectionForRunner();

                if (!string.IsNullOrEmpty(SortBy))
                {
                    if (SortDirAsc)
                    {
                        items = items.OrderBy(x => GetPropertyValue(x, SortBy)).ToList();
                    }
                    else
                    {
                        items = items.OrderByDescending(x => GetPropertyValue(x, SortBy)).ToList();
                    }
                }

                return View(items);
            }
        }

        [HttpPost]
        public ActionResult PartsRunnerTogglePicked(long ID)
        {
            using (var ipa = new DAL())
            {
                var part = ipa.PartOrderGetSingle(ID);
                if (part == null)
                {
                    return Json(new JsonResultData(JsonResultData.ResultStatus.Error));
                }
                part.IsPicked = !part.IsPicked;
                ipa.PartOrderSave(part);
                return Json(new JsonResultData(JsonResultData.ResultStatus.Ok) { Message = (part.IsPicked ? "on" : "off") });
            }
        }

        [HttpPost]
        public ActionResult PartsRunnerSetDelivered(long ID)
        {
            using (var ipa = new DAL())
            {
                var part = ipa.PartOrderGetSingle(ID);
                if (part == null)
                {
                    return Json(new JsonResultData(JsonResultData.ResultStatus.Error));
                }
                part.IsDelivered = true;
                part.IsPicked = true;
                part.DeliveredOnUtc = DateTime.UtcNow;
                ipa.PartOrderSave(part);
                return Json(new JsonResultData(JsonResultData.ResultStatus.Ok) { });
            }
        }


        /// <summary>
        /// View. Working of active product.
        /// </summary>
        /// <returns></returns>
        public ActionResult Running()
        {
            using (var ipa = new DAL())
            {
                var running = ipa.TimeLogGetRunning(CurrentUser.ID);
                if (running == null)
                {
                    ThumbsDown();
                    return RedirToErrorPage("Ingen igangværende ordre fundet");
                }
                if (!orderCanBeWorkedOn(running.Barcode))
                {
                    aService.StopRunningTimer(CurrentUser.ID);
                    ThumbsDown();
                    return RedirectToAction("Index", "Order");
                }

                var model = _getModel(running.Barcode);
                model.TimeLog = running;
                //var model = new TimerRunningModel();
                //model.TimeLog = running;
                //model.NavInfo = ipa.NavInfoGetSingle(running.Barcode, true);
                //model.CheckListItems = ipa.CheckListItemGetCollection(model.NavInfo.Master, true);
                //model.StatusCodesError = ipa.StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 1).ToList();

                return View(model);
            }
        }

        /// <summary>
        /// Response. User has chosen reason in Pause or Error views.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <param name="StatusCodeID"></param>
        /// <param name="Park">Obsolete. Indicates order should be filed as parked = effectively the same as an error</param>
        /// <returns></returns>
        public ActionResult SetTimeLogStatus(string Barcode, long? StatusCodeID, bool Park = false)
        {
            aService.StopRunningTimer(CurrentUser.ID);
            var log = new TimeLog();
            if (!orderCanBeWorkedOn(Barcode))
            {
                ThumbsDown();
                return RedirectToAction("Index", "Order");
            }
            using (var ipa = new DAL())
            {
                StatusCode code = null;
                if (StatusCodeID.HasValue)
                {
                    code = ipa.StatusCodeGetSingle(StatusCodeID.Value);
                    if (code == null)
                    {
                        ThumbsDown("Statuskode ikke fundet");
                        return RedirectToAction("Start", new { Barcode = Barcode });
                    }
                }

                bool parkOrder = false;

                // if there is no status code, then we'll always park the order
                if (!StatusCodeID.HasValue || code == null)
                {
                    parkOrder = true;
                }
                else if (code != null && code.StatusCodeTypeID == 1)
                { // error code
                    parkOrder = true;
                }
                // else = pause

                if (parkOrder)
                {
                    // <change log - only when parking - not when doing something else>
                    ipa.OrderChangeLogSave(new OrderChangeLog()
                    {
                        Barcode = Barcode,
                        CreatedByUserID = CurrentUser.ID,
                        CreatedOnUtc = DateTime.UtcNow,
                        IsParked = true,
                        ParkedStatusCodeID = StatusCodeID,
                        WorkstationID = CurrentWorkstationID.Value
                    });
                    // </change log - only when parking - not when doing something else>

                    ipa.OrderParkingSave(new OrderParking()
                    {
                        Barcode = Barcode,
                        ModifiedByUserID = CurrentUser.ID,
                        ModifiedOnUtc = DateTime.UtcNow,
                        StatusCodeID = StatusCodeID
                    });
                    ThumbsUp(DicValue("StatusMessage.orderParked"));
                    return RedirectToAction("Index", "Order");
                    //return RedirToErrorPage(DicValue("StatusMessage.orderParked"));
                }
                else
                {
                    //
                    // start new timer if this is meeting or other type of pause
                    log.Barcode = Barcode;
                    log.Duration = 0;
                    log.InProgress = true;
                    log.StartTimeUtc = DateTime.UtcNow;
                    log.UserID = CurrentUser.ID;
                    log.StatusCodeID = StatusCodeID;
                    log.WorkstationID = CurrentWorkstationID.Value;
                    ipa.TimeLogSave(log);
                }
                ThumbsUp();
                return RedirectToAction("Standby", new { Barcode = Barcode });
            }
        }

        /// <summary>
        /// View. Returning from wither Pause or Error views.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Standby(string Barcode)
        {
            if (!orderCanBeWorkedOn(Barcode))
            {
                ThumbsDown();
                return RedirectToAction("Index", "Order");
            }
            var model = _getModel(Barcode);
            return View(model);
        }

        /// <summary>
        /// View. Most recent timer is more than two minutes old. User must explain why.
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="ReturnToBarcode"></param>
        /// <returns></returns>
        public ActionResult PauseReason(long ID, string ReturnToBarcode)
        {
            using (var ipa = new DAL())
            {
                var recentTime = ipa.TimeLogGetSingle(ID);
                if (recentTime == null || recentTime.UserID != CurrentUser.ID)
                {
                    throw new Exception("Critial error. No sense was made...");
                }
                var model = _getModel(recentTime.Barcode);
                model.TimeLog = recentTime;
                model.StatusCodesError = ipa.StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 2).ToList();
                ViewBag.ReturnToBarcode = ReturnToBarcode;
                return View(model);
            }
        }

        public ActionResult SetPauseReason(string Barcode, long StatusCodeID, string ReturnToBarcode)
        {
            using (var ipa = new DAL())
            {
                var recentTimeLog = ipa.TimeLogGetSingleMostRecent(CurrentUser.ID);
                if (recentTimeLog != null)
                {
                    var timelog = new TimeLog
                    {
                        Barcode = recentTimeLog.Barcode,
                        ID = 0,
                        InProgress = false,
                        StartTimeUtc = recentTimeLog.StartTimeUtc.AddSeconds(recentTimeLog.Duration ?? 0),
                        UserID = CurrentUser.ID,
                        WorkstationID = recentTimeLog.WorkstationID,
                        StatusCodeID = StatusCodeID
                    };
                    timelog.Duration = (int)Math.Floor(DateTime.UtcNow.Subtract(timelog.StartTimeUtc).TotalSeconds);
                    ipa.TimeLogSave(timelog);
                }
                ThumbsUp();
                return RedirectToAction("Start", new { Barcode = ReturnToBarcode });
            }
        }

        /// <summary>
        /// Response. Start timer.
        /// </summary>
        /// <param name="Barcode"></param>
        /// <returns></returns>
        public ActionResult Start(string Barcode)
        {
            aService.StopRunningTimer(CurrentUser.ID);
            if (!orderCanBeWorkedOn(Barcode))
            {
                ThumbsDown();
                return RedirectToAction("Index", "Order");
            }

            // 2019-02-28: we now allow tracking time on a previously discarded barcode.
            // So we need to delete it from the order table in case it gets discarded once more
            // Hrm...does this mean we simply shouldn't savean Order-entry for discarded orders? The change log logs it anyhow...
            // (pause) yes, it does. I've removed the Order-table logging for discarded products.

            using (var ipa = new DAL())
            {
                var order = ipa.OrderGetSingle(Barcode);
                if (order != null && order.IsDiscarded)
                {
                    ipa.OrderDeletePermanently(order.Barcode);
                }
            }

            using (var ipa = new DAL())
            {
                var recentTimeLog = ipa.TimeLogGetSingleMostRecent(CurrentUser.ID);
                if (recentTimeLog != null && recentTimeLog.OrderCompleted)
                {
                    var recentEndTime = recentTimeLog.StartTimeUtc.AddSeconds(recentTimeLog.Duration ?? 0);
                    if (recentEndTime.Date == DateTime.UtcNow.Date && recentEndTime.AddMinutes(2) < DateTime.UtcNow)
                    {
                        return RedirectToAction("PauseReason", new { ID = recentTimeLog.ID, ReturnToBarcode = Barcode });
                    }
                }

                var timer = new TimeLog()
                {
                    StartTimeUtc = DateTime.UtcNow,
                    InProgress = true,
                    UserID = CurrentUser.ID,
                    Barcode = Barcode,
                    WorkstationID = CurrentWorkstationID.Value
                };

                #region repair
                // <is this a repair time log>
                // "assumption": when order is parked, it's always an due to 'an error'. 
                //When timer start on 'an error', it's always a repair is status code exists
                var parking = ipa.OrderParkingGetSingle(Barcode);
                if (parking != null && parking.StatusCodeID.HasValue)
                {
                    timer.IsRepair = true;
                }

                // in real life...a repair can to started and stopped, then restarted. we should check if an order has been repair before
                if (!timer.IsRepair)
                {
                    var timers = ipa.TimeLogGetCollection(Barcode);
                    if (timers.Any(m => m.IsRepair))
                    {
                        timer.IsRepair = true;
                    }
                }
                // </is this a repair time log>
                #endregion

                ipa.TimeLogSave(timer);

                ipa.OrderParkingDeletePermanently(Barcode);
                return RedirectToAction("Running");
            }
        }

        /// <summary>
        /// Response. Stop running tmer.
        /// </summary>
        /// <returns></returns>
        public ActionResult Stop()
        {
            using (var ipa = new DAL())
            {
                var running = ipa.TimeLogGetRunning(CurrentUser.ID);
                if (running == null)
                {
                    ThumbsDown();
                    return RedirToErrorPage(DicValue("StatusMessage.noOrderInProgress"));
                }

                running.Duration = statics.Duration(running.StartTimeUtc);
                running.InProgress = false;
                ipa.TimeLogSave(running);
                ThumbsUp();

                if (string.IsNullOrEmpty(running.Barcode))
                {
                    ThumbsUp(DicValue("StatusMessage.timerStopped"));
                    return RedirectToAction("Index", "Order");
                }
                else
                {
                    return RedirectToAction("Standby", new { Barcode = running.Barcode });
                }
            }
        }

        /// <summary>
        /// Action/View. Validate any running timer. If running on order return to it. If running on "statuscode", return to "stop timer page"
        /// </summary>
        /// <returns></returns>
        public ActionResult TimerRunning()
        {
            using (var ipa = new DAL())
            {
                var timer = ipa.TimeLogGetRunning(CurrentUser.ID);
                if (timer == null)
                {
                    return RedirectToAction("Index", "Order");
                }
                if (!orderCanBeWorkedOn(timer.Barcode))
                {
                    aService.StopRunningTimer(CurrentUser.ID);
                    ThumbsDown();
                    return RedirectToAction("Index", "Order");
                }
                return View(timer);
            }
        }

        public ActionResult partialOrderHeader(string Barcode)
        {
            return PartialView("_header", _getModel(Barcode));
        }

        protected override void Dispose(bool disposing)
        {
            //if (ipa != null) 
            //    ipa.Dispose();
            base.Dispose(disposing);
        }

        private TimerRunningModel _getModel(string Barcode)
        {
            var model = new TimerRunningModel();
            using (var db = new DAL())
            {
                model.NavInfo = db.NavInfoGetSingle(Barcode, true);
                model.CheckListItems = db.CheckListItemGetCollection(model.NavInfo.Master, true);
                //model.NavInfo.Comments.ShippingComment = "halløjsa Torkild - din mor lugten af kommen og chokoladekrymmel. nomsi nomsi.";
                //model.NavInfo.Comments.SpecAtt = "halløjsa Torkild - din mor lugten af kommen og chokoladekrymmel. nomsi nomsi.";
                //model.StatusCodesError = ipa.StatusCodeGetCollection(true).Where(m => m.StatusCodeTypeID == 1).ToList();
            }
            return model;
        }

        /// <summary>
        /// returns false if order is either discarded or completed
        /// </summary>
        /// <param name="Barcode"></param>
        private bool orderCanBeWorkedOn(string Barcode)
        {
            using (var db = new DAL())
            {
                using (var ipa = new DAL())
                {
                    var order = ipa.OrderGetSingle(Barcode);
                    return (order == null) || (order != null && !order.IsCompleted);
                }
            }
        }

    }
}
