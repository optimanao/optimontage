﻿using API;
using API.Data;
using API.Data.Models;
using OfficeOpenXml;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class StatsController : SiteController
    {

        private DAL ipa = null;
        public StatsController()
        {
            ipa = new DAL();
        }

        private TimeLogSearchModel _getModel(TimeLogSearchModel model)
        {
            var thisModel = model;
            if (model == null)
            {
                thisModel = new TimeLogSearchModel();
            }
            thisModel.AvailableUsers = ipa.UserGetAll();
            thisModel.AvailableWorkstations = ipa.WorkstationGetCollection();
            return thisModel;
        }

        public ActionResult Index()
        {
            return View(_getModel(null));
        }

        [HttpPost]
        public ActionResult Index(TimeLogSearchModel model)
        {

            model = _getModel(model);

            model.Timelogs = ipa.TimeLogSearch(model);

            if (model.ExportToExcel)
            {
                using (ExcelPackage ep = new ExcelPackage())
                {
                    int cell = 1;
                    int row = 2;
                    var sheet = ep.Workbook.Worksheets.Add("Statistik");

                    var headings = new string[] {
                         DicValue(TKeys.start),
                                DicValue("end"),
                                DicValue("duration"),
                                DicValue("duration"),
                                DicValue(TKeys.user),
                                DicValue(TKeys.workstation),
                                DicValue(TKeys.Order.order),
                                DicValue(TKeys.statusCode)
                                //,DicValue(TKeys.statusCode) + " TypeID"
                    };

                    foreach (var heading in headings)
                    {
                        sheet.Cells[1, cell].Value = heading;
                        sheet.Cells[1, cell++].Style.Font.Bold = true;
                    }
                    cell = 1;

                    foreach (var line in model.Timelogs)
                    {
                        sheet.Cells[row, cell++].Value = line.StartTimeUtc.ToDefStringLocalTime(true);
                        sheet.Cells[row, cell++].Value = line.StartTimeUtc.AddSeconds(line.Duration ?? 0).ToDefStringLocalTime(true);
                        sheet.Cells[row, cell++].Value = TimeSpan.FromSeconds(line.Duration ?? 0).ToTimeString();
                        sheet.Cells[row, cell++].Value = line.Duration ?? 0;
                        sheet.Cells[row, cell++].Value = line.User?.NameFull;
                        sheet.Cells[row, cell++].Value = line.Workstation?.Name;
                        sheet.Cells[row, cell++].Value = line.Barcode;
                        sheet.Cells[row, cell++].Value = line.StatusCode?.LocalizedValues.FirstOrDefault(m => m.Language.Locale == "da-DK")?.Text;
                        //sheet.Cells[row, cell++].Value = line.StatusCode?.StatusCodeTypeID;

                        row++;
                        cell = 1;
                    }
                    sheet.Cells[sheet.Dimension.Address].AutoFitColumns();

                    var byteArray = new byte[0];
                    using (var ms = new System.IO.MemoryStream())
                    {
                        ep.SaveAs(ms);
                        ms.Position = 0;
                        byteArray = new byte[ms.Length];
                        byteArray = ms.ToArray();
                    }
                    return File(byteArray, MimeMapping.GetMimeMapping("report.xlsx"), "montage-statistik.xlsx");
                }
            }
            else
            {
                return View(model);
            }
        }


        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

    }
}
