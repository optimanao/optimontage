﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using waAssembly.Filters;
using waAssembly.Models;
using dotUtils;

namespace waAssembly.Controllers {
    #region sql
    /*
	 * 
	USE [databasename]
	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[psdDictionarySection](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Name] [nvarchar](50) NOT NULL,
		[ParentID] [int] NOT NULL,
	 CONSTRAINT [PK_psdDictionarySection] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY]

	SET ANSI_NULLS ON
	SET QUOTED_IDENTIFIER ON

	CREATE TABLE [dbo].[psdDictionaryEntry](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[Key] [nvarchar](50) NOT NULL,
		[Values] [ntext] NOT NULL,
		[IsRichText] [bit] NOT NULL,
		[DictionarySectionID] [int] NULL,
	 CONSTRAINT [PK_psdDictionaryEntry] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	GO

	ALTER TABLE [dbo].[psdDictionaryEntry]  WITH CHECK ADD  CONSTRAINT [FK_psdDictionaryEntry_psdDictionarySection] FOREIGN KEY([DictionarySectionID])
	REFERENCES [dbo].[psdDictionarySection] ([ID])
	GO

	ALTER TABLE [dbo].[psdDictionaryEntry] CHECK CONSTRAINT [FK_psdDictionaryEntry_psdDictionarySection]
	GO
	*/

    #endregion

    [dotAuthorize(OnlyAdmin = true)]
    public class DictionarySectionController : API.SiteController {

		private DAL ipa = null;

		
		public DictionarySectionController() {
			ipa = new DAL();
		}

		public ActionResult Create() {
			return View(new DictionarySection());
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Create(DictionarySection model) {
			using (var ipa = new DAL()) {
				model.Name = model.Name.ToUpperCase(0);
				ipa.DictionarySectionSave(model);
			}
			CacheHandler.RemoveItem("_dictionaryKeyCount"); // to force refresh
			return RedirectToAction("Index", "Dictionary");
		}

		public ActionResult Edit(int id = 0) {
			using (var ipa = new DAL()) {
				var data = ipa.DictionarySectionGetSingle(id);
				var model = new DictionarySection() { ID = id, Name = data.Name };
				return View("Edit", model);
			}
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(DictionarySection model) {
			using (var ipa = new DAL()) {
				if (ModelState.IsValid) {
					model.Name = model.Name.ToUpperCase(0);
					ipa.DictionarySectionSave(new DictionarySection() { ID = model.ID, Name = model.Name });
					CacheHandler.RemoveItem("_dictionaryKeyCount"); // to force refresh
					return RedirectToAction("Index", "Dictionary");
				} else {
					return View("Edit", model);
				}
			}
		}

		public ActionResult Delete(int id) {
			using (var ipa = new DAL()) {
				var entriesInSection = ipa.DictionaryEntryGetCollection(id);
				if (entriesInSection.Count() == 0) {
					ipa.DictionarySectionDeletePermanently(id);
					return RedirectToAction("Index", "Dictionary");
				} else {
					return View(ipa.DictionarySectionGetSingle(id));
				}
			}
		}

		public ActionResult DeleteConfirmed(int id, bool DeleteKeys) {
			using (var ipa = new DAL()) {

				var entries = ipa.DictionaryEntryGetCollection(id);
				if (DeleteKeys) {
					foreach (var entry in entries) {
						ipa.DictionaryEntryDeletePermanently(entry.ID);
					}
				} else {
					string doubleKeys = "";
					// test for possible key conflict in target section
					foreach (var entry in entries) {
						entry.DictionarySectionID = null;
						if (!ipa.DictionaryEntryOkToSave(entry)) {
							doubleKeys += entry.Key + ", ";
						}
					}

					if (String.IsNullOrEmpty(doubleKeys)) {
						// no conflict found. delete entries and section
						foreach (var entry in entries) {
							entry.DictionarySectionID = null;
							ipa.DictionaryEntrySave(entry);
						}
						ipa.DictionarySectionDeletePermanently(id);
					} else {
						// conflict found. show error.
						TempData[enums.TempDataKeys.MessageError.ToString()] = "Can't move entries to root! The following Keys would become doubles: " + doubleKeys;
						return RedirectToAction("Delete", new { id = id });
					}
				}
				
			}
			return RedirectToAction("Index", "Dictionary");
		}

		protected override void Dispose(bool disposing) {

			base.Dispose(disposing);
		}

	}
}