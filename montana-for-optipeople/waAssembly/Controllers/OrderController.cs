﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [ResetViewBag]
    [dotAuthorize]
    [WorkstationRequired]
    public class OrderController : SiteController
    {

        private DAL ipa = null;
        public OrderController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        // partial
        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.NavInfoSearch(Phrase);
            return PartialView("_search", d);
        }

        // partial
        public ActionResult SearchInProgress() {
            var d = ipa.OrderGetCollectionInProgress();
            return PartialView("_searchInProgress", d);
        }

        public ActionResult Parked()
        {
            //var orders = ipa.OrderParkingGetCollection().OrderBy(m => m.ModifiedByUserID != (CurrentUser?.ID ?? 0)).ToList();
            return View();
        }

        // partial
        [HttpPost]
        public ActionResult SearchParked(string Phrase)
        {
            var orders = ipa.OrderParkingGetCollection().OrderBy(m => m.ModifiedByUserID != (CurrentUser?.ID ?? 0)).ToList();
            if (!string.IsNullOrEmpty(Phrase)) {
                orders = orders.Where(m => m.Barcode.Contains(Phrase)).ToList();
            }
            return PartialView("_searchParked", orders);
        }


        public ActionResult Done()
        {
            return View();
        }

        // partial
        [HttpPost]
        public ActionResult SearchDone(string Phrase)
        {
            var orders = ipa.OrderSearchCompleted(Phrase);
            return PartialView("_searchDone", orders);
        }


        private List<OrderFlowModel> statsGetOrderFlowModels(string BarcodePrefix) {
            List<OrderFlowModel> collection = new List<OrderFlowModel>();
           
            var bars = ipa.NavInfoGetCollectionByOrderPrefix(BarcodePrefix);
            var dicWorkstations = new Dictionary<long, Workstation>();
            foreach (var bar in bars)
            {
                var order = ipa.OrderGetSingle(bar.Barcode);
                var ofm = new OrderFlowModel()
                {
                    Barcode = bar.Barcode
                };
                ofm.TimeLogs = ipa.TimeLogGetCollectionForStats(bar.Barcode).OrderByDescending(m => m.StartTimeUtc).ToList();
                ofm.MarkedCompleted = order?.IsCompleted ?? false;
                ofm.NavInfoDepth = bar.Depth;
                ofm.NavInfoColorSides = bar.Color_sides;

                collection.Add(ofm);
            }
            return collection;
        }

        public ActionResult StatsFlow(string BarcodePrefix) {
            if (!CurrentUser.IsAdmin) {
                return RedirToErrorPage("Not found");
            }

            if (string.IsNullOrEmpty(BarcodePrefix))
            {
                return View(new List<OrderFlowModel>());
            }

            var collection = statsGetOrderFlowModels(BarcodePrefix);
            return View(collection);
        }

        public ActionResult StatsChangeLog(string BarcodePrefix) {
            if (!CurrentUser.IsAdmin)
            {
                return RedirToErrorPage("Not found");
            }

            // load all modules
            // find all change logs
            // find all time logs
            // display

            if (string.IsNullOrEmpty(BarcodePrefix))
            {
                return View(new List<OrderFlowModel>());
            }

            var collection = statsGetOrderFlowModels(BarcodePrefix);
            var changelogs = ipa.OrderChangeLogGetCollection(BarcodePrefix);

            foreach (var item in collection) {
                item.ChangeLogs = changelogs.Where(m => m.Barcode == item.Barcode).ToList();
            }

            return View(collection);
        }

        //public ActionResult Create()
        //{
        //    var model = new UserEditModel();
        //    //model.AvailableOutlets = ipa.OutletGetCollection(CurrentUser.CompanyID);
        //    //model.AvailableCompanies = ipa.CompanyGetCollection();
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult Create(UserEditModel model)
        //{
        //    //if (string.IsNullOrEmpty(model.Email) || !dotUtils.RegExp.IsValidEmail(model.Email))
        //    //{
        //    //    ThumbsDown("Enter valid email");
        //    //    return View(model);

        //    //}
        //    //var existing = ipa.UserGetSingle(model.Email.Trim());
        //    //if (existing != null)
        //    //{
        //    //    ThumbsDown("Email already in use by User ID " + existing.ID);
        //    //    return View(model);
        //    //}

        //    if (!ModelState.IsValid) {
        //        ThumbsDown("");
        //        return View(model);
        //    }
        //    var user = new User();
        //    statics.Mapper.Map(model, user);
        //    user = ipa.UserSave(user);

        //    ThumbsUp();
        //    return ReturnToView("Index", model.ID);
        //}

        //public ActionResult Edit(long ID)
        //{
        //    ViewBag.Languages = ipa.LanguageGetCollection().Where(m => m.Active).ToList();
        //    var user = ipa.UserGetSingle(ID);
        //    var model = statics.Mapper.Map<UserEditModel>(user);
        //    //model.CompanyLinks = ipa.CompanyUserTypeLinkGetCollectionByUser(ID);
        //    return View(model);
        //}

        //[HttpPost]
        //public ActionResult Edit(UserEditModel model)
        //{
        //    ViewBag.Languages = ipa.LanguageGetCollection().Where(m => m.Active).ToList();
        //    var user = ipa.UserGetSingle(model.ID);

        //    if (!ModelState.IsValid)
        //    {
        //        ThumbsDown();
        //        return View(model);
        //    }

        //    statics.Mapper.Map(model, user);
        //    user.NameFull = user.FullName;
        //    ipa.UserSave(user);

        //    //ipa.UserClearRoles(user.ID);
        //    //foreach (var roleID in model.SelectedUserRoles)
        //    //{
        //    //    ipa.UserAddRole(user.ID, roleID);
        //    //}

        //    ThumbsUp();
        //    return ReturnToView("Index", model.ID);
        //}

        //public ActionResult Delete(long ID)
        //{
        //    var user = ipa.UserGetSingle(ID);
        //    user.DeletedOnUtc = DateTime.UtcNow;
        //    ipa.UserSave(user);
        //    ThumbsUp();
        //    return RedirectToAction("Index");
        //}


        //public ActionResult Impersonate(long ID)
        //{
        //    var user = ipa.UserGetSingle(ID);
        //    if (user != null && user.Roles.Contains("SBA") && !CurrentUser.HasRole("SBA"))
        //    {
        //        ThumbsDown();
        //        return RedirectToAction("Index", "User");
        //    }

        //    var accountControllerService = SimpleInjectorInitializer.container.GetInstance<AccountService>();
        //    accountControllerService.SetupFormsAuthTicket(ID, false, true, Response, CurrentUser.ID);
        //    ThumbsUp();
        //    return RedirectToAction("Index", "Home", new { area = "" });
        //}

    }
}
