﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using API;
using API.Data;
using waAssembly.Filters;
using waAssembly.Models;
using waAssembly.Services.Models;
using dotUtils;

namespace waAssembly.Controllers
{
    #region sql
    /*
	USE [databasename]

SET ANSI_NULLS ON

SET QUOTED_IDENTIFIER ON

CREATE TABLE [dbo].[psdDictionarySection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[ParentID] [int] NOT NULL,
 CONSTRAINT [PK_psdDictionarySection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]



CREATE TABLE [dbo].[psdDictionaryEntry](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Key] [nvarchar](50) NOT NULL,
	[Values] nvarchar(MAX) NOT NULL,
	[IsRichText] [bit] NOT NULL,
	[Notes] [nvarchar](200) NULL,
	[DictionarySectionID] [int] NULL,
	[RequiredMergeFields] [nvarchar](100) NULL,
 CONSTRAINT [PK_psdDictionaryEntry] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
ALTER TABLE [dbo].[psdDictionaryEntry]  WITH CHECK ADD  CONSTRAINT [FK_psdDictionaryEntry_psdDictionarySection] FOREIGN KEY([DictionarySectionID])
REFERENCES [dbo].[psdDictionarySection] ([ID])

ALTER TABLE [dbo].[psdDictionaryEntry] CHECK CONSTRAINT [FK_psdDictionaryEntry_psdDictionarySection]

	

CREATE TABLE [dbo].[psdLanguage](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[LocalName] [nvarchar](50) NOT NULL,
	[Locale] [nvarchar](10) NOT NULL,
	[LocaleTwoLetter] [nvarchar](2) NOT NULL,
	[Active] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

	
	 INSERT INTO vsLanguage (Name, LocalName, Locale, LocaleTwoLetter, Active) values('Danish', 'Dansk', 'da-DK', 'da', 1)
	 INSERT INTO vsLanguage (Name, LocalName, Locale, LocaleTwoLetter, Active) values('English', 'English', 'en-US', 'en', 1)
	 
*/

    /*
		validationErrorMessage
		Feltet '{0}' skal udfyldes
		The '{0}' field is required
	*/

    #endregion


    #region user-sql
    /*
    USE [database]
    GO

    SET ANSI_NULLS ON
    GO

    SET QUOTED_IDENTIFIER ON
    GO

    CREATE TABLE [dbo].[psdUser](
        [ID] [bigint] IDENTITY(1,1) NOT NULL,
        [NameFirst] [nvarchar](50) NOT NULL,
        [NameLast] [nvarchar](50) NOT NULL,
        [NameFull] [nvarchar](110) NULL,
        [Email] [nvarchar](200) NOT NULL,
        [Password] [nvarchar](250) NOT NULL,
        [ConfirmationToken] [nvarchar](100) NOT NULL,
        [Confirmed] [bit] NOT NULL,
        [CreatedOn] [datetime2](7) NOT NULL,
        [DeletedOn] [datetime2](7) NULL,
        [Locale] [nvarchar](10) NULL,
        
        [RecentLoginUtc] [datetime2](7) NULL,
     CONSTRAINT [PK_psdUser] PRIMARY KEY CLUSTERED 
    (
        [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    GO



        
    CREATE TABLE [dbo].[psdUserRole](
	    [ID] [int] IDENTITY(1,1) NOT NULL,
	    [Name] [nvarchar](20) NOT NULL,
     CONSTRAINT [PK_psdUserRole] PRIMARY KEY CLUSTERED 
    (
	    [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    GO


        
    CREATE TABLE [dbo].[psdUserInRole](
	    [ID] [int] IDENTITY(1,1) NOT NULL,
	    [UserID] [bigint] NOT NULL,
	    [UserRoleID] [int] NOT NULL,
     CONSTRAINT [PK_psdUserInRole] PRIMARY KEY CLUSTERED 
    (
	    [ID] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    GO

    ALTER TABLE [dbo].[psdUserInRole]  WITH CHECK ADD  CONSTRAINT [FK_psdUserInRole_psdUser] FOREIGN KEY([UserID])
    REFERENCES [dbo].[psdUser] ([ID])
    ON DELETE CASCADE
    GO

    ALTER TABLE [dbo].[psdUserInRole] CHECK CONSTRAINT [FK_psdUserInRole_psdUser]
    GO

    ALTER TABLE [dbo].[psdUserInRole]  WITH CHECK ADD  CONSTRAINT [FK_psdUserInRole_psdUserRole] FOREIGN KEY([UserRoleID])
    REFERENCES [dbo].[psdUserRole] ([ID])
    ON DELETE CASCADE
    GO

    ALTER TABLE [dbo].[psdUserInRole] CHECK CONSTRAINT [FK_psdUserInRole_psdUserRole]
    GO





    */
    #endregion

    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
	public class DictionaryController : API.SiteController {

		private object populateViewBag(object model = null) {
			using (var ipa = new DAL()) {
				var languages = ipa.LanguageGetCollection();
				ViewBag.Languages = languages;
				if (model != null) {
					var sections = ipa.DictionarySectionGetCollection();
					sections.Insert(0, new DictionarySection() { ID = 0, Name = "[root]" });
					model.TrySetPropertyValue(statics.GetMemberName((DictionaryEntryModel m) => m.AvailableLanguages), languages);
					model.TrySetPropertyValue(statics.GetMemberName((DictionaryEntryModel m) => m.Sections), sections);
				}
			}
			return model;
		}

		public ActionResult Index() {
			var model = new DictionaryModel();
			model = (DictionaryModel)populateViewBag(model);
			using (var ipa = new DAL()) {
				return View(model);
			}
		}

		[HttpPost]
		public ActionResult Search(string Phrase, int SectionID, bool SearchAllSections) {
			//System.Threading.Thread.Sleep(500);
			using (var ipa = new DAL()) {
				return Json(new JsonResultData() { Data = ipa.DictionaryEntrySearch(Phrase, SectionID, SearchAllSections) });
			}
		}

		public ActionResult Create() {
			DictionaryEntryModel model = new DictionaryEntryModel();
			model = (DictionaryEntryModel)populateViewBag(model);
			model.Entry = new DictionaryEntry();
			int sectionid = 0;
			if (Request.QueryString["SectionID"] != null && int.TryParse(Request.QueryString["SectionID"], out sectionid)) {
				model.Entry.DictionarySectionID = sectionid;
			}
			model.Entry.IsRichText = ((Request.QueryString["Type"] ?? "") == "Richtext");
			return View(model);
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Create(DictionaryEntryModel model) {
			if (ModelState.IsValid) {
				model = (DictionaryEntryModel)populateViewBag(model);
				using (var ipa = new DAL()) {
					model.Entry.Key = model.Entry.Key.ToLowerCase(0);
					
					foreach (Language lang in ViewBag.Languages)
						model.Entry.SetValue(lang.Locale, Request["language" + lang.ID] ?? "");

					TryValidateModel(model);

					if (!ModelState.IsValid) {
						return View("Edit", model);
					}

					//var testEntry = ipa.DictionaryEntryGetSingle(dictionaryentry.Entry.Key.Trim(), dictionaryentry.DictionarySectionID);
					//if (testEntry != null) {
					//	ViewBag.Error = "Key already exists!";
					//	return View(dictionaryentry);
					//}
					

					model.Entry.Key = model.Entry.Key.Trim();

					ipa.DictionaryEntrySave(model.Entry);
				}
				DictionaryReload(true);

				return RedirectToAction("Index", "Dictionary", new { SectionID = model.Entry.DictionarySectionID });
			}
			model = (DictionaryEntryModel)populateViewBag(model);
			return View(model);
		}

		public ActionResult Edit(int id = 0) {
			DictionaryEntryModel model = new DictionaryEntryModel();
			model = (DictionaryEntryModel)populateViewBag(model);
			using (var ipa = new DAL()) {
				model.Entry = ipa.DictionaryEntryGetSingle(id);
			}
			return View("Edit", model);
		}

		[HttpPost]
		[ValidateInput(false)]
		public ActionResult Edit(DictionaryEntryModel model) {
			model = (DictionaryEntryModel)populateViewBag(model);
			using (var ipa = new DAL()) {
				model.Entry.Key = model.Entry.Key.ToLowerCase(0);
				
				foreach (Language lang in ViewBag.Languages)
					model.Entry.SetValue(lang.Locale, Request["language" + lang.ID] ?? "");
				
				if (!ipa.DictionaryEntryOkToSave(model.Entry)) {
					TempData[enums.TempDataKeys.MessageError.ToString()] = "Nope! Key already exists in section";
					return View(model);
				}
				
				TryValidateModel(model);
				
				if (!ModelState.IsValid) {
					return View("Edit", model);
				}

				model.Entry.Key = model.Entry.Key.Trim();
				ipa.DictionaryEntrySave(model.Entry);

				DictionaryReload(true);

				if (!String.IsNullOrEmpty(Url.ReturnUrl())) {
					return Redirect(Url.ReturnUrl());
				} else {
					return RedirectToAction("Index", new { SectionID = model.Entry.DictionarySectionID });
				}
				//bool blnReturnToEdit = false;
				//if (Request["saveAction"] != null)
				//	blnReturnToEdit = (Request["saveAction"] == "Save");
				
				//if (blnReturnToEdit) {
				//	var data = ipa.DictionaryEntryGetSingle(model.Entry.ID);
				//	return View("Edit", data);
				//} else {
				//	return RedirectToAction("Index", new { SectionID = model.Entry.DictionarySectionID });
				//}
				
			}
		}

		//public ActionResult Delete(int id = 0) {
		//	using (var ipa = new DAL()) {
		//		DictionaryEntry dictionaryentry = ipa.DictionaryEntryGetSingle(id);
		//		if (dictionaryentry == null) {
		//			return HttpNotFound();
		//		}
		//		return View(dictionaryentry);
		//	}
		//}

		public ActionResult DeleteConfirmed(int id) {
			using (var ipa = new DAL()) {
				ipa.DictionaryEntryDeletePermanently(id);
			}
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing) {

			base.Dispose(disposing);
		}

		public ActionResult ReportMissingLocaleValues(string Locale) {
			List<DictionaryEntry> collection = new List<DictionaryEntry>();
			using(var ipa = new DAL()) {
				var list = ipa.DictionaryEntryGetCollection();
				foreach (var entry in list) {
					if (String.IsNullOrEmpty(entry.GetValue(Locale))) {
						collection.Add(entry);
					}
				}
			}
			return View(collection);
		}

		[HttpPost]
		public JsonResult SectionKeyCount() {
			if (CacheHandler.ItemExpired("_dictionaryKeyCount")) {
				using(var ipa = new DAL()) {
					CacheHandler.AddItem("_dictionaryKeyCount", ipa.DictionarySectionKeyCountCollection(), 60);
				}
			}
			return Json(new JsonResultData(JsonResultData.ResultStatus.Ok) { Data = CacheHandler.GetItem("_dictionaryKeyCount") });
      }

		public ActionResult CopyToNew(int id)
		{
			using (var ipa = new DAL())
			{
				var Entry = ipa.DictionaryEntryGetSingle(id);
				if (Entry != null)
				{
					Entry.ID = 0;
					Entry.Key = "copy of " + Entry.Key;
					var newEntry = ipa.DictionaryEntrySave(Entry);
					ThumbsUp();
					return RedirectToAction("Edit", new { ID = newEntry.ID });
				}
			}
			return RedirectToAction("Index");
		}

	}
}