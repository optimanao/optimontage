﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;
using waAssembly.Services.Models;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class CheckListItemController : SiteController
    {

        private DAL ipa = null;
        public CheckListItemController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        private CheckListItemEditModel fixModel(CheckListItemEditModel model)
        {
            var entityType = typeof(CheckListItem).Name;
            model.AvailableLanguages = statics.Cache.LanguagesAll();
            // don't overwrite on postback
            if (model.LocalizedValues == null || !model.LocalizedValues.Any())
            {
                model.LocalizedValues = ipa.LocalizedValueGetCollection(entityType, model.ID);
            }

            foreach (var lang in model.AvailableLanguages)
            {
                if (!model.LocalizedValues.Any(m => m.LanguageID == lang.ID))
                {
                    model.LocalizedValues.Add(new LocalizedValue()
                    {
                        ID = 0,
                        LanguageID = lang.ID,
                        SourceEntityID = model.ID,
                        SourceEntityName = entityType,
                        Text = model.Name
                    });
                }
            }
            return model;
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.CheckListItemSearch(Phrase);
            //if (!CurrentCheckListItem.HasRole("SBA"))
            //{
            //    d = d.Where(m => m.CompanyID == CurrentCheckListItem.CompanyID).ToList();
            //}
            return PartialView("_search", d);
        }

        [HttpPost]
        public ActionResult SearchAjax(string Phrase)
        {
            var d = ipa.CheckListItemSearch(Phrase);
            return Json(new JsonResultData(JsonResultData.ResultStatus.Ok) { Data = d });
        }

        public ActionResult Create()
        {
            var model = new CheckListItemEditModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(CheckListItemEditModel model)
        {
            if (!ModelState.IsValid)
            {
                ThumbsDown("");
                return View(model);
            }
            var entity = new CheckListItem();
            statics.Mapper.Map(model, entity);
            entity = ipa.CheckListItemSave(entity);

            ThumbsUp();
            return ReturnToView("Index", entity.ID);
        }

        public ActionResult Edit(long ID)
        {
            var entity = ipa.CheckListItemGetSingle(ID);
            var model = statics.Mapper.Map<CheckListItemEditModel>(entity);
            model = fixModel(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CheckListItemEditModel model)
        {
            if (string.IsNullOrEmpty(model.Name))
            {
                model = fixModel(model);
                ThumbsDown();
                return View(model);
            }
            var entity = ipa.CheckListItemGetSingle(model.ID);
            statics.Mapper.Map(model, entity);
            ipa.CheckListItemSave(entity);

            foreach (var localizedValue in model.LocalizedValues)
            {
                ipa.LocalizedValueSave(localizedValue);
            }

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Delete(long ID)
        {
            ipa.CheckListItemDeletePermanently(ID);
            ThumbsUp();
            return RedirectToAction("Index");
        }


    }
}
