﻿using API;
using API.Data;
using API.Data.Models;
using System.Web.Mvc;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize]
    [ResetViewBag]
    public class PartCategoryController : SiteController
    {
        private DAL ipa = null;
        public PartCategoryController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
            {
                ipa.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.PartCategoryGetCollection();
            return PartialView("_search", d);
        }

        public ActionResult Create()
        {
            var model = new PartCategoryEditModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PartCategoryEditModel model)
        {
            if (!ModelState.IsValid)
            {
                ThumbsDown("");
                return View(model);
            }

            model.Name = model.Name.Trim();

            var user = new PartCategory();
            statics.Mapper.Map(model, user);
            user = ipa.PartCategorySave(user);

            ThumbsUp();
            return ReturnToView("Index", user.ID);
        }

        public ActionResult Edit(long ID)
        {
            var user = ipa.PartCategoryGetSingle(ID);
            var model = statics.Mapper.Map<PartCategoryEditModel>(user);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PartCategoryEditModel model)
        {
            var user = ipa.PartCategoryGetSingle(model.ID);

            if (!ModelState.IsValid)
            {
                ThumbsDown();
                return View(model);
            }

            statics.Mapper.Map(model, user);
            ipa.PartCategorySave(user);

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        //public ActionResult Print() {
        //    return View(ipa.PartCategoryGetCollection());
        //}

        public ActionResult Delete(int ID) {
            ipa.PartCategoryDeletePermanently(ID);
            ThumbsUp();
            return RedirectToAction("Index");
        }
    }
}
