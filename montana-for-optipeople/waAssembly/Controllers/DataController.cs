﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;

namespace waAssembly.Controllers {
	public class DataController : API.SiteController, IDisposable {

		//private string entityPrefix = "bg";
		private DAL ipa = null;

		public DataController() {
			ipa = new DAL();
		}



		void IDisposable.Dispose() {
			if (ipa != null)
				ipa.Dispose();
		}


	}
}
