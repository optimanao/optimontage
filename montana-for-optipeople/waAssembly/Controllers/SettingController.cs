﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class SettingController : SiteController
    {

        private DAL ipa = null;
        public SettingController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View(ipa.SettingGetCollection());
        }

        [HttpPost]
        public ActionResult Index(List<Setting> items)
        {
            var settings = ipa.SettingGetCollection();

            foreach (var item in items) {
                var setting = settings.FirstOrDefault(m => m.ID == item.ID);
                if (setting != null) {
                    setting.SettingValue = item.SettingValue;
                    ipa.SettingSave(setting);
                }
            }

            foreach(var item in ipa.CacheGetCollection())
            {
                item.CreatedOnUtc = item.CreatedOnUtc.AddDays(-1);
                ipa.CacheSave(item);
            }
            ThumbsUp();
            return RedirectToAction("Index");
        }

        
    }
}
