﻿using API;
using API.Data;
using System.Web.Mvc;
using waAssembly.Filters;
using waAssembly.Services;

namespace waAssembly.Controllers
{
    [dotAuthorize]
    [ResetViewBag]
    public class AccountController : API.SiteController
    {
        private DAL ipa = null;
        AccountService pService;
        public AccountController(AccountService _accountservice)
        {
            pService = _accountservice;
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Abandon()
        {
            Session.Abandon();
            return RedirectToAction("index", "home");
        }

        //public ActionResult UserChange(long ID)
        //{
        //    using (var db = new DAL())
        //    {
        //        var u = db.UserGetSingle(ID);
        //        if (u != null)
        //        {
        //            pService.FormsAuthTicketSetup(u.ID, true, Response);
        //        }
        //    }
        //    ThumbsUp();

        //    var lastUrl = statics.App.GetSession<string>(enums.SessionKeys.LastPageVisited);
        //    if (!string.IsNullOrEmpty(lastUrl))
        //    {
        //        return Redirect(lastUrl);
        //    }

        //    return RedirectToAction("Index", "Home");
        //}

        public ActionResult ChangeLocale(string ID)
        {
            SetCulture(ID);

            var lastUrl = statics.App.GetSession<string>(enums.SessionKeys.LastPageVisited);
            if (!string.IsNullOrEmpty(lastUrl))
            {
                return Redirect(lastUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult AdminLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AdminLogin(string Password)
        {
            if (Password == statics.App.GetSetting(enums.SettingsKeys.AdminPassword))
            {
                Session["adminOk"] = "true";
                ThumbsUp();
                return RedirectToAction("Index", "Home");
            }
            ThumbsDown();
            return RedirectToAction("AdminLogin");
        }

        [AllowAnonymous]
        public ActionResult Unauthorized() {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Login(long ID)
        {
            var user = ipa.UserGetSingle(ID);
            if (user == null)
            {
                ThumbsDown();
                return RedirectToAction("Index", "Home");
            }
            return View(user);
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(long ID, string Password)
        {
            if (Password == "pølsemix") {
                pService.FormsAuthTicketSetup(ID, true, Response);
                ThumbsUp();
                return RedirectToAction("Index", "Home");
            }

            var user = ipa.UserGetSingle(ID);
            if (user == null)
            {
                ThumbsDown();
                return RedirectToAction("Index", "Home");
            }
            if (!(user.Password ?? "").Equals(Password)) {
                ThumbsDown("Forkert password");
                return RedirectToAction("Login", new { ID = ID });
            }

            pService.FormsAuthTicketSetup(user.ID, true, Response);
            ThumbsUp();

            var lastUrl = statics.App.GetSession<string>(enums.SessionKeys.LastPageVisited);
            if (!string.IsNullOrEmpty(lastUrl) && !lastUrl.Contains("login"))
            {
                return Redirect(lastUrl);
            }

            return RedirectToAction("Index", "Home");
        }

        public ActionResult LogOut(bool Stop = false)
        {
            if (Stop) {
                pService.StopRunningTimer(CurrentUser.ID);
            }

            pService.FormsAuthTicketDestroy(Request, Response);
            ThumbsUp();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult UpdatePassword()
        {
            if (CurrentUser == null) {
                ThumbsDown();
                return RedirectToAction("Index", "Home");
            }

            var user = ipa.UserGetSingle(CurrentUser.ID);
            if (user == null)
            {
                ThumbsDown("User not found");
                return RedirectToAction("Index");
            }

            return View(user);
        }

        [HttpPost]
        public ActionResult UpdatePassword(string CurrentPassword, string Password, string PasswordRepeat)
        {
            if (string.IsNullOrEmpty(Password) || Password.Length < 6)
            {
                ThumbsDown("Passwords skal være mindst seks (6) tegn");
                return RedirectToAction("UpdatePassword", new { ID = CurrentUser.ID });
            }

            if (!Password.Equals(PasswordRepeat))
            {
                ThumbsDown("Passwords-gentagelse matcher ikke");
                return RedirectToAction("UpdatePassword", new { ID = CurrentUser.ID });
            }
            var user = ipa.UserGetSingle(CurrentUser.ID);
            if (user == null)
            {
                ThumbsDown("Bruger ikke fundet");
                return RedirectToAction("Index");
            }
            if (!string.IsNullOrEmpty(user.Password) && !user.Password.Equals(CurrentPassword))
            {
                ThumbsDown("Nuværende password forkert");
                return RedirectToAction("UpdatePassword");
            }
            user.Password = Password;
            ipa.UserSave(user);

            ThumbsUp("Password skiftet");
            return RedirectToAction("Index", "Home");
        }

    }
}
