﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using API;
using API.Data;
using API.Data.Models;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize(OnlyAdmin = true)]
    [ResetViewBag]
    public class ProductController : SiteController
    {

        private DAL ipa = null;
        public ProductController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.ProductSearch(Phrase);
            //if (!CurrentProduct.HasRole("SBA"))
            //{
            //    d = d.Where(m => m.CompanyID == CurrentProduct.CompanyID).ToList();
            //}
            return PartialView("_search", d);
        }

        public ActionResult Create()
        {
            var model = new ProductEditModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(ProductEditModel model)
        {
            if (!ModelState.IsValid) {
                ThumbsDown("");
                return View(model);
            }

            model.Name = model.Name.Trim();

            var user = new Product();
            statics.Mapper.Map(model, user);
            user = ipa.ProductSave(user);

            ThumbsUp();
            return ReturnToView("Index", user.ID);
        }

        public ActionResult Edit(long ID)
        {
            var user = ipa.ProductGetSingle(ID);
            var model = statics.Mapper.Map<ProductEditModel>(user);
            model.CheckListItems = ipa.CheckListItemGetCollection(ID);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(ProductEditModel model)
        {
            var user = ipa.ProductGetSingle(model.ID);
            
            if (!ModelState.IsValid)
            {
                ThumbsDown();
                return View(model);
            }

            statics.Mapper.Map(model, user);
            ipa.ProductSave(user);

            ipa.LinkProductCheckListItemDeleteByProduct(model.ID);
            int sortOrder = 0;
            foreach (var checkId in model.CheckListItemsPostback) {
                ipa.LinkProductCheckListItemSave(new LinkProductCheckListItem() {
                    CheckListItemID = checkId,
                    ProductID = model.ID,
                    SortOrder = sortOrder++
                });
            }


            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }


        //public ActionResult Import() {

        //    foreach (var name in ipa.NavInfoGetProductNamesUnique().OrderBy(m => m)) {
        //        ipa.ProductSave(new Product() { Name = name });
        //    }
        //    return Content("OK");
        //}

        //public ActionResult Delete(long ID)
        //{
        //    var user = ipa.ProductGetSingle(ID);
        //    user.DeletedOnUtc = DateTime.UtcNow;
        //    ipa.ProductSave(user);
        //    ThumbsUp();
        //    return RedirectToAction("Index");
        //}

    }
}
