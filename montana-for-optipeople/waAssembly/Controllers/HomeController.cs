﻿using API;
using API.Data;
using API.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Hosting;
using System.Web.Mvc;
using waAssembly.Services;

namespace waAssembly.Controllers
{
    [dotAuthorize]
    [WorkstationRequired]
    public class HomeController : SiteController
    {
        private int normTimeFactor = 2280; //2280 seconds = 38 minutes = 1 norm

        private ValidationTestControllerService validationService;

        public HomeController(ValidationTestControllerService vService)
        {
            validationService = vService;
        }

        public ActionResult Index()
        {
            return RedirectToAction("Dashboard");
        }

        public ActionResult Error()
        {
            if (!string.IsNullOrEmpty(Request.QueryString["msg"]))
            {
                TempData[enums.TempDataKeys.MessageError.ToString()] = Request.QueryString["msg"];
            }
            return View();
        }

        public ActionResult Dashboard(DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            var dtFrom = (dateFrom.HasValue ? dateFrom.Value.Date : DateTime.Now.Date.AddDays(-7));
            var dtTo = (dateTo.HasValue ? dateTo.Value.Date : DateTime.Now.Date);

            if (dtTo < dtFrom)
            {
                dtTo = dtFrom;
            }

            DashboardModel model;

            // data for current user is loaded on the fly (infrequently requrested)
            using (var db = new DAL())
            {
                // data for entire department is loaded from cache
                var cache = db.CacheGetSingle("DashboardAll");
                if (cache == null)
                {
                    // must load inline
                    model = getDashboardModelAll(dtFrom, dtTo, CurrentUser.ID);
                    db.CacheSave(new Cache() { ID = 0,
                        CacheKey = "DashboardAll",
                        CacheValue = statics.ToJson(model),
                        CreatedOnUtc = DateTime.UtcNow }
                    );
                }
                else
                {
                    model = statics.FromJson<DashboardModel>(cache.CacheValue);
                    if (cache.CreatedOnUtc < DateTime.UtcNow.AddHours(-2)) {
                        // temporarily update timestamp to prevent further background tasks
                        cache.CreatedOnUtc = DateTime.UtcNow.AddHours(-1);
                        db.CacheSave(cache);

                        var cuid = CurrentUser.ID;

                        HostingEnvironment.QueueBackgroundWorkItem(task =>
                        {
                            var modelToDb = getDashboardModelAll(dtFrom, dtTo, cuid);
                            using (var db2 = new DAL()) {
                                db2.CacheSave(new Cache() { ID = 0, CacheKey = "DashboardAll", CacheValue = statics.ToJson(modelToDb), CreatedOnUtc = DateTime.UtcNow });
                            }
                        });

                    }
                }



                var items = db.TimeLogGetCollection(CurrentUser.ID, dtFrom, dtTo)
                   .Where(m => !m.InProgress)
                   .OrderBy(m => m.StartTimeUtc)
                   .ToList();

                var orders = db.OrderGetCollection(CurrentUser.ID, dtFrom, dtTo)
                    .OrderBy(m => m.CreatedOnUtc)
                    .ToList();

                // <user data>
                if (items.Any())
                {
                    for (var i = 0; i <= (int)Math.Floor(dtTo.Subtract(dtFrom).TotalDays); i++)
                    {
                        var dt = dtFrom.AddDays(i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            continue;
                        }
                        var itemsToday = items.Where(m => m.StartTimeUtc.Date == dt.Date);
                        var firstStart = itemsToday.FirstOrDefault();
                        if (firstStart == null)
                        {
                            continue;
                        }
                        var lastEnd = itemsToday.Select(m => m.StartTimeUtc.AddSeconds(m.Duration ?? 0)).OrderByDescending(m => m).First();

                        model.ChartSecsTotal += (int)Math.Floor(lastEnd.Subtract(firstStart.StartTimeUtc).TotalSeconds);
                        model.ChartSecsPause += itemsToday.Where(m => m.StatusCodeID.HasValue).Sum(m => m.Duration) ?? 0;
                        model.ChartSecsMounting += itemsToday.Where(m => m.StatusCodeID == null).Sum(m => m.Duration) ?? 0;
                    }

                    model.ChartSecsWaste = model.ChartSecsTotal - model.ChartSecsPause - model.ChartSecsMounting;

                    for (var i = 0; i <= (int)Math.Floor(dtTo.Subtract(dtFrom).TotalDays); i++)
                    {
                        var dt = dtFrom.AddDays(i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            continue;
                        }
                        var timeRegistered = items.Where(m => m.StatusCodeID == null && m.StartTimeUtc.Date == dt.Date).Sum(m => m.Duration);
                        model.NormCountsDic.Add(dt.Date.ToDefString(), (decimal)timeRegistered / normTimeFactor);


                        /*
                         total indlogget tid pr. dag
                         total antal normer pr dag (*38)
                         = resultatgraf er procentuel
                        */
                        // <productivity>
                        {
                            var dicKey = dt.Date.ToDefString();
                            var itemsToday = items.Where(m => m.StartTimeUtc.Date == dt.Date);
                            var firstStart = itemsToday.FirstOrDefault();
                            if (firstStart == null)
                            {
                                continue;
                            }
                            var lastEnd = itemsToday.Select(m => m.StartTimeUtc.AddSeconds(m.Duration ?? 0)).OrderByDescending(m => m).First();
                            var todayLoggedIn = (int)Math.Floor(lastEnd.Subtract(firstStart.StartTimeUtc).TotalSeconds);

                            if (model.ProductivityLoggedInDic.ContainsKey(dicKey))
                            {
                                model.ProductivityLoggedInDic[dicKey] += todayLoggedIn;
                            }
                            else
                            {
                                model.ProductivityLoggedInDic.Add(dicKey, todayLoggedIn);
                            }

                            //if (model.ProductivityChartDic.ContainsKey(dicKey))
                            //{
                            //    model.ProductivityChartDic[dicKey].TimeLoggedIn += todayLoggedIn;
                            //    model.ProductivityChartDic[dicKey].Norms += (decimal)timeRegistered; // same as above
                            //}
                            //else
                            //{
                            //    var produtivityModel = new DashboardProductivityChartModel();
                            //    produtivityModel.Date = dt.Date;
                            //    produtivityModel.TimeLoggedIn = todayLoggedIn;
                            //    produtivityModel.Norms = (decimal)timeRegistered; // same as above
                            //    model.ProductivityChartDic.Add(dicKey, produtivityModel);
                            //}
                        }
                        // </productivity>



                        var docModel = new DashboardOrderChartModel();
                        docModel.Date = dt;

                        // OrdersCompleted = antal reoler som er færdigmeldte og som ikke har været til rep
                        // OrdersDiscarded = kasserede ordrer
                        // OrdersRepaired = antal reol du har repareret (altså, hvor du er den sidste der har lavet en 'rep' timelog på den)

                        // <OrdersCompleted>
                        // færdigmeldte
                        var ordersCompleted = orders.Where(m => m.CreatedOnUtc.Date == dt.Date && m.IsCompleted).ToList();
                        // som ikke har været til rep nogensinde overhovedet
                        var lstOrdersInRep = db.OrderHasBeenRepaired(ordersCompleted.Select(m => m.Barcode).Distinct().ToList()).Select(m => m.Barcode).Distinct();
                        ordersCompleted.RemoveAll(m => lstOrdersInRep.Contains(m.Barcode));
                        docModel.OrdersCompleted = ordersCompleted.Count;
                        //docModel.OrdersCompleted = orders.Count(m => m.CreatedOnUtc.Date == dt.Date && m.IsCompleted); 
                        // </OrdersCompleted>


                        docModel.OrdersDiscarded = orders.Count(m => m.CreatedOnUtc.Date == dt.Date && m.IsDiscarded);

                        // <OrdersRepaired>
                        //var ordersRepaired = db.OrderGetCollectionRepaired(CurrentUser.ID, dt);
                        var repairedData = db.OrderGetCollectionRepairedAndCompletedByUser(CurrentUser.ID, dt);
                        docModel.OrdersRepaired = repairedData.Count();
                        // </OrdersRepaired>

                        model.OrderChartDic.Add(dt.Date.ToDefString(), docModel);
                    }
                }
            }
            // </user data>
            return View(model);
        }

        private DashboardModel getDashboardModelAll(DateTime dtFrom, DateTime dtTo, long CurrentUserID)
        {
            var model = new DashboardModel();
            using (var db = new DAL())
            {
                List<TimeLog> items = new List<TimeLog>();
                List<Order> orders = new List<Order>();
                int normTarget = 0;
                int.TryParse(db.SettingGetSingle("NormtalMontage")?.SettingValue ?? "", out normTarget);

                model.WSNormTarget = normTarget;

                var usersAtWorkstation = db.UserGetCollectionByTimeLogPeriod(dtFrom, dtTo);


                // <workstation data>
                foreach (var userid in usersAtWorkstation)
                {
                    items = db.TimeLogGetCollection(userid.ID, dtFrom, dtTo)
                        .Where(m => !m.InProgress)
                        .OrderBy(m => m.StartTimeUtc)
                        .ToList();

                    orders = db.OrderGetCollection(userid.ID, dtFrom, dtTo)
                        .OrderBy(m => m.CreatedOnUtc)
                        .ToList();

                    for (var i = 0; i <= (int)Math.Floor(dtTo.Subtract(dtFrom).TotalDays); i++)
                    {
                        var dt = dtFrom.AddDays(i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            continue;
                        }
                        var itemsToday = items.Where(m => m.StartTimeUtc.Date == dt.Date);
                        var firstStart = itemsToday.FirstOrDefault();
                        if (firstStart == null)
                        {
                            continue;
                        }
                        var lastEnd = itemsToday.Select(m => m.StartTimeUtc.AddSeconds(m.Duration ?? 0)).OrderByDescending(m => m).First();

                        model.WSChartSecsTotal += (int)Math.Floor(lastEnd.Subtract(firstStart.StartTimeUtc).TotalSeconds);
                        model.WSChartSecsPause += itemsToday.Where(m => m.StatusCodeID.HasValue).Sum(m => m.Duration) ?? 0;
                        model.WSChartSecsMounting += itemsToday.Where(m => m.StatusCodeID == null).Sum(m => m.Duration) ?? 0;
                    }
                }
                model.WSChartSecsWaste = model.WSChartSecsTotal - model.WSChartSecsPause - model.WSChartSecsMounting;

                foreach (var userid in usersAtWorkstation)
                {
                    items = db.TimeLogGetCollection(userid.ID, dtFrom, dtTo)
                       .Where(m => !m.InProgress)
                       .OrderBy(m => m.StartTimeUtc)
                       .ToList();

                    orders = db.OrderGetCollection(userid.ID, dtFrom, dtTo)
                        .OrderBy(m => m.CreatedOnUtc)
                        .ToList();

                    for (var i = 0; i <= (int)Math.Floor(dtTo.Subtract(dtFrom).TotalDays); i++)
                    {
                        var dt = dtFrom.AddDays(i);
                        if (dt.DayOfWeek == DayOfWeek.Saturday || dt.DayOfWeek == DayOfWeek.Sunday)
                        {
                            continue;
                        }
                        var timeRegistered = items.Where(m => m.StatusCodeID == null && m.StartTimeUtc.Date == dt.Date).Sum(m => m.Duration);
                        var dicKey = dt.Date.ToDefString();
                        if (model.WSNormCountsDic.ContainsKey(dicKey))
                        {
                            model.WSNormCountsDic[dicKey] += (decimal)timeRegistered / normTimeFactor; 
                        }
                        else
                        {
                            model.WSNormCountsDic.Add(dicKey, (decimal)timeRegistered / normTimeFactor);
                        }
                        
                        /*
                          total indlogget tid pr. dag
                          total antal normer pr dag (*38)
                          = resultatgraf er procentuel
                         */
                        // <productivity>
                        {
                            var itemsToday = items.Where(m => m.StartTimeUtc.Date == dt.Date);
                            var firstStart = itemsToday.FirstOrDefault();
                            if (firstStart == null)
                            {
                                continue;
                            }
                            var lastEnd = itemsToday.Select(m => m.StartTimeUtc.AddSeconds(m.Duration ?? 0)).OrderByDescending(m => m).First();
                            var todayLoggedIn = (int)Math.Floor(lastEnd.Subtract(firstStart.StartTimeUtc).TotalSeconds);

                            if (model.WSProductivityLoggedInDic.ContainsKey(dicKey))
                            {
                                model.WSProductivityLoggedInDic[dicKey] += todayLoggedIn;
                            }
                            else
                            {
                                model.WSProductivityLoggedInDic.Add(dicKey, todayLoggedIn);
                            }

                            //if (model.WSProductivityChartDic.ContainsKey(dicKey)) {
                            //    model.WSProductivityChartDic[dicKey].TimeLoggedIn += todayLoggedIn;
                            //    model.WSProductivityChartDic[dicKey].Norms += (decimal)timeRegistered; // same as above

                            //} else {
                            //    var produtivityModel = new DashboardProductivityChartModel();
                            //    produtivityModel.Date = dt.Date;
                            //    produtivityModel.TimeLoggedIn = todayLoggedIn;
                            //    produtivityModel.Norms = (decimal)timeRegistered; // same as above
                            //    model.WSProductivityChartDic.Add(dicKey, produtivityModel);
                            //}
                        }
                        // </productivity>

                        var docModel = new DashboardOrderChartModel();
                        if (model.WSOrderChartDic.ContainsKey(dicKey))
                        {
                            docModel = model.WSOrderChartDic[dicKey];
                        }
                        else
                        {
                            model.WSOrderChartDic.Add(dicKey, docModel);
                        }
                        docModel.Date = dt;

                        // OrdersCompleted = antal reoler som er færdigmeldte og som ikke har været til rep
                        // OrdersDiscarded = kasserede ordrer
                        // OrdersRepaired = antal reol du har repareret (altså, hvor du er den sidste der har lavet en 'rep' timelog på den)

                        // <OrdersCompleted>
                        // færdigmeldte
                        var ordersCompleted = orders.Where(m => m.CreatedOnUtc.Date == dt.Date && m.IsCompleted).ToList();
                        // som ikke har været til rep nogensinde overhovedet
                        var lstOrdersInRep = db.OrderHasBeenRepaired(ordersCompleted.Select(m => m.Barcode).Distinct().ToList()).Select(m => m.Barcode).Distinct();
                        ordersCompleted.RemoveAll(m => lstOrdersInRep.Contains(m.Barcode));

                        docModel.OrdersCompleted += ordersCompleted.Count;
                        // </OrdersCompleted>


                        docModel.OrdersDiscarded += orders.Count(m => m.CreatedOnUtc.Date == dt.Date && m.IsDiscarded);

                        // <OrdersRepaired>
                        //var ordersRepaired = db.OrderGetCollectionRepaired(CurrentUser.ID, dt);
                        var repairedData = db.OrderGetCollectionRepairedAndCompletedByUser(CurrentUserID, dt);
                        docModel.OrdersRepaired += repairedData.Count();
                        // </OrdersRepaired>
                    }
                }
                // </workstation data>
            }
            return model;
        }

        public ActionResult ThrowException() {
            throw new Exception("Testing throw up");
        }
    }
}
