﻿using API;
using API.Data;
using API.Data.Models;
using System.Web.Mvc;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [dotAuthorize]
    [ResetViewBag]
    public class PartsController : SiteController
    {
        private DAL ipa = null;
        public PartsController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
            {
                ipa.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Search(string Phrase)
        {
            var d = ipa.PartSearch(Phrase);
            return PartialView("_search", d);
        }

        public ActionResult Create()
        {
            var model = new PartEditModel();
            model.AvailableCategories = ipa.PartCategoryGetCollection();
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(PartEditModel model)
        {
            if (!ModelState.IsValid)
            {
                ThumbsDown("");
                model.AvailableCategories = ipa.PartCategoryGetCollection();
                return View(model);
            }

            model.Name = model.Name.Trim();

            var user = new Part();
            statics.Mapper.Map(model, user);
            if (user.PartCategoryID == 0) {
                user.PartCategoryID = null;
            }
            user = ipa.PartSave(user);

            ThumbsUp();
            return ReturnToView("Index", user.ID);
        }

        public ActionResult Edit(long ID)
        {
            var user = ipa.PartGetSingle(ID);
            var model = statics.Mapper.Map<PartEditModel>(user);
            model.AvailableCategories = ipa.PartCategoryGetCollection();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(PartEditModel model)
        {
            var user = ipa.PartGetSingle(model.ID);

            if (!ModelState.IsValid)
            {
                ThumbsDown();
                model.AvailableCategories = ipa.PartCategoryGetCollection();
                return View(model);
            }

            statics.Mapper.Map(model, user);
            ipa.PartSave(user);

            ThumbsUp();
            return ReturnToView("Index", model.ID);
        }

        public ActionResult Print() {
            return View(ipa.PartGetCollection());
        }

        public ActionResult Delete(int ID) {
            ipa.PartDeletePermanently(ID);
            ThumbsUp();
            return RedirectToAction("Index");
        }
    }
}
