﻿using API;
using API.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using waAssembly.Filters;

namespace waAssembly.Controllers
{
    [ResetViewBag]
    public class AndOnController : SiteController
    {

        private DAL ipa = null;
        public AndOnController()
        {
            ipa = new DAL();
        }

        protected override void Dispose(bool disposing)
        {
            if (ipa != null)
                ipa.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult IndexOld()
        {
            //var week = statics.WeekNumber(DateTime.Now);
            //var monday = statics.FirstDateOfWeekISO8601(DateTime.Now.Year, week).Date;
            //var sunday = monday.AddDays(7);

            var from = DateTime.Now.Date;
            //from = from.AddDays(-1);
            var to = from.AddDays(1);

            var data = ipa.OrderParkingGetCollectionForAndOn(from, to).ToList();

            if (!data.Any())
            {
                ViewBag.ErrorCodes = new Tuple<int, string>[] { new Tuple<int, string>(0, "Ingen fejl i dag") }.AsEnumerable();
                return View();
            }

            //string q = "";
            //foreach (var dat in data.OrderByDescending(m => m.Item1).Take(10))
            //{
            //    q += dat.Item1 + ", " + dat.Item2 + Environment.NewLine;
            //}

            ViewBag.ErrorCodes = data.OrderByDescending(m => m.Item1).Take(10).ToList();
            return View();
        }

        public ActionResult Index()
        {
            var from = DateTime.Now.Date;
            var to = from.AddDays(1);

            var data = ipa.OrderChangeLogGetCollectionForAndOn(from, to).ToList();

            if (!data.Any())
            {
                ViewBag.ErrorCodes = new Tuple<int, string>[] { new Tuple<int, string>(0, "Ingen fejl i dag") }.AsEnumerable();
                return View();
            }

            ViewBag.ErrorCodes = data.OrderByDescending(m => m.Item1).Take(5).ToList();
            return View();
        }

        public ActionResult Targets()
        {
            ViewBag.TargetsWeekly = andonGetTargetsDaily("andon-targets-weekly.txt"); ;
            ViewBag.TargetUpdate = DateTime.Now.Date.AddDays(-1);


            var targetsPath = statics.App.GetSetting(enums.SettingsKeys.AndonFolder);
            List<int> targetsDaily = new List<int>();
            // <general daily target>
            {
                string filePath = System.IO.Path.Combine(targetsPath, "andon-targets-daily.txt");
                if (System.IO.File.Exists(filePath))
                {
                    int iTry = 0;
                    int.TryParse(System.IO.File.ReadAllLines(filePath).First(), out iTry);
                    targetsDaily.Add(iTry);
                }
            }
            // </general daily target>


            // <individual daily targets>
            var dailyFiles = new System.IO.DirectoryInfo(targetsPath).GetFiles("andon-targets-daily-*.txt");
            var todayInt = (int)DateTime.Now.DayOfWeek;

            //todayInt = (int)DayOfWeek.Sunday;

            foreach (var weekday in Enum.GetValues(typeof(DayOfWeek)))
            {
                var dayInt = (int)weekday;
                var day = (DayOfWeek)weekday;
                if (day == DayOfWeek.Saturday || day == DayOfWeek.Sunday)
                {
                    continue;
                }

                if (dayInt > todayInt)
                {
                    continue;
                }

                var dayName = Enum.GetName(typeof(DayOfWeek), weekday).ToLower();
                var file = dailyFiles.FirstOrDefault(m => m.Name.ToLower().Contains(dayName));
                if (file == null)
                {
                    continue;
                }

                ViewBag.TargetUpdate = file.LastWriteTime;

                var lines = System.IO.File.ReadAllLines(file.FullName);
                if (lines?.Any() ?? false)
                {
                    int todaysTarget = 0;
                    int.TryParse(lines.First(), out todaysTarget);
                    targetsDaily.Add(todaysTarget);
                }
            }

            ViewBag.TargetsDaily = targetsDaily;
            // </ individual daily targets>

            using (var db = new DAL())
            {
                List<TimeLog> items = new List<TimeLog>();
                List<Order> orders = new List<Order>();
                int normTarget = 0;
                int.TryParse(db.SettingGetSingle("WorkWeekDays")?.SettingValue ?? "", out normTarget);
                if (normTarget <= 0)
                {
                    normTarget = 5;
                }
                ViewBag.WorkWeekDays = normTarget;
            }
            return View();
        }

        private List<int> andonGetTargetsDaily(string filename)
        {
            var list = new List<int>();
            string path = System.IO.Path.Combine(statics.App.GetSetting(enums.SettingsKeys.AndonFolder), filename);
            if (!System.IO.File.Exists(path))
            {
                return list;
            }

            var lines = System.IO.File.ReadAllLines(path);
            lines.ToList().ForEach(m =>
            {
                var i = 0;
                if (int.TryParse(m, out i))
                {
                    list.Add(i);
                }
            });
            return list;
        }

    }
}
